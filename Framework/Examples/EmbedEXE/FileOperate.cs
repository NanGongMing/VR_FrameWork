/*
* FileName:          FileOperate
* CompanyName:       
* Author:            Relly
* CreateTime:        2022-06-21-10:37:27
* UnityVersion:      2020.3.26f1c1
* Version:           1.0
* Description:       
* 
*/

using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;

public class FileOperate
{

	private void OnGUI()
	{
		if (GUILayout.Button("OpenDialog"))
		{
			OpenFileName ofn = new OpenFileName();
			ofn.structSize = Marshal.SizeOf(ofn);
			ofn.filter = "All Files(.json)\0*.json\0\0";
			ofn.file = new string(new char[256]);
			ofn.maxFile = ofn.file.Length;
			ofn.fileTitle = new string(new char[64]);
			ofn.maxFileTitle = ofn.fileTitle.Length;
			ofn.initialDir = Application.dataPath;
			ofn.title = "open";
			ofn.defExt = "jpg";
			ofn.flags = 0x00080000 | 0x00001000 | 0x00000800 | 0x00000200 | 0x00000008;
			if (GetOpenFileName1(ofn))
			{
				Debug.Log(ofn.file);
			}
		}
	}

	[DllImport("Comdlg32.dll", SetLastError = true, ThrowOnUnmappableChar = true, CharSet = CharSet.Auto)]
	public static extern bool GetOpenFileName([In, Out] OpenFileName ofn);

	public static bool GetOpenFileName1([In, Out] OpenFileName ofn)
	{
		return GetOpenFileName(ofn);
	}

	public static OpenFileName GetOpenFile()
	{
		OpenFileName ofn = new OpenFileName();
		ofn.structSize = System.Runtime.InteropServices.Marshal.SizeOf(ofn);
		ofn.filter = "All Files()\0*.*\0\0";
		ofn.file = new string(new char[256]);
		ofn.maxFile = ofn.file.Length;
		ofn.fileTitle = new string(new char[64]);
		ofn.maxFileTitle = ofn.fileTitle.Length;
		ofn.initialDir = Application.streamingAssetsPath;
		ofn.title = "open";
		ofn.defExt = "jpg";
		ofn.flags = 0x00080000 | 0x00001000 | 0x00000800 | 0x00000200 | 0x00000008;
		GetOpenFileName1(ofn);
		return ofn;
	}
}

[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
public class OpenFileName
{
	public int structSize = 0;
	public IntPtr dlgOwner = IntPtr.Zero;
	public IntPtr instance = IntPtr.Zero;
	public string filter = null;
	public string custonFilter = null;
	public int maxCustFilter = 0;
	public int filterIndex = 0;
	public string file = null;
	public int maxFile = 0;
	public string fileTitle = null;
	public int maxFileTitle = 0;
	public string initialDir = null;
	public string title = null;
	public int flags = 0;
	public short fileOffect = 0;
	public short fileExtension = 0;
	public string defExt = null;
	public IntPtr custData = IntPtr.Zero;
	public IntPtr hook = IntPtr.Zero;
	public string templateName = null;
	public IntPtr reservedPtr = IntPtr.Zero;
	public int reservedInt = 0;
	public int flagsEx = 0;
}




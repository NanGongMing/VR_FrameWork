/*
* FileName:          NewForm
* CompanyName:       杭州中锐
* Author:            Relly
* CreateTime:        2022-06-21-09:13:28
* UnityVersion:      2020.3.26f1c1
* Version:           1.0
* Description:       
* 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using UnityEngine;

public class NewForm : Form
{


	public int formWidth = 500;
	public int formHeight = 800;
	public NewForm()
	{
		BackColor = System.Drawing.Color.FromArgb(239, 235, 233);
		MinimumSize = new Size(320, 240);
		Text = "窗体1";
		Size = new Size(formWidth, formHeight);
		SizeGripStyle = SizeGripStyle.Show;
		var textboxAutoSize = new TextBox();
		textboxAutoSize.Text = "input here";
		textboxAutoSize.Location = new Point(50, 50);
		textboxAutoSize.Size = new Size(200, 50);
		textboxAutoSize.Font = new System.Drawing.Font("Arial", 20f);
		var labelAutosize = new Label();
		labelAutosize.Location = new Point(260, 50);
		labelAutosize.Size = new Size(200, 50);
		labelAutosize.Font = new System.Drawing.Font("Arial", 20f);
		textboxAutoSize.TextChanged += (sender, args) => labelAutosize.Text = textboxAutoSize.Text;
		labelAutosize.Text = textboxAutoSize.Text;
		labelAutosize.BorderStyle = BorderStyle.FixedSingle;
		var mybutton = new Button();
		mybutton.Text = "传值";
		mybutton.Location = new Point(50, 200);
		mybutton.Size = new Size(250, 50);
		mybutton.Font = new System.Drawing.Font("Arial", 20f);
		Controls.Add(textboxAutoSize);
		Controls.Add(labelAutosize);
		Controls.Add(mybutton);
	}



}




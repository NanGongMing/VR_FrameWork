/*
* FileName:          IReference
* CompanyName:       
* Author:            Relly
* CreateTime:        2021-11-18-09:19:08
* UnityVersion:      2019.4.8f1
* Version:           1.0
* Description:       
* 
*/



namespace Framework
{
    /// <summary>
    /// 引用接口
    /// </summary>
    public interface IReference
    {
        /// <summary>
        /// 清理引用
        /// </summary>
        void Clear();
    }
}
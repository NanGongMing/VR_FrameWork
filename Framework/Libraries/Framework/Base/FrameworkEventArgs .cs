/*
* FileName:          FrameworkEventArgs 
* CompanyName:       
* Author:            Relly
* CreateTime:        2021-11-18-09:24:25
* UnityVersion:      2019.4.8f1
* Version:           1.0
* Description:       
* 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Framework
{
    /// <summary>
    /// 框架中包含事件数据的类的基类。
    /// </summary>
    public abstract class FrameworkEventArgs : EventArgs, IReference
    {
        /// <summary>
        /// 初始化框架中包含事件数据的类的新实例。
        /// </summary>
        public FrameworkEventArgs()
        {

        }

        /// <summary>
        /// 清理引用。
        /// </summary>
        public abstract void Clear();
    }
}
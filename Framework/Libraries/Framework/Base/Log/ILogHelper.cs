/*
* FileName:          ILogHelper
* CompanyName:  
* Author:            
* CreateTime:        2021-10-28-17:38:36
* UnityVersion:      2019.4.8f1
* Version:           1.0
* Description:       
* 
*/


namespace Framework
{
    /// <summary>
    /// 框架日志辅助器接口。
    /// </summary>
    public interface ILogHelper
    {
        /// <summary>
        /// 记录日志。
        /// </summary>
        /// <param name="level">游戏框架日志等级。</param>
        /// <param name="message">日志内容。</param>
        void Log(FrameworkLogLevel level, object message);
    }
}
/*
* FileName:          ConfigComponent
* CompanyName:       
* Author:            Relly
* CreateTime:        2022-09-02-13:49:15
* UnityVersion:      2020.3.26f1c1
* Description:       全局配置
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityFramework.Runtime;

namespace UnityFramework.Config
{
    [DisallowMultipleComponent]
    public class ConfigComponent : UnityFrameworkComponent
    {

    }
}

/*
* FileName:          FrameworkComponent
* CompanyName:  
* Author:            Relly
* CreateTime:        2021-10-28-13:16:22
* UnityVersion:      2019.4.8f1
* Version:           1.0
* Description:       
* 
*/


using UnityEngine;

namespace UnityFramework.Runtime
{
    /// <summary>
    /// 框架组件抽象类
    /// </summary>
    public abstract class UnityFrameworkComponent : MonoBehaviour
    {
        /// <summary>
        /// 框架组件初始化。
        /// </summary>
        protected virtual void Awake()
        {
            UnityFrameworkEntry.RegisterComponent(this);
        }
    }
}
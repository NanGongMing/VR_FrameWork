/*
* FileName:          KeyCodePressEventArgs
* CompanyName:       杭州中锐
* Author:            Relly
* CreateTime:        2022-06-14-13:45:29
* UnityVersion:      2020.3.26f1c1
* Version:           1.0
* Description:       
* 
*/

using Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyCodePressEventArgs : BaseEventArgs
{
    public static readonly int EventId = typeof(KeyCodePressEventArgs).GetHashCode();

    public override int Id
    {
        get
        {
            return EventId;
        }
    }

    public object UserData
    {
        get;
        private set;
    }
    public KeyCode KeyCode
    {
        get;
        private set;
    }
    public KeyCodePressEventArgs()
    {
        KeyCode = KeyCode.None;
        UserData = null;
    }
    public static KeyCodePressEventArgs Create(KeyCode keyCode, object userData = null)
    {
        // 使用引用池技术，避免频繁内存分配
        KeyCodePressEventArgs keyCodePressEventArgs = ReferencePool.Acquire<KeyCodePressEventArgs>();
        keyCodePressEventArgs.KeyCode = keyCode;
        keyCodePressEventArgs.UserData = userData;
        return keyCodePressEventArgs;
    }
    public override void Clear()
    {
        KeyCode = KeyCode.None;
        UserData = null;
    }
}




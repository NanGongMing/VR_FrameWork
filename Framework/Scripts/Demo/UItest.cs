/*
* FileName:          UItest
* CompanyName:  
* Author:            ShangChao
* CreateTime:        2020-07-07-11:07:58
* UnityVersion:      2018.4.2f1
* Version:           1.0
* Description:       
* 
*/

using UnityFramework.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UItest : UIView
{

	protected override void OnStart()
	{
		//Debug.LogError(GetUIViewId());

		//Debug.LogError(Id);
		//Debug.LogError(UIManager.Instance.GetUIView(Id));
		//Debug.LogError(UIManager.Instance.GetUIViewId(this));
		this.transform.SetAsLastSibling();
		Id = GetHashCode();

	}

	public override void OnUpdate(float deltaTime)
	{
		//	Debug.LogError(name + "OnUpdate");

	}

}

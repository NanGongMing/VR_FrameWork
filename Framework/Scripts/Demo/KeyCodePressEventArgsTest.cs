/*
* FileName:          KeyCodePressEventArgsTest
* CompanyName:       杭州中锐
* Author:            Relly
* CreateTime:        2022-06-14-14:08:53
* UnityVersion:      2020.3.26f1c1
* Version:           1.0
* Description:       
* 
*/

using System;
using UnityEngine;
using UnityFramework;
using UnityFramework.Runtime;

public class KeyCodePressEventArgsTest : MonoBehaviour
{
    private void Start()
    {
        UtilityTools.DelayCommandEvent(5, () => UnityFramework.Framework.Event.Subscribe(KeyCodePressEventArgs.EventId, KeyCodePress));


    }
    private void Update()
    {
        if (Input.anyKeyDown)
        {
            foreach (KeyCode keyCode in Enum.GetValues(typeof(KeyCode)))
            {
                if (Input.GetKeyDown(keyCode))
                {
                    UnityFramework.Framework.Event.Fire(this, KeyCodePressEventArgs.Create(keyCode, ""));

                }
            }
        }
    }
    private void OnApplicationQuit()
    {
        UnityFramework.Framework.Event.Unsubscribe(KeyCodePressEventArgs.EventId, KeyCodePress);

    }
    void KeyCodePress(object sender, Framework.BaseEventArgs e)
    {
        KeyCodePressEventArgs keyCodePressEventArgs = (KeyCodePressEventArgs)e;
        Log.Error($"{keyCodePressEventArgs.KeyCode}被按下");
        if (KeyCode.Escape == keyCodePressEventArgs.KeyCode)
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
			Application.Quit();

#endif
        }
        else
        {
            UnityFramework.Framework.Scene.SyncLoadScene("Demo1");
        }
    }
}




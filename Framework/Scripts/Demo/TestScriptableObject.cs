/*
* FileName:          TestScriptableObject
* CompanyName:       
* Author:            Relly
* CreateTime:        2022-07-12-14:05:27
* UnityVersion:      2020.3.26f1c1
* Version:           1.0
* Description:       
* 
*/

#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
[CreateAssetMenu(fileName = "TestScriptableObject", menuName = "ScriptableObjects/TestScriptableObject", order = 1)]
public class TestScriptableObject : ScriptableObject
{
	public string prefabName;
	public int numberOfPrefabsToCreate;
	public Vector3[] spawnPoints;
#if UNITY_EDITOR

	//如果存在同名的不会创建
	[MenuItem("Assets/My Scriptable Object", false, 0)]
	public static void CreateMyAsset()
	{

		UnityEngine.Object obj = Selection.activeObject;
		if (obj)
		{
			string path = AssetDatabase.GetAssetPath(obj);
			TestScriptableObject asset = CreateInstance<TestScriptableObject>();

			if (asset)
			{

				int index = 0;
				string confName = "";
				UnityEngine.Object obj1 = null;
				do
				{
					confName = path + "/" + typeof(TestScriptableObject).Name + "_" + index + ".asset";
					obj1 = UnityEditor.AssetDatabase.LoadAssetAtPath(confName, typeof(TestScriptableObject));
					index++;
				} while (obj1);

				AssetDatabase.CreateAsset(asset, confName);
				AssetDatabase.SaveAssets();
				EditorUtility.FocusProjectWindow();
				Selection.activeObject = asset;
			}


		}

	}
#endif


}



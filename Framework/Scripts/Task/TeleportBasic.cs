using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportBasic : MonoBehaviour
{
	public TeleportConf teleportConf;

	protected Collider[] colliders;

	public Action OnPlayerEnter;


	bool isInit;
	// 不能使用Awake
	protected virtual void Awake()
	{
		if (!isInit)
		{
			InitComponent();
		}
	}

	public virtual void InitComponent()
	{
		if (isInit)
		{
			return;
		}
		isInit = true;

		TeleportConf.dic[teleportConf.triggerName] = teleportConf;

		if (teleportConf != null)
		{
			teleportConf.triggerBasic = this;
		}

		colliders = gameObject.GetComponents<Collider>();

		DisableCollider();
	}

	public void ShowObject()
	{
		EnableCollider();
		if (!gameObject.activeInHierarchy)
		{
			gameObject.SetActive(true);
		}
	}

	public void HideObject()
	{
		DisableCollider();
		if (gameObject.activeSelf)
		{
			gameObject.SetActive(false);
		}
	}


	void OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<CharacterController>())
		{
			OnPlayerEnter?.Invoke();
		}
	}

	public virtual void EnableCollider()
	{
		//Debug.LogError ("EnableCollider" + triggerConf.triggerName);
		if (colliders == null)
		{
			return;
		}
		foreach (Collider item in colliders)
		{
			item.enabled = true;
		}
	}

	public virtual void DisableCollider()
	{
		//Debug.LogError ("DisableCollider" + triggerConf.triggerName);
		if (colliders == null)
		{
			return;
		}
		foreach (Collider item in colliders)
		{
			item.enabled = false;
		}
	}

}

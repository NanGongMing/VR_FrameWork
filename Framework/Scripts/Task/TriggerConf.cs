using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class TriggerConf : ScriptableObject
{
	public string triggerName;
	public int priority;
	public bool isCanDo;

	public string errorKey;

	public string keyDesc;

	[HideInInspector]
	public TriggerBasic triggerBasic;

#if UNITY_EDITOR
	[MenuItem("Assets/TriggerConf", false, 0)]
	static void CreateDynamicConf()
	{
		UnityEngine.Object obj = Selection.activeObject;
		if (obj)
		{
			string path = AssetDatabase.GetAssetPath(obj);
			ScriptableObject bullet = CreateInstance<TriggerConf>();
			if (bullet)
			{
				string confName = UnityUtil.TryGetName<TriggerConf>(path);
				AssetDatabase.CreateAsset(bullet, confName);
			}
			else
			{
				Debug.Log(typeof(TriggerConf) + " is null");
			}
		}
	}

#endif
}

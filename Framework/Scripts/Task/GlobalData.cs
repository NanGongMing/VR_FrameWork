﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalData 
{
   /// <summary>
	/// 下个场景名
	/// </summary>
	public static string nextSceneName;

	/// <summary>
	/// 任务模式
	/// </summary>
	public static TaskMode TaskMode = TaskMode.Default;

	/// <summary>
	/// 任务类型
	/// </summary>
	public static SkipTaskType skipTaskType;


	/// <summary>
	/// 任务开始时间
	/// realtimeSinceStartup
	/// </summary>
	public static float startTime;





}

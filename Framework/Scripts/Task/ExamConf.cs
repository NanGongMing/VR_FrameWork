#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class ExamConf : ScriptableObject
{
    [Header("测试题目 key")]
    public string quest;
    [Header("测试选项 key")]
    public string[] options;
    [Header("测试答案 key")]
    public string[] answers;
#if UNITY_EDITOR
    [MenuItem("Assets/Create/Exam/ExamConf", false, 0)]
    static void CreateDynamicConf()
    {
        UnityEngine.Object obj = Selection.activeObject;
        if (obj)
        {
            string path = AssetDatabase.GetAssetPath(obj);
            ScriptableObject bullet = ScriptableObject.CreateInstance<ExamConf>();
            if (bullet)
            {
                string confName = UnityUtil.TryGetName<ExamConf>(path);
                AssetDatabase.CreateAsset(bullet, confName);
            }
            else
            {
                Debug.Log(typeof(ExamConf) + " is null");
            }
        }
    }
#endif
}

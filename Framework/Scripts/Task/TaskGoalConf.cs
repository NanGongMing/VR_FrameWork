#if UNITY_EDITOR
using UnityEditor;
#endif

using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityFramework.Task
{


	//任务目标，不分先后顺序,须有判断成功完成目标或者错误完成目标的功能
	public class TaskGoalConf : ScriptableObject
	{
		/// <summary>
		/// 任务目标描述
		/// </summary>
		public string goalName;
		/// <summary>
		/// 目标执行类型
		/// 所有模式任务目标都执行
		/// All,
		/// 只有培训模式任务目标执行
		/// OnlyTraining,
		/// 只有考试模式任务目标执行
		/// OnlyExamination
		/// </summary>
		public GoalType goalMode;
		/// <summary>
		/// 任务目标优先级
		/// </summary>
		public int priority;
		/// <summary>
		/// 目标对应的Task
		/// </summary>
		protected TaskConf targetTask;

		/// <summary>
		/// 是否完成
		/// </summary>
		protected bool isAchieveGoal = false;
		/// <summary>
		/// 是否自动播放音频
		/// </summary>
		public bool isAutoPlayStartAudio = true;
		/// <summary>
		/// 是否自动高亮
		/// </summary>
		public bool isAutoHighlightStartTool = true;
		/// <summary>
		/// 是否重新播放音频
		/// </summary>
		public bool isReplayStartAudio = true;

		// 本地化key
		public string goalKey;

		/// <summary>
		/// 提示培训模式任务目标开始语音
		/// </summary>
		[Tooltip("提示培训模式任务目标开始语音")]
		public List<string> goalTrainingStartAudioKeys;
		public List<string> goalTrainingEndAudioKeys;
		public List<string> goalTrainingSuccessAudioKeys;
		public List<string> goalTrainingErrorAudioKeys;
		/// <summary>
		/// 提示考试模式任务目标开始语音
		/// </summary>
		[Tooltip("提示考试模式任务目标开始语音")]
		public List<string> goalExaminationStartAudioKeys;
		public List<string> goalExaminationEndAudioKeys;
		public List<string> goalExaminationSuccessAudioKeys;
		public List<string> goalExaminationErrorAudioKeys;

		/// <summary>
		/// 教学引导任务语音
		/// </summary>
		public List<string> goalTeachingStartAudioKeys;



		/// <summary>
		/// 是否在目标开始时自动将工具加入到快捷工具栏
		/// </summary>
		[Header("自动添加工具到目标工具栏")]
		public bool autoAddFastTool = true;

		/// <summary>
		/// 是否在目标开始时自动切换手套
		/// </summary>
		[Header("是否切换手套")]
		public bool isSetChangeGlove;


		/// <summary>
		/// 是否设置倒计时
		/// </summary>
		[Header("是否设置倒计时")]
		public bool isSetCountDown = false;
		/// <summary>
		/// 倒计时时间
		/// </summary>
		[Header("倒计时时间")]
		public int countDownTime = 60;
		/// <summary>
		/// 倒计时结束提示
		/// </summary>
		public string GoalTimeOutHint;
		protected TaskTimer countDownTimer;

		/// <summary>
		/// 目标成功事件
		/// </summary>
		public event Action<bool> OnAchieveGoal;
		/// <summary>
		/// 音频延迟时间
		/// </summary>
		public float playDeltTime;
		/// <summary>
		/// 是否最后的任务目标，如果被先执行，则此任务失败
		/// </summary>
		public bool isFinalGoal = false;
		/// <summary>
		/// 强制提示
		/// </summary>    

		public virtual void ForceTip()
		{

		}

		private bool hasDone = false;
		public bool HasDone { get => hasDone; set => hasDone = value; }

		public virtual void OnTaskPlayStartAudioComplete(TaskConf taskConf)
		{
			if (TaskManager.Instance.taskMode != TaskMode.Study
				  && isSetCountDown)
			{
				if (!hasDone && targetTask.taskState.Equals(TaskState.Doing))
				{
					Debug.Log("Start Count Down " + goalName);
					countDownTimer = TaskTimerMgr.Instance.CreateTimer(GoalTimeOut, countDownTime, 1);
				}
			}
		}

		/// <summary>
		/// 任务目标初始化
		/// </summary>
		public virtual void OnTaskInit(TaskConf taskConf)
		{

			countDownTimer = TaskTimerMgr.Instance.DestroyTimer(countDownTimer);

			HasDone = false;
			targetTask = taskConf;
			isAchieveGoal = false;
		}

		/// <summary>
		/// 播放音频
		/// </summary>
		protected virtual void PlayAudios(List<string> audios)
		{
			if (audios != null)
			{
				AudioManager.Instance.PlayLocalLocalizationAudio(audios, playDeltTime);
			}
		}

		public void PlayStartAudio()
		{
			if (TaskManager.Instance.taskMode == TaskMode.Exam)
			{
				if (goalMode != GoalType.Study)
				{
					PlayAudios(goalExaminationStartAudioKeys);
				}
			}
			else if (TaskManager.Instance.taskMode == TaskMode.Study)
			{
				if (goalMode != GoalType.Exam)
				{
					PlayAudios(goalTrainingStartAudioKeys);
				}
			}
			else
			{
				PlayAudios(goalTeachingStartAudioKeys);
			}
		}

		protected virtual void PlayEndAudio()
		{
			if (TaskManager.Instance.taskMode == TaskMode.Exam)
			{
				if (goalMode != GoalType.Study)
				{
					PlayAudios(goalExaminationEndAudioKeys);
				}
			}
			else if (TaskManager.Instance.taskMode == TaskMode.Study)
			{
				if (goalMode != GoalType.Exam)
				{
					PlayAudios(goalTrainingEndAudioKeys);
				}
			}
		}




		/// <summary>
		/// 任务开始
		/// </summary>
		/// <param name="taskConf"></param>
		public virtual void OnTaskStart(TaskConf taskConf)
		{
			Debug.Log("===Goal Start==>" + goalName);

			OnGoalStart();
		}

		/// <summary>
		/// 任务中一直需要监测的事件：比如手柄
		/// </summary>
		/// <param name="targetTask"></param>
		public virtual void OnTaskDoing(TaskConf taskConf)
		{
			//Debug.LogError ("OnTaskDoing");
		}

		/// <summary>
		/// 任务结束时需要的处理
		/// 在任务结束(所有Goal都完成)的时候调用
		/// </summary>
		/// <param name="taskConf"></param>
		public virtual void OnTaskEnd(TaskConf taskConf)
		{
			Debug.Log("===Goal End==>" + goalName);

			HasDone = true;
			PlayEndAudio();

			countDownTimer = TaskTimerMgr.Instance.DestroyTimer(countDownTimer);


		}



		/// <summary>
		/// 强制完成任务
		/// 检测并记录目标是否完成
		/// </summary>
		public virtual void ForceCompleteTask(TaskConf taskConf)
		{
			Debug.Log("ForceCompleteGoal====" + goalName);
		}

		/// <summary>
		/// 重置任务
		/// </summary>
		public virtual void ResetTask(TaskConf taskConf)
		{

		}


		protected virtual void GoalTimeOut()
		{
			countDownTimer = TaskTimerMgr.Instance.DestroyTimer(countDownTimer);
			AudioManager.Instance.OnPlayComplete += GoalTimeOutPlayComplete;
			AudioManager.Instance.PlayLocalLocalizationAudio(GoalTimeOutHint, 0, true);
			float _time = AudioManager.Instance.GetLocalLocalizationAudioLength(new List<string>() { GoalTimeOutHint });
		}

		protected virtual void GoalTimeOutPlayComplete()
		{
			//	AudioManager.Instance.OnPlayComplete -= GoalTimeOutPlayComplete;
		}

		/// <summary>
		/// 完成任务目标发送
		/// </summary>
		/// <param name="success">是否成功完成任务目标</param>
		protected virtual void AchieveGoal(bool success)
		{
			if (!targetTask)
			{
				return;
			}
			if (isAchieveGoal)
			{
				return;
			}
			Debug.LogFormat("{0}完成任务目标:{1}", success ? "成功" : "失败", goalName);
			HasDone = true;
			isAchieveGoal = success;
			OnAchieveGoal_(success);

			DelayAchieveTaskGoal(success);


			//targetTask.AchieveGoal (success);
		}

		public virtual void ReStartGoal()
		{
			if (isReplayStartAudio)
			{
				AutoPlayStartAudio();
			}
		}

		public virtual void OnGoalStart()
		{
			HasDone = false;
			isAchieveGoal = false;

			if (TaskManager.Instance.taskMode == TaskMode.Exam)
			{
				if (goalMode == GoalType.Study)
				{
					HasDone = true;
					isAchieveGoal = true;
					targetTask.AchieveGoal(true);
				}
			}
			else
			{
				if (goalMode == GoalType.Exam)
				{
					HasDone = true;
					isAchieveGoal = true;
					targetTask.AchieveGoal(true);
				}
			}



			AutoPlayStartAudio();

		}

		public virtual void AutoPlayStartAudio()
		{
			if (isAutoPlayStartAudio)
			{
				PlayStartAudio();
			}
		}



		/// <summary>
		/// 目标成功触发事件，一般时提示信息
		/// </summary>

		protected virtual void OnAchieveGoal_(bool success)
		{

		}

		TaskTimer _timer;
		protected virtual void DelayAchieveTaskGoal(bool success)
		{
			_timer = TaskTimerMgr.Instance.DestroyTimer(_timer);

			float _delayTime = 0;
			//语音
			if (!success)
			{
				if (TaskManager.Instance.taskMode == TaskMode.Exam)
				{
					PlayAudios(goalExaminationErrorAudioKeys);
					_delayTime = AudioManager.Instance.GetLocalLocalizationAudioLength(goalExaminationErrorAudioKeys);

				}
				else if (TaskManager.Instance.taskMode == TaskMode.Study)
				{
					PlayAudios(goalTrainingErrorAudioKeys);
					_delayTime = AudioManager.Instance.GetLocalLocalizationAudioLength(goalTrainingErrorAudioKeys);
				}
			}
			else
			{
				if (TaskManager.Instance.taskMode == TaskMode.Exam)
				{
					PlayAudios(goalExaminationSuccessAudioKeys);
					_delayTime = AudioManager.Instance.GetLocalLocalizationAudioLength(goalExaminationSuccessAudioKeys);

				}
				else if (TaskManager.Instance.taskMode == TaskMode.Study)
				{
					PlayAudios(goalTrainingSuccessAudioKeys);
					_delayTime = AudioManager.Instance.GetLocalLocalizationAudioLength(goalTrainingSuccessAudioKeys);
				}
			}

			//Debug.LogError("OnAchieveGoal_==" + goalName + "==Delay Time==" + _delayTime);

			if (_delayTime == 0)
			{
				DelayAchieveGoalAfterEndAudio(success);
			}
			else
			{
				_timer = TaskTimerMgr.Instance.CreateTimer(() =>
			   {
				   DelayAchieveGoalAfterEndAudio(success);
			   }, _delayTime);
			}

			//动画
			if (OnAchieveGoal != null)
				OnAchieveGoal(success);
		}
		//发送任务成为成功
		protected virtual void DelayAchieveGoalAfterEndAudio(bool success)
		{
			//targetTask.AchieveGoal(success);
			Debug.Log("DelayAchieveGoalAfterEndAudio===" + targetTask + "===" + goalName + "===" + success);
		}

		public bool CheckAchieveGoal()
		{
			return isAchieveGoal;
		}

		public bool CheckDone()
		{
			return hasDone;
		}

		protected bool BeforeCheck()
		{
			//已完成不再检测
			if (isAchieveGoal)
				return false;

			if (TaskManager.Instance.CurrentTask != targetTask)
			{
				return false;
			}

			if (TaskManager.Instance.CurrentTask == targetTask &&
					targetTask.taskState == TaskState.End)
			{
				return false;
			}

			return true;
		}


		/// <summary>
		/// 阶段前置任务显示
		/// </summary>
		public virtual void ShowPreTask()
		{

		}
		/// <summary>
		/// 阶段后置任务显示
		/// </summary>
		public virtual void HidePreTask()
		{

		}

#if UNITY_EDITOR
		[MenuItem("Assets/Create/VRTracing/TaskGoal/TaskGoalConf", false, 0)]
		static void CreateDynamicConf()
		{
			UnityEngine.Object obj = Selection.activeObject;
			if (obj)
			{
				string path = AssetDatabase.GetAssetPath(obj);
				ScriptableObject bullet = CreateInstance<TaskGoalConf>();
				if (bullet)
				{
					string confName = UnityUtil.TryGetName<TaskGoalConf>(path);
					AssetDatabase.CreateAsset(bullet, confName);
				}
				else
				{
					Debug.Log(typeof(TaskGoalConf) + " is null");
				}
			}
		}
#endif
	}
}
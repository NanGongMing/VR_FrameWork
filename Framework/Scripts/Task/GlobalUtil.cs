﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalUtil : System.Object
{
    static public SystemLanguage language = SystemLanguage.Chinese;
    static public string LanguageSuffix
    {
        get
        {
            string _result = "";
            switch (language)
            {
                case SystemLanguage.Chinese:
                    _result = "CN";
                    break;
                case SystemLanguage.English:
                    _result = "EN";
                    break;
                case SystemLanguage.German:
                    // Deutsch
                    _result = "DE";
                    break;
                default:
                    _result = "CN";
                    break;
            }
            return _result;
        }
    }
    static public string GetLocalizationName (string _name)
    {
        return $"{LanguageSuffix}_{_name}";
    }


    /// <summary>
    /// StreamingAssets
    /// </summary>
    static private string GetStreamingAssetsPath ()
    {
        #if UNITY_EDITOR_WIN
        return string.Concat (Application.streamingAssetsPath , "/");
        #elif UNITY_EDITOR_OSX
        return string.Concat("file://", Application.streamingAssetsPath, "/");
        #elif !UNITY_EDIOR && UNITY_ANDROID
        return string.Concat(Application.streamingAssetsPath, "/");
        #elif !UNITY_EDIOR && ( UNITY_IOS || UNITY_IPHONE )
        return string.Concat("file://", Application.streamingAssetsPath, "/");
        #else
        return string.Concat ( Application.streamingAssetsPath, "/" );
        #endif
    }

    static private string streamingAssetsPath;
    static public string StreamingAssetsPath
    {
        get
        {
            if (string.IsNullOrEmpty (streamingAssetsPath))
            {
                streamingAssetsPath = GetStreamingAssetsPath ();
            }
            return streamingAssetsPath;
        }
    }
}

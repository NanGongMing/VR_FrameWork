using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

namespace UnityFramework.Task
{
	public class QueueAudioSource : System.Object
	{
		public Queue<AudioSource> m_FreeLib = new Queue<AudioSource>();
	}
	public class AudioManager : MonoBehaviour
	{

		private static AudioManager mInstance;
		private static readonly object mStaticSyncRoot = new object();

		private Dictionary<string, AudioClip> mAudioPool = new Dictionary<string, AudioClip>();

		private Dictionary<string, QueueAudioSource> g_AudioSourcePool = new Dictionary<string, QueueAudioSource>();
		private List<AudioSource> mAudioSourcePool = new List<AudioSource>();
		private Dictionary<string, GameObject> mPrefabs = new Dictionary<string, GameObject>();

		private static float mVolumeSFX;
		private static float mVolumeBGM;

		private static float mVolumeSFXMax = 1.0f;
		private static float mVolumeBGMMax = 0.5f;

		public event Action OnPlayComplete;
		public event Action OnStopAll;

		private AudioSource mPlayingBGM;

		private AudioManager() { }

		public static AudioManager Instance
		{
			get
			{
				if (mInstance == null)
				{
					lock (mStaticSyncRoot)
					{
						if (mInstance == null)
						{
							GameObject singleton = GameObject.Find("TaskManager");
							if (singleton == null)
							{
								singleton = new GameObject("TaskManager");
							}
							mInstance = singleton.AddComponent<AudioManager>();

							if (PlayerPrefs.HasKey("VolumeSFX"))
							{
								mVolumeSFX = PlayerPrefs.GetFloat("VolumeSFX");
							}
							else
							{
								mVolumeSFX = mVolumeSFXMax;
								PlayerPrefs.SetFloat("VolumeSFX", mVolumeSFX);
							}
							if (PlayerPrefs.HasKey("VolumeBGM"))
							{
								mVolumeBGM = PlayerPrefs.GetFloat("VolumeBGM");
							}
							else
							{
								mVolumeBGM = mVolumeBGMMax;
								PlayerPrefs.SetFloat("VolumeBGM", mVolumeBGM);
							}
							mInstance.g_AudioSourcePool.Clear();
						}
					}
				}
				return mInstance;
			}
		}

		public float VolumeSFX
		{
			get
			{
				return mVolumeSFX;
			}
			set
			{
				mVolumeSFX = value;
			}
		}

		public float VolumeBGM
		{
			get
			{
				return mVolumeBGM;
			}
			set
			{
				mVolumeBGM = value;
				mPlayingBGM.volume = value;
			}
		}

		#region 播放StreamingAssets目录下文件

		//public void PlayStreamBGM(string _filename, string _sourcename, bool _loop = true)
		//{
		//    StartCoroutine(SyncPlayStreamBGM(_filename, _sourcename, _loop));
		//}

		//private IEnumerator SyncPlayStreamBGM(string _filename, string _sourcename, bool _loop = true)
		//{
		//    yield return null;
		//    AudioSource audioSource = GetAudioSource(_sourcename).GetComponent<AudioSource>();
		//    audioSource.priority = 0;

		//    yield return LoadStreamAssetSounds(_filename);

		//    AudioClip clip = mAudioPool[_filename];
		//    audioSource.clip = clip;
		//    audioSource.volume = mVolumeBGM;
		//    audioSource.loop = _loop;
		//    audioSource.Play();
		//    mPlayingBGM = audioSource;
		//}

		//IEnumerator LoadStreamAssetSounds(string _filename)
		//{
		//    yield return null;

		//    AudioClip clip = null;
		//    if (mAudioPool.ContainsKey(_filename))
		//    {
		//        clip = mAudioPool[_filename];
		//    }
		//    else
		//    {
		//        WWW www = new WWW(string.Concat(Application.streamingAssetsPath, "/Sounds/", _filename));

		//        yield return www;

		//        if (!string.IsNullOrEmpty(www.error))
		//        {
		//            Debug.Log("Load Stream Asset Sounds Fail !  " + _filename);
		//        }
		//        else
		//        {
		//            Debug.Log("Load Stream Asset Sounds Success !  " + _filename);
		//            clip = www.GetAudioClip();
		//            clip.name = _filename;
		//            mAudioPool[_filename] = clip;
		//        }
		//    }
		//}

		#endregion

		#region 播放本地音频

		public void PlayLocalAudio(string _filename)
		{
			AudioSource audioSource = GetDefaultAudioSource();

			AudioClip _clip = GetAudioClip("SFX", _filename);

			if (_clip == null)
			{
				return;
			}
			audioSource.clip = _clip;

			audioSource.volume = mVolumeSFX;
			audioSource.Play();
		}

		public void PlayLocalAudio(string _filename, string _path)
		{
			AudioSource audioSource = GetDefaultAudioSource();

			AudioClip _clip = GetAudioClip(_path, _filename);

			if (_clip == null)
			{
				return;
			}

			audioSource.clip = _clip;
			audioSource.volume = mVolumeSFX;
			audioSource.Play();
		}

		//public void PlayLocalAudioCallback(string _filename, string _path, bool _callBack = false)
		//{
		//	AudioSource audioSource = GetDefaultAudioSource();

		//	AudioClip _clip = GetAudioClip(_path, _filename);

		//	if (_clip == null)
		//	{
		//		return;
		//	}

		//	if(audioSource.isPlaying)
		//	{
		//		audioSource.Stop();
		//	}

		//	audioSource.clip = _clip;
		//	audioSource.volume = mVolumeSFX;
		//	audioSource.Play();

		//	if (_callBack)
		//	{
		//		if (IsInvoking("PalyComplete"))
		//		{
		//			CancelInvoke("PalyComplete");
		//		}
		//		//CancelInvoke ();
		//		Invoke("PalyComplete", audioSource.clip.length + 0.1f);
		//	}
		//}

		public void PlayLocalAudioCallback(string _fileName, string _path, bool _callBack = false)
		{
			if (string.IsNullOrEmpty(_fileName))
			{
				if (_callBack)
				{
					PalyComplete();
				}
				return;
			}
			Debug.LogError("PlayLocalLocalizationAudio" + _fileName);
			StopAll();

			AudioSource audioSource = GetDefaultAudioSource();

			AudioClip _clip = GetAudioClip(_path, GlobalUtil.GetLocalizationName(_fileName));

			if (_clip == null)
			{
				if (_callBack)
				{
					PalyComplete();
				}
				return;
			}

			audioSource.clip = _clip;

			audioSource.volume = mVolumeSFX;

			audioSource.Play();

			if (_callBack)
			{
				if (IsInvoking("PalyComplete"))
				{
					CancelInvoke("PalyComplete");
				}
				//CancelInvoke ();
				Invoke("PalyComplete", _clip.length + 0.2618f);
			}
		}

		public void PlayLocalAudio(string _filename, string _path, float _delay)
		{
			AudioSource audioSource = GetDefaultAudioSource();

			AudioClip _clip = GetAudioClip(_path, _filename);

			if (_clip == null)
			{
				return;
			}

			audioSource.clip = _clip;

			audioSource.volume = mVolumeSFX;
			audioSource.PlayDelayed(_delay);
		}

		public void PlayLocalAudio(string _filename, string _path, bool _loop)
		{
			if (string.IsNullOrEmpty(_filename))
			{
				return;
			}
			AudioSource audioSource = GetDefaultAudioSource();

			AudioClip _clip = GetAudioClip(_path, _filename);

			if (_clip == null)
			{
				return;
			}

			audioSource.clip = _clip;

			audioSource.loop = _loop;
			audioSource.volume = mVolumeSFX * 0.5f;
			audioSource.Play();
		}

		public void PlayLocalAudio(string _filename, string _path, float _delay, bool _loop)
		{
			AudioSource audioSource = GetDefaultAudioSource();

			AudioClip _clip = GetAudioClip(_path, _filename);

			if (_clip == null)
			{
				return;
			}

			audioSource.clip = _clip;

			audioSource.loop = _loop;
			audioSource.volume = mVolumeSFX;
			audioSource.PlayDelayed(_delay);
		}

		public void PlayLocalAudio(string _filename, float _delay)
		{
			AudioSource audioSource = GetDefaultAudioSource();

			AudioClip _clip = GetAudioClip("SFX", _filename);

			if (_clip == null)
			{
				return;
			}

			audioSource.clip = _clip;
			audioSource.volume = mVolumeSFX;
			audioSource.PlayDelayed(_delay);
		}

		public void PlayLocalAudio(string _filename, bool _loop)
		{
			AudioSource audioSource = GetDefaultAudioSource();

			AudioClip _clip = GetAudioClip("SFX", _filename);

			if (_clip == null)
			{
				return;
			}

			audioSource.clip = _clip;

			audioSource.loop = _loop;
			audioSource.volume = mVolumeSFX;
			audioSource.Play();
		}

		public void PlayLocalLocalizationAudio(string _fileName, float _delay = 0, bool _callBack = false)
		{
			if (string.IsNullOrEmpty(_fileName))
			{
				if (_callBack)
				{
					if (IsInvoking("PalyComplete"))
					{
						CancelInvoke("PalyComplete");
					}
					//CancelInvoke ();
					Invoke("PalyComplete", _delay);
				}
				return;
			}
			Debug.Log("PlayLocalLocalizationAudio" + _fileName);
			StopAll();

			AudioSource audioSource = GetDefaultAudioSource();

			AudioClip _clip = GetAudioClip("SFX", GlobalUtil.GetLocalizationName(_fileName));

			if (_clip == null)
			{
				if (_callBack)
				{
					if (IsInvoking("PalyComplete"))
					{
						CancelInvoke("PalyComplete");
					}
					//CancelInvoke ();
					Invoke("PalyComplete", _delay);
				}
				return;
			}

			audioSource.clip = _clip;

			audioSource.volume = mVolumeSFX;

			audioSource.PlayDelayed(_delay);


			if (_callBack)
			{
				if (IsInvoking("PalyComplete"))
				{
					CancelInvoke("PalyComplete");
				}
				//CancelInvoke ();
				Invoke("PalyComplete", audioSource.clip.length + _delay);
			}
		}

		public AudioSource PlayLocalLocalizationAudio(string _fileName)
		{
			if (string.IsNullOrEmpty(_fileName))
			{
				return null;
			}

			Debug.Log("PlayLocalLocalizationAudio" + _fileName);

			StopAll();

			AudioSource audioSource = GetDefaultAudioSource();

			AudioClip _clip = GetAudioClip("SFX", GlobalUtil.GetLocalizationName(_fileName));

			if (_clip == null)
			{
				return null;
			}

			audioSource.clip = _clip;

			audioSource.volume = mVolumeSFX;

			audioSource.Play();

			return audioSource;

		}

		public void PlayLocalLocalizationAudio(string _fileName, float _delay = 0)
		{
			if (string.IsNullOrEmpty(_fileName))
			{
				return;
			}
			Debug.LogError("PlayLocalLocalizationAudio" + _fileName);
			StopAll();

			AudioSource audioSource = GetDefaultAudioSource();

			AudioClip _clip = GetAudioClip("SFX", GlobalUtil.GetLocalizationName(_fileName));

			if (_clip == null)
			{
				return;
			}

			audioSource.clip = _clip;

			audioSource.volume = mVolumeSFX;

			audioSource.PlayDelayed(_delay);

		}

		public void PlayLocalLocalizationAudio(string _fileName, bool _callBack = false)
		{
			if (string.IsNullOrEmpty(_fileName))
			{
				if (_callBack)
				{
					PalyComplete();
				}
				return;
			}
			StopAll();

			AudioSource audioSource = GetDefaultAudioSource();

			AudioClip _clip = GetAudioClip("SFX", GlobalUtil.GetLocalizationName(_fileName));

			if (_clip == null)
			{
				if (_callBack)
				{
					PalyComplete();
				}
				return;
			}

			audioSource.clip = _clip;

			audioSource.volume = mVolumeSFX;

			audioSource.Play();


			if (_callBack)
			{
				if (IsInvoking("PalyComplete"))
				{
					CancelInvoke("PalyComplete");
				}
				//CancelInvoke ();
				Invoke("PalyComplete", _clip.length);
			}
		}

		public void PlayLocalLocalizationAudio(List<string> _fileNames, float _delay = 0)
		{
			if (_fileNames == null || _fileNames.Count < 1)
			{
				return;
			}
			Debug.Log("PlayLocalLocalizationAudio" + _fileNames.Count);
			StopAll();

			float _totalDelay = _delay;
			foreach (string _fileName in _fileNames)
			{
				AudioSource audioSource = GetDefaultAudioSource();

				AudioClip _clip = GetAudioClip("SFX", GlobalUtil.GetLocalizationName(_fileName));

				if (_clip == null)
				{
					return;
				}

				audioSource.clip = _clip;

				audioSource.volume = mVolumeSFX;

				audioSource.PlayDelayed(_totalDelay);

				_totalDelay += audioSource.clip.length + _delay;

			}
		}




		public float GetLocalLocalizationAudioLength(List<string> _fileNames, float _delay = 0)
		{
			float _totalDelay = _delay;
			foreach (string _fileName in _fileNames)
			{
				_totalDelay += GetLocalLocalizationAudioLength(_fileName) + _delay;
			}
			return _totalDelay;
		}

		public float GetLocalLocalizationAudioLength(string _fileName)
		{
			AudioSource audioSource = GetDefaultAudioSource();

			AudioClip _clip = GetAudioClip("SFX", GlobalUtil.GetLocalizationName(_fileName));

			if (_clip == null)
			{
				return 0;
			}
			return _clip.length;
		}


		public float GetAudioLength(string _path, string _fileName)
		{
			AudioClip _clip = GetAudioClip(_path, _fileName);
			return _clip == null ? 0 : _clip.length;
		}

		void PalyComplete()
		{
			//Debug.LogError ("PalyCompletePalyCompletePalyComplete" + Time.realtimeSinceStartup);
			OnPlayComplete?.Invoke();
		}
		public void LogAction()
		{
			Delegate[] _dls = OnPlayComplete.GetInvocationList();
			Debug.LogError("LogAction  " + _dls.Length);
			foreach (Delegate item in _dls)
			{
				Debug.LogError("Delegate ===" + item.Method + "==" + item.ToString());
			}
		}


		public AudioClip GetAudioClip(string _path, string _fileName)
		{
			if (string.IsNullOrEmpty(_fileName))
			{
				Debug.LogError("GetAudioClip AudioName is Null");
				return null;
			}

			//Debug.LogError("GetAudioClip===" + _path + "  " + _fileName);
			AudioClip clip = null;
			if (mAudioPool.ContainsKey(_fileName))
			{
				clip = mAudioPool[_fileName];
			}
			else
			{
				clip = Resources.Load(string.Concat("Sounds/", _path, "/", _fileName), typeof(AudioClip)) as AudioClip;

				if (clip == null)
				{
					Debug.LogError("Audio " + _fileName + " not exist !");
					return null;
				}
				mAudioPool[_fileName] = clip;
			}
			return clip;
		}

		#endregion

		public float GetSFXTime(string _fileName)
		{
			if (mAudioPool.ContainsKey(_fileName))
			{
				return mAudioPool[_fileName].length;
			}
			else
			{
				return 0;
			}
		}

		public void StopAll()
		{
			Debug.Log("StopAll");
			foreach (AudioSource audioSource in mAudioSourcePool)
			{
				AudioSource tempsource = audioSource;
				//Debug.LogError ("Stop All Audio " + tempsource.clip);
				tempsource.Stop();

				OnStopAll?.Invoke();

				//Delegate[] _list = OnPlayComplete.GetInvocationList ();
				//foreach (Delegate item in _list)
				//{
				//    if (item != null)
				//    {
				//        OnPlayComplete -= (Action)item;
				//    }
				//}
			}
		}

		public void Stop(string _clipname)
		{
			foreach (AudioSource audioSource in mAudioSourcePool)
			{
				if (audioSource.clip != null && audioSource.clip.name.Equals(_clipname))
				{
					audioSource.loop = false;
					audioSource.Stop();
					//Debug.Log ("Stop Audio " + _clipname);                
				}
			}
		}

		public void ClearAll()
		{
			foreach (var obj in g_AudioSourcePool)
			{
				obj.Value.m_FreeLib.Clear();
			}
			mPrefabs.Clear();
			mAudioSourcePool.Clear();
			mAudioPool.Clear();
		}

		//获得一个空闲的事件对象
		public AudioSource GetDefaultAudioSource()
		{
			return GetAudioSource("AudioSourceBGM");
		}

		public AudioSource GetAudioSource(string outName)
		{
			for (int i = 0; i < mAudioSourcePool.Count; i++)
			{
				AudioSource audioSource = mAudioSourcePool[i];
				if (audioSource == null)
				{
					mAudioSourcePool.RemoveAt(i);
					i--;
				}
				else
				{
					AudioSource tempsource = audioSource;
					if (!tempsource.isPlaying)
					{
						pushFreeAudioSource(audioSource, audioSource.name);
					}
					else
					{
					}
				}
			}

			AudioSource rv = null;
			QueueAudioSource _freeLib = null;
			if (g_AudioSourcePool.TryGetValue(outName, out _freeLib) == true)
			{
				if (_freeLib.m_FreeLib.Count > 0)
				{
					rv = _freeLib.m_FreeLib.Dequeue();
				}
			}
			if (rv == null)
			{
				GameObject _go = null;
				if (mPrefabs.ContainsKey(outName))
				{
					_go = (GameObject)GameObject.Instantiate(mPrefabs[outName]);
				}
				else
				{
					UnityEngine.Object obj = Resources.Load(string.Concat("Sounds/", outName), typeof(GameObject));
					_go = Instantiate(obj) as GameObject;
					_go.name = _go.name.Substring(0, _go.name.IndexOf("(Clone)"));
					mPrefabs.Add(outName, _go);
				}
				rv = _go.GetComponent<AudioSource>();
				rv.name = outName;
				mAudioSourcePool.Add(rv);
			}
			rv.loop = false;
			return rv;
		}

		//传入一个空闲的对象
		public void pushFreeAudioSource(AudioSource inAudioSource, string inName)
		{
			QueueAudioSource _freeLib = null;
			if (g_AudioSourcePool.TryGetValue(inName, out _freeLib) == false)
			{
				_freeLib = new QueueAudioSource();
				g_AudioSourcePool.Add(inName, _freeLib);
			}
			if (!_freeLib.m_FreeLib.Contains(inAudioSource))
			{
				_freeLib.m_FreeLib.Enqueue(inAudioSource);
			}
		}


	}

}


﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TaskState
{
	UnInit = 0,
	Init = 1,
	Start = 2,
	Doing = 3,
	End = 4
}
public enum TaskMode
{
	Default =0,
    Study=1,
    Exam=2
}
public enum TaskType
{
	All = 0,
	Study = 1,
	Exam = 2
}
public enum GoalType
{
	All = 0,
	Study = 1,
	Exam = 2
}
public enum SkipTaskType
{
	Default = 0,
}

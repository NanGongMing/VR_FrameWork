using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace UnityFramework.Task
{
	public class TaskManager : Singleton<TaskManager>
	{
		/// <summary>
		/// 第一个教学任务 新手引导
		/// </summary>
		public TaskConf firstTask;
		/// <summary>
		/// 最后一个结算任务
		/// </summary>
		public TaskConf finalTask;
		/// <summary>
		/// 所有的任务
		/// </summary>
		private List<TaskConf> allTasks;
		int allTaskCount = 0;

		/// <summary>
		/// 任务模式 
		/// </summary>
		[SerializeField]
		private TaskMode mTaskMode = TaskMode.Default;
		/// <summary>
		/// 任务记录
		/// </summary>
		public Dictionary<TaskMode, List<TaskConf>> dicRecordTasks;

		public event Action<TaskConf> OnTaskChange;

		private Coroutine coroutineDoing;
		private Coroutine coroutineDelayTask;



		/// <summary>
		/// 任务模式
		/// </summary>
		public TaskMode taskMode
		{
			get
			{
				return mTaskMode;
			}
			set
			{
				if (value != mTaskMode)
				{
					mTaskMode = value;
				}
			}
		}

		public bool IsExam
		{
			get
			{
				return mTaskMode == TaskMode.Exam;
			}
		}

		/// <summary>
		/// 当前任务
		/// </summary>
		private TaskConf mCurrentTask;
		public TaskConf CurrentTask
		{
			get
			{
				return mCurrentTask;
			}
			set
			{
				mCurrentTask = value;
			}
		}

		private void InitTasks()
		{
			//Debug.Log("——————————————————————————AllTaskInit--Start——————————————————————————");
			TaskConf nextTask = firstTask;
			while (nextTask != null)
			{
				nextTask.taskState = TaskState.UnInit;
				nextTask.taskState = TaskState.Init;

				//Debug.LogError ("Init Task===" + nextTask);
				nextTask = GetNextTaskLittle(nextTask);
			}
			//Debug.Log("——————————————————————————AllTaskInit--Over——————————————————————————");
		}




		protected override void Awake()
		{
			base.Awake();

			if (allTasks == null)
				allTasks = new List<TaskConf>();
			if (dicRecordTasks == null)
				dicRecordTasks = new Dictionary<TaskMode, List<TaskConf>>();

			ReadTasks(firstTask);
			RecordSkipTasks();
		}

		void Start()
		{
			Invoke("DelayStart", 1);
		}

		void DelayStart()
		{

			StartTask();


		}




		public void StartTask()
		{


			InitTasks();

			if (taskMode == TaskMode.Exam)
			{
				CurrentTask = firstTask;
			}
			else if (taskMode == TaskMode.Study)
			{
				CurrentTask = firstTask;
			}

			if (CurrentTask != null)
			{
				ExecuteTask(CurrentTask);
			}
		}

		/// <summary>
		/// 执行任务
		/// </summary>
		private void ExecuteTask(TaskConf taskConf)
		{
			if (CurrentTask == null)
				return;

			Debug.Log("Execute Task " + taskConf.taskName);

			TaskConf tempTask = CurrentTask;
			CurrentTask = taskConf;

			//首先判断该任务是否跳步，如果跳步，做跳步处理           
			int compareResult = CompareTaskPriority(taskConf, tempTask);

			//向前执行跳步
			if (compareResult < 0)
			{
				//Debug.LogError ("compareResult < 0");
				//重新设置两个任务之间的状态      
				TaskConf last = tempTask;
				while (last && last != taskConf)
				{
					last.taskState = TaskState.Init;
					last = GetLastTaskLittle(last);
					//last = GetLastNode (last);
				}


				CurrentTask.taskState = TaskState.Init;
				CurrentTask.taskState = TaskState.Start;
			}
			//向后执行跳步
			else if (compareResult > 0)
			{
				//跳步到这个任务之后的任务
				TaskConf next = tempTask;
				//Debug.LogError ("compareResult > 0" + next?.taskName + " ====  " + taskConf.taskName);
				while (next && next != taskConf)
				{
					//Debug.LogError ("compareResult > 0" + next?.taskName + " end  " + taskConf.taskName);

					next.taskState = TaskState.End;
					next = GetNextTaskLittle(next);
					//next = GetNextNode (next);
				}

				CurrentTask.taskState = TaskState.Start;
			}
			else
			{

				//Debug.LogError ("重新执行这个任务");
				//重新执行这个任务
				CurrentTask.taskState = TaskState.Init;
				CurrentTask.taskState = TaskState.Start;
			}

			OnTaskChange?.Invoke(CurrentTask);
		}

		private IEnumerator ExecuteTaskDelay(TaskConf taskConf)
		{
			//Debug.Log("Start ExecuteTaskDelay coroutine");
			yield return new WaitForSeconds(taskConf.delayTimeToStart);
			ExecuteTask(taskConf);
		}

		private void ResetTasks()
		{
			if (allTasks != null && allTasks.Count > 0)
			{
				// ==== 解决测试的时候挂载子节点，会把父节点isRead设置为true
				// ==== 但是父节点又不在allTask里
				TaskConf _parent = (TaskConf)allTasks[0].parent;
				if (_parent != null)
				{
					_parent.InitTask();
					_parent.taskState = TaskState.UnInit;
				}
				// ===

				foreach (TaskConf item in allTasks)
				{
					item.InitTask();
					item.taskState = TaskState.UnInit;
				}
			}
		}

		private void ReadTasks(TaskConf first)
		{
			if (!firstTask)
				return;

			allTasks.Clear();
			allTaskCount = 0;
			TaskConf nextTask = first;
			int index = 0;
			while (nextTask)
			{
				allTasks.Add(nextTask);
				nextTask.index = index++;

				//Debug.LogError ("read task==" + nextTask.taskName + " index " + nextTask.index);
				//         Debug.LogError ("parent==" + nextTask.parent + "==little brother=="
				//             + nextTask.littleBrother + "==old brother==" + nextTask.oldBrother
				//             + "==child==" + nextTask.child);
				nextTask = (TaskConf)nextTask.NextNode;
			}
			allTaskCount = allTasks.Count;
			//读完任务要重设状态
			ResetTasks();
		}

		public void StartDoingCoroutine(TaskConf taskConf)
		{
			//Debug.Log("StartDoingState");
			coroutineDoing = StartCoroutine(SetDoingState(taskConf));
		}

		public void StopDoingCoroutine()
		{
			if (coroutineDoing != null)
			{
				//Debug.Log("StopDoingState");
				StopCoroutine(coroutineDoing);
				coroutineDoing = null;
			}
		}

		private IEnumerator SetDoingState(TaskConf taskConf)
		{
			yield return new WaitForSeconds(taskConf.delayTimeStartToDoing);
			while (true)
			{
				taskConf.taskState = TaskState.Doing;
				yield return null;
			}
		}
		//执行上个小任务
		public void ExecuteLastTaskLittle()
		{
			if (coroutineDelayTask != null)
			{
				StopCoroutine(coroutineDelayTask);
				coroutineDelayTask = null;
			}

			if (CurrentTask)
			{
				TaskConf nextTaskLittle = GetLastTaskLittle(CurrentTask);

				if (nextTaskLittle != null)
				{

					coroutineDelayTask = StartCoroutine(ExecuteTaskDelay(nextTaskLittle));

				}
				else
				{
					//OnCompletAllTask();
				}
			}
		}
		/// <summary>
		/// 下个任务，包括小任务
		/// </summary>
		public void ExecuteNextTaskLittle()
		{
			if (coroutineDelayTask != null)
			{
				//Debug.Log("Stop ExecuteNextTask coroutine");
				StopCoroutine(coroutineDelayTask);
				coroutineDelayTask = null;
			}

			if (CurrentTask)
			{
				TaskConf nextTaskLittle = GetNextTaskLittle(CurrentTask);

				if (nextTaskLittle != null)
				{

					coroutineDelayTask = StartCoroutine(ExecuteTaskDelay(nextTaskLittle));

				}
				else
				{
					OnCompletAllTask();
				}
			}
		}

		public void ExecuteNextTaskBig()
		{
			if (coroutineDelayTask != null)
			{
				Debug.Log("Stop ExecuteNextTask coroutine");
				StopCoroutine(coroutineDelayTask);
				coroutineDelayTask = null;
			}

			if (CurrentTask)
			{
				TaskConf nextTaskBig = GetNextTaskBig(CurrentTask);

				if (nextTaskBig != null)
				{
					coroutineDelayTask = StartCoroutine(ExecuteTaskDelay(nextTaskBig));
				}
				else
				{
					coroutineDelayTask = StartCoroutine(ExecuteTaskDelay(allTasks[allTasks.Count - 1]));
					OnCompletAllTask();
				}

			}
		}

		/// <summary>
		/// 跳步到大任务
		/// </summary>
		/// <param name="taskConf"></param>
		public void SkipTask(TaskConf taskConf)
		{
			if (CurrentTask != null)
			{
				CurrentTask.IsPass = false;
			}
			if (coroutineDelayTask != null)
			{
				//Debug.Log("Stop ExecuteNextTask coroutine");
				StopCoroutine(coroutineDelayTask);
				coroutineDelayTask = null;
			}

			coroutineDelayTask = StartCoroutine(ExecuteTaskDelay(taskConf));
		}

		/// <summary>
		/// 强制完成当前任务
		/// 重置所有任务
		/// </summary>
		public void ForceStopAllTask()
		{
			AudioManager.Instance.StopAll();

			CurrentTask.taskState = TaskState.End;

			ResetTasks();

			OnCompletAllTask();
		}

		/// <summary>
		/// 获取任务的下一个节点，不考虑模式，只是下一个节点
		/// </summary>
		/// <param name="taskConf"></param>
		public TaskConf GetNextNode(TaskConf taskConf)
		{
			if (taskConf.index < allTasks.Count - 1)
			{
				return allTasks[taskConf.index + 1];
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获取任务的上一个节点，不考虑模式，只是上一个节点
		/// </summary>
		/// <param name="taskConf"></param>
		public TaskConf GetLastNode(TaskConf taskConf)
		{
			if (taskConf.index > 0)
			{
				return allTasks[taskConf.index - 1];
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 下一个小任务:考虑任务模式
		/// </summary>
		/// <returns></returns>
		public TaskConf GetNextTaskLittle(TaskConf taskConf)
		{
			TaskConf next = GetNextNode(taskConf);
			if (taskMode == TaskMode.Exam)
			{
				while (next != null)
				{
					if (next.taskType == TaskType.Study)
					{
						next = GetNextNode(next);
					}
					else
					{
						break;
					}
				}
				return next;
			}
			else if (taskMode == TaskMode.Study)
			{
				while (next != null)
				{
					if (next.taskType == TaskType.Exam)
					{
						next = GetNextNode(next);
					}
					else
					{
						break;
					}
				}
				return next;
			}
			else
			{
				return next;
			}
		}

		/// <summary>
		/// 上一个小任务:考虑任务模式ram>
		/// <returns></returns>
		public TaskConf GetLastTaskLittle(TaskConf taskConf)
		{
			TaskConf last = GetLastNode(taskConf);
			if (taskMode == TaskMode.Exam)
			{
				while (last != null)
				{
					if (last.taskType == TaskType.Study)
					{
						last = GetLastNode(last);
					}
					else
					{
						break;
					}
				}
				return last;
			}
			else if (taskMode == TaskMode.Study)
			{
				while (last != null)
				{
					if (last.taskType == TaskType.Exam)
					{
						last = GetLastNode(last);
					}
					else
					{
						break;
					}
				}
				return last;
			}
			else
			{
				return last;
			}
		}

		/// <summary>
		/// 下一个大任务:考虑任务模式
		/// </summary>
		/// <param name="taskConf"></param>
		/// <returns></returns>
		public TaskConf GetNextTaskBig(TaskConf taskConf)
		{
			TaskConf next = GetNextNode(taskConf);
			if (taskMode == TaskMode.Exam)
			{
				while (next != null)
				{
					if (next.taskType == TaskType.Study || next.isCanSkip)
					{
						next = GetNextNode(next);
					}
					else
					{
						break;
					}
				}
				return next;
			}
			else
			{
				while (next != null)
				{
					if (next.taskType == TaskType.Exam || next.isCanSkip)
					{
						next = GetNextNode(next);
					}
					else
					{
						break;
					}
				}
				return next;
			}
		}

		/// <summary>
		/// 重新开始
		/// </summary>
		public void RestartAllTask()
		{
			StartTask();

		}

		public void RestartTask()
		{
			RestartTask(CurrentTask);
		}

		/// <summary>
		/// 重置当前任务
		/// 往上找到最近一个不可跳过的任务
		/// </summary>
		/// <param name="taskConf"></param>
		public void RestartTask(TaskConf taskConf)
		{
			Debug.LogError("RestartTask " + taskConf.taskName);

			TaskConf nextTask = taskConf;
			// 将当前任务结束
			nextTask.taskState = TaskState.End;
			nextTask.HadReset = true;


			while (nextTask.isCanSkip)
			{
				// 将中间任务设置为初始状态
				nextTask.taskState = TaskState.UnInit;
				nextTask.taskState = TaskState.Init;
				nextTask.HadReset = true;

				nextTask = GetLastTaskLittle(nextTask);

				Debug.LogError("RestartTask not can Skip " + nextTask.taskName);
			}

			// 将重置任务设置为初始状态
			nextTask.taskState = TaskState.UnInit;
			nextTask.taskState = TaskState.Init;
			nextTask.HadReset = true;

			CurrentTask = nextTask;
			ExecuteTask(CurrentTask);
		}

		public void ForceCompleteTask()
		{
			CurrentTask.ForceCompleteTask();

			Debug.Log("ForceCompleteTask==>" + CurrentTask.taskName + "<==Skip To Next Task");
		}

		/// <summary>
		/// 完成所有任务
		/// </summary>
		private void OnCompletAllTask()
		{
			Debug.Log("所有任务已完成");



			if (IsExam)
			{


			}

		}






		protected override void OnDestroy()
		{
			ResetTasks();
			//Messenger.RemoveAllListener ();
			base.OnDestroy();
		}

		private void OnDisabled()
		{
			Debug.LogWarning("OnDisabled");
		}

		/// <summary>
		/// 传送
		/// </summary>
		/// <param name="tfDestination"></param>
		public void TeleportToTransform(Transform _tfDestination)
		{
			if (_tfDestination != null)
			{
				//playerTeleport.TeleportPlayerToTransform (_tfDestination);
				Vector3 _pos = _tfDestination.position;
				_pos.y += 1;
			}
		}

		/// <summary>
		/// 比较任务优先级，返回结果小于零 大于零 等于零
		/// </summary>
		private int CompareTaskPriority(TaskConf taskConf0, TaskConf taskConf1)
		{
			return taskConf0.index - taskConf1.index;
		}

		/// <summary>
		/// 记录不可跳转任务
		/// </summary>
		/// <returns></returns>
		private void RecordSkipTasks()
		{
			List<TaskConf> recordExaminationTasks = new List<TaskConf>();
			List<TaskConf> recordTrainingTasks = new List<TaskConf>();

			if (allTasks != null && allTasks.Count > 0)
			{
				foreach (TaskConf item in allTasks)
				{
					if (!item.isCanSkip && item.taskType != TaskType.Study)
						recordExaminationTasks.Add(item);

					if (!item.isCanSkip && item.taskType != TaskType.Exam)
						recordTrainingTasks.Add(item);
				}
			}



			dicRecordTasks.Add(TaskMode.Exam, recordExaminationTasks);
			dicRecordTasks.Add(TaskMode.Study, recordTrainingTasks);
		}

		/// <summary>
		/// 阶段所有任务
		/// </summary>
		/// <returns></returns>
		public List<TaskConf> GetAllStageTask()
		{
			List<TaskConf> _result = new List<TaskConf>();
			for (int i = 0; i < allTaskCount; i++)
			{
				if (allTasks[i].skipType == GlobalData.skipTaskType
					&& allTasks[i].taskType != TaskType.Exam
					&& !allTasks[i].isCanSkip)
				{
					_result.Add(allTasks[i]);
				}
			}
			return _result;
		}

		/// <summary>
		/// 获取任务
		/// </summary>
		/// <param name="index">任务index</param>
		/// <returns></returns>
		public TaskConf GetTaskByIndex(int index)
		{
			foreach (var item in allTasks)
			{
				if (item.index == index)
				{
					return item;
				}
			}
			Debug.LogError($"GetTaskByIndex: {index} 获取失败,return当前任务");
			return CurrentTask;
		}
		/// <summary>
		/// 执行指定任务
		/// </summary>
		/// <param name="index">任务index</param>
		public void ExecuteTaskByIndex(int index)
		{
			TaskConf temp = GetTaskByIndex(index);
			if (CurrentTask != temp)
			{


				if (coroutineDelayTask != null)
				{
					//Debug.Log("Stop ExecuteNextTask coroutine");
					StopCoroutine(coroutineDelayTask);
					coroutineDelayTask = null;
				}
				coroutineDelayTask = StartCoroutine(ExecuteTaskDelay(temp));
			}
		}



	}
}
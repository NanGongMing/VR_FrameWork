using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class TeleportConf : ScriptableObject
{
	public static Dictionary<string, TeleportConf> dic = new Dictionary<string, TeleportConf>();
	public string triggerName;

	[HideInInspector]
	public TeleportBasic triggerBasic;

#if UNITY_EDITOR
	[MenuItem("Assets/TeleportConf", false, 0)]
	static void CreateDynamicConf()
	{
		UnityEngine.Object obj = Selection.activeObject;
		if (obj)
		{
			string path = AssetDatabase.GetAssetPath(obj);
			ScriptableObject bullet = CreateInstance<TeleportConf>();
			if (bullet)
			{
				string confName = UnityUtil.TryGetName<TeleportConf>(path);
				AssetDatabase.CreateAsset(bullet, confName);
			}
			else
			{
				Debug.Log(typeof(TeleportConf) + " is null");
			}
		}
	}

#endif
}

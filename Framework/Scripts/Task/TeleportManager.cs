using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportManager : UnityFramework.Task.Singleton<TeleportManager>
{
	TeleportBasic[] teleportBasics;

	List<TeleportConf> teleportConfList = new List<TeleportConf>();

	/// <summary>
	/// 保证执行顺序
	/// 初始化之后再隐藏
	/// </summary>
	// Start is called before the first frame update
	protected override void Awake()
	{
		base.Awake();
		teleportBasics = transform.GetComponentsInChildren<TeleportBasic>(true);
		foreach (TeleportBasic item in teleportBasics)
		{
			item.InitComponent();
			item.HideObject();

			if (!teleportConfList.Contains(item.teleportConf))
			{
				teleportConfList.Add(item.teleportConf);
			}
		}
	}

	public void ShowTeleport(TeleportConf _conf)
	{
		if (teleportConfList.Contains(_conf))
		{
			_conf.triggerBasic.ShowObject();
		}
	}

	public void HideTeleport(TeleportConf _conf)
	{
		if (teleportConfList.Contains(_conf))
		{
			_conf.triggerBasic.HideObject();
		}
	}
	public TeleportBasic GetNewPart()
	{
		for (int i = 0; i < teleportBasics.Length; ++i)
		{
			if (teleportBasics[i].gameObject.name == "TeleportDestination_NewPart")
			{
				return teleportBasics[i];
			}
		}
		return null;
	}
	public TeleportBasic GetCar()
	{
		for (int i = 0; i < teleportBasics.Length; ++i)
		{
			if (teleportBasics[i].gameObject.name == "TeleportDestination_Car")
			{
				return teleportBasics[i];
			}
		}
		return null;
	}
	public void HideAll()
	{
		foreach (var u in teleportBasics)
		{
			u.HideObject();
		}
	}
}

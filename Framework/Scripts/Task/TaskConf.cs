﻿using System;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityFramework.Task;
public enum GoalReslut
{
	Init = -1,
	Error = 0,
	Sucess = 1
}
/// <summary>
/// 流程没有问题了，如何根据任务类型来设定关系
/// </summary>
public class TaskConf : LinkConf
{
	/// <summary>
	/// 任务的名称
	/// </summary>
	public string taskName;
	/// <summary>
	/// 此节点的下标
	/// </summary>
	public int index;

	/// <summary>
	/// 得分 key
	/// </summary>
	[Header("ScoreTitle ")]
	public string scoreTitle;

	/// <summary>
	/// 任务 id
	/// </summary>
	[Header("Title")]
	public string keyTitle;

	/// <summary>
	/// 任务是否可以跳步或者记录
	/// 可跳过为true
	/// 不可跳过false
	/// </summary>
	[Header("任务跳步")]
	public bool isCanSkip = false;
	/// <summary>
	/// 任务是否完成
	/// </summary>
	private bool isPass = false;

	/// <summary>
	/// 任务类型
	/// </summary>
	public TaskType taskType;

	/// <summary>
	/// 体验模式跳步类型
	/// </summary>
	[Header("体验模式跳步类型")]
	public SkipTaskType skipType;

	[Header("任务结束时清理工具栏")]
	public bool clearFastTools = false;
	/// <summary>
	/// 任务目标，拆分任务，所有任务目标完成即认为完成任务
	/// </summary>
	public List<TaskGoalConf> taskGoals;

	/// <summary>
	/// 任务未开始状态
	/// </summary>
	public event Action<TaskConf> OnTaskInit;
	/// <summary>
	/// 任务开始时事件
	/// </summary>
	public event Action<TaskConf> OnTaskStart;
	/// <summary>
	/// 任务执行中事件
	/// </summary>
	public event Action<TaskConf> OnTaskDoing;
	/// <summary>
	/// 任务结束时事件
	/// </summary>
	public event Action<TaskConf> OnTaskEnd;
	/// <summary>
	/// 任务延迟进入doing状态时间
	/// </summary>
	public float delayTimeStartToDoing = 0;
	/// <summary>
	/// 任务延迟Start时间
	/// </summary>
	public float delayTimeToStart = 0;


	/// <summary>
	/// 是否在测试模式任务失败时播放语音提示
	/// </summary>
	public bool isPlayAudioOnErrorInExaming = false;

	/// <summary>
	/// 是否播放任务失败语音提示
	/// </summary>
	public bool isPlayAudioOnError = true;

	/// <summary>
	/// 任务失败语音提示
	/// </summary>
	[Header("任务失败语音,会等播放完成")]
	public List<string> audioOnErrorKeys;
	/// <summary>
	/// 任务成功语音提示
	/// </summary>
	[Header("任务成功语音,会等播放完成")]
	public List<string> successTaskAudioKeys;

	/// <summary>
	/// 是否播放任务成功语音提示
	/// </summary>    
	public bool isPlayAudioSuccessTask = true;

	/// <summary>
	/// 任务启动时语音提示
	/// </summary>
	[Header("训练任务开始语音")]
	public List<string> startAudioKeys;
	[Header("考核模式任务开始语音")]
	public List<string> startExamAudioKeys;

	private List<string> _realStartAudioeys;

	/// <summary>
	/// 任务完成事件
	/// </summary>
	public event Action<TaskConf> OnTaskAchieve;
	/// <summary>
	/// 将结果异常的任务目标通知出去，异常的任务目标是指：做错的任务，没做但是必要要做的任务
	/// </summary>
	public event Action<TaskGoalConf> OnDoErrorGoal;

	public event Action OnAchiveUnDone;
	/// <summary>
	/// 目标失败后是否自动跳入下一Brother任务
	/// </summary>    
	public bool isJumpBigOnGoalError = false;

	/// <summary>
	/// 任务是否只是播放语音提示
	/// 只播语音，播完进入下一任务，不需要目标
	/// </summary>
	[Header("只播语音，播完进入下一任务，不需要Goal")]
	public bool isJustPlayAudio = false;

	/// <summary>
	/// 是否开启光圈
	/// </summary>
	[Header("开始时是否开启任务提示光圈")]
	public bool isShowTeleport = true;

	/// <summary>
	/// 是否隐藏光圈
	/// </summary>
	[Header("结束时是否隐藏任务提示光圈")]
	public bool isHideTeleport = true;

	/// <summary>
	/// 是否启用机器人
	/// </summary>
	[Header("是否启用机器人")]
	public bool isShowRobot = false;
	/// <summary>
	/// 机器人循环语音
	/// </summary>
	[Header("机器人循环语音")]
	public string robotAudioKey;


	public float reDoTime = 10;
	private bool isFirstReDo = true;
	private bool isHasPlayEffect = false;


	private bool hasReset = false;
	public bool HadReset
	{
		get
		{
			return hasReset;
		}
		set
		{
			hasReset = value;
		}
	}

	/// <summary>
	/// 当一个任务目标完成时，通知任务系统
	/// 如果此目标的结果是对的，则遍历任务是否存在尚未执行的任务目标
	/// 如果全部目标都已完成，则此任务结束。如果存在未操作的任务目标，则返回，继续完成目标
	/// 如果此目标错误，则记录目标
	/// </summary>
	/// <param name="success"></param>
	public void AchieveGoal(bool success, bool isForceJump = false)
	{
		dynTimer = TaskTimerMgr.Instance.DestroyTimer(dynTimer);

		///如果先做了最后做的任务
		bool isDoLast = false;
		bool isAllHasDone = true;
		foreach (var result in taskGoals)
		{
			if (result.isFinalGoal)
			{
				if (result.HasDone)
				{
					isDoLast = true;
				}
			}
			else
			{
				if (!result.HasDone)
				{
					isAllHasDone = false;
					break;
				}
			}
		}
		bool isReDoAll = false;
		///如果做了最后一步，但是其他的没做完
		if (isDoLast && !isAllHasDone)
		{
			success = false;
			isForceJump = true;
			///如果是考试模式，播放对应的错误动效，然后结束任务
			if (TaskManager.Instance.taskMode == TaskMode.Exam)
			{
			}
			///如果是培训模式，播放对应的错误动效，然后引导错误流程
			else
			{
				isReDoAll = true;
			}
		}

		foreach (var result in taskGoals)
		{
			///存在没有完成的任务
			if (!result.HasDone)
			{
				result.ReStartGoal();

				///如果是非强制跳跃，代表继续执行此任务
				if (!isForceJump)
				{
					return;
				}
				success = false;
			}
			else if (!result.CheckAchieveGoal())
			{
				success = false;
			}
		}
		float dynShowTime = 0;
		if (!success && isPlayAudioOnError
			&& audioOnErrorKeys != null && audioOnErrorKeys.Count > 0)
		{
			if (isPlayAudioOnErrorInExaming || TaskManager.Instance.taskMode != TaskMode.Exam)
			{
				PlayAudios(audioOnErrorKeys);
				dynShowTime = UnityFramework.Task.AudioManager.Instance.GetLocalLocalizationAudioLength(audioOnErrorKeys);
				Debug.Log("Play Task Error Auido" + dynShowTime);
			}
		}

		if (success && isPlayAudioSuccessTask
			&& successTaskAudioKeys != null && successTaskAudioKeys.Count > 0)
		{
			if (TaskManager.Instance.taskMode != TaskMode.Exam)
			{
				PlayAudios(successTaskAudioKeys);
				dynShowTime = UnityFramework.Task.AudioManager.Instance.GetLocalLocalizationAudioLength(successTaskAudioKeys);
				Debug.Log("Play Task Success  Auido" + dynShowTime);
			}
		}
		if (isReDoAll)
		{
			float wtime = isFirstReDo ? reDoTime : 0;
			TaskTimerMgr.Instance.CreateTimer(() =>
			{
				isFirstReDo = false;
				OnAchiveUnDone?.Invoke();
				foreach (var result in taskGoals)
				{
					if (!result.HasDone)
					{
						result.ForceTip();
						return;
					}
				}
			}, wtime);
			return;
		}
		//dynTimer = TimerMgr.Instance.DestroyTimer(dynTimer);
		if (dynShowTime == 0)
		{
			DoNextTask(success);
		}
		else
		{
			dynTimer = TaskTimerMgr.Instance.CreateTimer(() =>
		   {
			   DoNextTask(success);
		   }, dynShowTime);
		}
	}

	/// <summary>
	/// 培训模式下，将会依次执行每一个步骤
	/// 考试模式下，如果做错，跳过此任务，执行下一个任务
	/// </summary>
	/// <param name="isAllComplete"></param>
	public void DoNextTask(bool isAllComplete)
	{
		dynTimer = TaskTimerMgr.Instance.DestroyTimer(dynTimer);
		this.IsPass = isAllComplete;
		TaskManager.Instance.CurrentTask.taskState = TaskState.End;
		if (TaskManager.Instance.taskMode == TaskMode.Exam)
		{
			if (!isAllComplete && isJumpBigOnGoalError)
			{
				TaskManager.Instance.ExecuteNextTaskBig();
			}
			else
			{
				TaskManager.Instance.ExecuteNextTaskLittle();
			}
		}
		else
		{
			TaskManager.Instance.ExecuteNextTaskLittle();
		}
	}

	private void PlayAudios(List<string> audios)
	{
		if (audios != null)
		{
			UnityFramework.Task.AudioManager.Instance.PlayLocalLocalizationAudio(audios);
		}
	}

	private void SortGoals()
	{
		//Debug.Log(TaskManager.Instance.CurrentTask + "-----" + "SortGoals");
		taskGoals.Sort((x, y) =>
		{
			if (x.priority > y.priority)
				return 1;
			else if (x.priority < y.priority)
				return -1;
			else
				return 0;
		});
	}

	private void DoTaskRight()
	{
		//所有任务目标已达成,正确执行任务
		Debug.LogFormat("成功完成任务:{0} Index:{1}", this.taskName, this.index);
		this.IsPass = true;
		TaskManager.Instance.CurrentTask.taskState = TaskState.End;
		TaskManager.Instance.ExecuteNextTaskLittle();
	}

	private void DoTaskError()
	{
		//考试模式直接执行下一个任务，培训模式则不处理
		if (TaskManager.Instance.taskMode == TaskMode.Exam)
		{
			Debug.LogFormat("失败完成任务:{0} Index:{1}", this.taskName, this.index);
			this.IsPass = false;
			TaskManager.Instance.CurrentTask.taskState = TaskState.End;
			TaskManager.Instance.ExecuteNextTaskBig();
		}
	}

	//事件源，供内部调用
	private void _OnTaskInit(TaskConf taskConf)
	{
		//Debug.LogFormat("任务:{0} TaskInit Index:{1} 时间：{2}", this.taskName, this.index, Time.time);

		//初始化
		IsPass = false;
		isFirstReDo = true;
		hasReset = false;



		SortGoals();



		//Debug.LogError (taskName);
		if (taskGoals != null && taskGoals.Count > 0)
		{
			foreach (TaskGoalConf item in taskGoals)
			{
				item?.OnTaskInit(this);
				//Debug.LogError (item.goalName);
			}



		}


		if (OnTaskInit != null)
		{
			OnTaskInit(taskConf);
		}
	}

	private void _OnTaskStart(TaskConf taskConf)
	{
		dynTimer = TaskTimerMgr.Instance.DestroyTimer(dynTimer);



		// 播放开始语音
		// 考试模式和训练模式开始语音不一样
		if (TaskManager.Instance.taskMode == TaskMode.Exam)
		{
			_realStartAudioeys = startExamAudioKeys;
		}
		else
		{
			_realStartAudioeys = startAudioKeys;
		}
		float _dynShowTime = 0;
		if (_realStartAudioeys.Count != 0)
		{
			PlayAudios(_realStartAudioeys);


			_dynShowTime = UnityFramework.Task.AudioManager.Instance.GetLocalLocalizationAudioLength(_realStartAudioeys);
			dynTimer = TaskTimerMgr.Instance.CreateTimer(() =>
		   {
			   OnPlayStartAudioComplete();

		   }, _dynShowTime);
		}
		else
		{
			OnPlayStartAudioComplete();
		}




		isHasPlayEffect = false;
		isFirstReDo = true;

		TaskManager.Instance.StartDoingCoroutine(this);


		if (taskGoals != null && taskGoals.Count > 0)
		{
			foreach (TaskGoalConf item in taskGoals)
			{
				item?.OnTaskStart(this);
			}
		}

		//通知工具系统
		if (OnTaskStart != null)
		{
			OnTaskStart(taskConf);
		}

		if (taskGoals == null || taskGoals.Count == 0)
		{
			if (isJustPlayAudio)
			{
				_dynShowTime = UnityFramework.Task.AudioManager.Instance.GetLocalLocalizationAudioLength(_realStartAudioeys);
				dynTimer = TaskTimerMgr.Instance.CreateTimer(() =>
			   {
				   AchieveGoal(true);
			   }, _dynShowTime);
			}
			else
			{
				AchieveGoal(true);
			}
		}
	}

	/// <summary>
	/// 取消
	/// </summary>
	/// <param name="taskConf"></param>
	private void DelayStart(TaskConf taskConf)
	{
		dynTimer = TaskTimerMgr.Instance.DestroyTimer(dynTimer);
		isHasPlayEffect = false;
		isFirstReDo = true;

		Debug.LogError("Delay Start");

		TaskManager.Instance.StartDoingCoroutine(this);

		if (taskGoals != null && taskGoals.Count > 0)
		{
			foreach (TaskGoalConf item in taskGoals)
			{
				item?.OnTaskStart(this);
			}
		}

		//通知工具系统
		if (OnTaskStart != null)
		{
			OnTaskStart(taskConf);
		}

		if (taskGoals == null || taskGoals.Count == 0)
		{
			if (isJustPlayAudio)
			{
				AchieveGoal(true);
			}
			else
			{
				AchieveGoal(true);
			}
		}
	}

	private void OnPlayStartAudioComplete()
	{
		Debug.Log(TaskManager.Instance.CurrentTask + "-----" + "OnPlayStartAudioComplete");
		if (TaskManager.Instance.CurrentTask == this)
		{
			if (taskGoals != null && taskGoals.Count > 0)
			{
				int _count = taskGoals.Count;
				for (int i = 0; i < _count; i++)
				{
					taskGoals[i].OnTaskPlayStartAudioComplete(this);
				}
			}
		}
	}

	private void _OnTaskDoing(TaskConf taskConf)
	{

		if (taskGoals != null && taskGoals.Count > 0)
		{
			foreach (TaskGoalConf item in taskGoals)
			{
				item?.OnTaskDoing(this);
			}
		}

		if (OnTaskDoing != null)
		{
			OnTaskDoing(taskConf);
		}
	}

	private void _OnTaskEnd(TaskConf taskConf)
	{
		Debug.Log($"===Task End==>Name==>{taskConf.taskName}==index=={taskConf.index}");

		dynTimer = TaskTimerMgr.Instance.DestroyTimer(dynTimer);







		if (taskGoals != null && taskGoals.Count > 0)
		{
			foreach (TaskGoalConf item in taskGoals)
			{
				item?.OnTaskEnd(this);
			}
		}

		if (OnTaskEnd != null)
		{
			OnTaskEnd(taskConf);
		}



	}

	//任务状态
	private TaskState mTaskState = TaskState.UnInit;
	private TaskTimer dynTimer;
	public TaskState taskState
	{
		get
		{
			return mTaskState;
		}
		set
		{
			TaskState oldState = mTaskState;
			mTaskState = value;
			if (mTaskState != TaskState.Doing)
				if (TaskManager.Instance)
					TaskManager.Instance.StopDoingCoroutine();
			switch (mTaskState)
			{
				case TaskState.Init:
					if (oldState != TaskState.Init)
					{
						_OnTaskInit(this);
						//Debug.Log("_OnTaskInit------------------------------"+this);
					}
					break;
				case TaskState.Start:
					if (oldState != TaskState.Start)
					{
						_OnTaskStart(this);
						//Debug.Log("_OnTaskStart------------------------------" + this);
					}
					break;
				case TaskState.Doing:
					_OnTaskDoing(this);
					break;
				case TaskState.End:
					if (oldState != TaskState.End)
					{
						_OnTaskEnd(this);
						//Debug.Log("_OnTaskEnd------------------------------" + this);
					}
					break;
				default:
					break;
			}
		}
	}

	public bool IsPass
	{
		get
		{
			if (child)
			{
				if (!(child as TaskConf).isPass)
				{
					return false;
				}
				if (child.littleBrother)
				{
					if (!(child.littleBrother as TaskConf).isPass)
					{
						return false;
					}
					if (child.littleBrother.littleBrother)
					{
						if (!(child.littleBrother.littleBrother as TaskConf).isPass)
						{
							return false;
						}
					}
				}
			}
			return isPass;
		}
		set
		{
			isPass = value;
		}
	}




	/// <summary>
	/// 强制完成任务
	/// 检测并记录目标是否完成
	/// </summary>
	public virtual void ForceCompleteTask()
	{
		dynTimer = TaskTimerMgr.Instance.DestroyTimer(dynTimer);

		Debug.Log("ForceCompleteTask====" + TaskManager.Instance.CurrentTask.taskName);
		UnityFramework.Task.AudioManager.Instance.StopAll();
		if (taskGoals == null || taskGoals.Count < 1)
		{
			AchieveGoal(true);
		}

		foreach (TaskGoalConf item in taskGoals)
		{
			item.ForceCompleteTask(this);
		}
	}





#if UNITY_EDITOR
	[MenuItem("Assets/Create/VRTracing/TaskConf", false, 0)]
	static void CreateDynamicConf()
	{
		UnityEngine.Object obj = Selection.activeObject;
		if (obj)
		{
			string path = AssetDatabase.GetAssetPath(obj);
			ScriptableObject bullet = CreateInstance<TaskConf>();
			if (bullet)
			{
				string confName = UnityUtil.TryGetName<TaskConf>(path);
				AssetDatabase.CreateAsset(bullet, confName);
			}
			else
			{
				Debug.Log(typeof(TaskConf) + " is null");
			}
		}
	}

#endif
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerBasic : MonoBehaviour
{
	[Header("触发器用途,只用来备注")]
	public List<string> triggerType;

	[Header("触发器配置")]
	public TriggerConf triggerConf;

	public enum HighLightAutoAddType
	{
		AddToSelf,
		AddToChild,
		AddToParent,
		NoHighlight,
		AddToAllChild,
	}
	[Header("当前物体上没有HL时执行")]
	public HighLightAutoAddType highLightAutoAddType = HighLightAutoAddType.AddToSelf;
	//  protected TriggerHighLight highLight;

	protected Collider[] colliders;

	public event Action<Collider, TriggerBasic> _OnTriggerEnter;
	public event Action<Collider, TriggerBasic> _OnTriggerExit;
	public event Action<Collider, TriggerBasic> _OnTriggerStay;

	public event Action _OnPlayAnimationComplete;
	public class BeginWaitPlayAnimationComplete_proxy : System.IDisposable
	{
		private TriggerBasic Owner;
		private Action wait_fun;
		public BeginWaitPlayAnimationComplete_proxy(TriggerBasic Owner_in, Action wait_fun_in)
		{
			Owner = Owner_in;
			wait_fun = wait_fun_in;
			Owner._OnPlayAnimationComplete += wait_fun;
		}
		public void Dispose()
		{
			Owner._OnPlayAnimationComplete -= wait_fun;
		}
		~BeginWaitPlayAnimationComplete_proxy()
		{
			Dispose();
		}
	};
	public IEnumerator BeginWaitPlayAnimationComplete()
	{
		bool ok = false;
		using (var tmp = new BeginWaitPlayAnimationComplete_proxy(this, () => { ok = true; }))
		{
			while (!ok)
			{
				if (!gameObject.activeInHierarchy)
				{
					break;
				}
				yield return null;
			}
		}
	}

	[Header("目标工具触发器配置文件")]
	public List<TriggerConf> targetToolTriggerConfs;

	bool isInit;
	// 不能使用Awake
	protected virtual void Awake()
	{
		if (!isInit)
		{
			InitComponent();
		}
	}

	public virtual void InitComponent()
	{
		if (isInit)
		{
			return;
		}
		isInit = true;

		if (triggerConf != null)
		{
			triggerConf.triggerBasic = this;
		}
		colliders = gameObject.GetComponents<Collider>();


		InitHighLight();

		DisableCollider();

		//  
		_OnAwake();

		//Debug.LogError ("Init Trigger" + triggerConf.triggerName);
	}

	void InitHighLight()
	{
		//if(highLightAutoAddType != HighLightAutoAddType.NoHighlight)
		//{
		//	highLight = gameObject.GetComponent<TriggerHighLight> ();
		//	if (highLight == null)
		//	{
		//		if (highLightAutoAddType == HighLightAutoAddType.AddToChild)
		//		{
		//			highLight = transform.gameObject.GetComponentInChildren<TriggerHighLight> ();
		//			if (highLight == null)
		//			{
		//				highLight = transform.GetChild (0).gameObject.AddComponent<TriggerHighLight> ();
		//			}
		//		}
		//		else if (highLightAutoAddType == HighLightAutoAddType.AddToParent)
		//		{
		//			highLight = transform.parent.gameObject.GetComponent<TriggerHighLight> ();
		//			if (highLight == null)
		//			{
		//				highLight = transform.parent.gameObject.AddComponent<TriggerHighLight> ();
		//			}
		//		}
		//		else if (highLightAutoAddType == HighLightAutoAddType.AddToAllChild) {
		//                  for (int i = 0; i < transform.childCount; i++) {
		//				highLight = transform.GetChild(i).gameObject.AddComponent<TriggerHighLight>();
		//				highLight.InitComponent();
		//                  }					
		//		}
		//		else
		//		{
		//			highLight = gameObject.AddComponent<TriggerHighLight> ();               
		//		}
		//	}
		//	highLight.InitComponent ();
		//}
	}

	protected virtual void _OnAwake()
	{

	}


	protected virtual void OnTriggerEnter(Collider other)
	{
		_OnTriggerEnter?.Invoke(other, this);
	}

	protected virtual void OnTriggerStay(Collider other)
	{
		_OnTriggerStay?.Invoke(other, this);
	}

	protected virtual void OnTriggerExit(Collider other)
	{
		//Debug.LogError ("game===" + gameObject.name);
		_OnTriggerExit?.Invoke(other, this);
	}

	protected virtual void PlayAnimationComplete()
	{
		Debug.LogError(triggerConf + "PlayAnimationComplete");
		_OnPlayAnimationComplete?.Invoke();
	}

	//public virtual void ShowHighLight(bool _showBody, 
	//       TriggerHighLight.HighlightType _hlType)
	//{
	//	//Debug.LogError ("ShowHighLight" + triggerConf.triggerName);
	//	gameObject.SetActive(true);
	//	if (highLight != null)
	//	{
	//		highLight.ShowLight(_showBody, _hlType);
	//	}
	//}

	//public virtual void ShowHighLight (bool _showBody = false )
	//{
	//    //Debug.LogError ("ShowHighLight" + triggerConf.triggerName);
	//    gameObject.SetActive (true);
	//    if (highLight != null)
	//    {
	//        highLight.ShowLight (_showBody);
	//    }
	//}

	public virtual void ShowHighLight(float flashFrequency, bool _showBody = false)
	{
		gameObject.SetActive(true);
		//if (highLight != null)
		//{
		//    highLight.ShowLight (flashFrequency, _showBody, TriggerHighLight.HighlightType.NoOverlayNoSeeThrough);
		//}
	}

	//public virtual void ShowHighLight (float flashFrequency , bool _showBody = false, TriggerHighLight.HighlightType _hlt = TriggerHighLight.HighlightType.NoOverlayNoSeeThrough)
	//{
	//    gameObject.SetActive (true);
	//    //if (highLight != null)
	//    //{
	//    //    highLight.ShowLight (flashFrequency , _showBody , _hlt);
	//    //}
	//}

	public virtual void HideHighLight(bool _showBody = false)
	{
		//if (highLight != null)
		//{
		//    //Debug.LogError ("HideHighLight" + triggerConf.triggerName);
		//	highLight.HideLight(_showBody);
		//}
	}

	public void HideHighLightMesh()
	{
		//if (highLight != null)
		//{
		//	highLight.HideMeshRender();
		//}
	}

	public void ShowHighLightMesh()
	{
		//if (highLight != null)
		//{
		//	highLight.ShowMeshRender();
		//}
	}

	public void ShowTrigger()
	{
		//Debug.LogError ("ShowTrigger" + gameObject.name);
		EnableCollider();
		if (!gameObject.activeInHierarchy)
		{
			gameObject.SetActive(true);
		}
	}

	public void HideTrigger()
	{
		//Debug.LogError ("HideTrigger" + gameObject.name);
		DisableCollider();
		if (gameObject.activeInHierarchy)
		{
			gameObject.SetActive(false);
		}
	}

	public virtual void EnableCollider()
	{
		//Debug.LogError ("EnableCollider" + triggerConf.triggerName);
		if (colliders == null)
		{
			return;
		}
		foreach (Collider item in colliders)
		{
			item.enabled = true;
		}
	}

	public virtual void DisableCollider()
	{
		//Debug.LogError ("DisableCollider" + triggerConf.triggerName);
		if (colliders == null)
		{
			return;
		}
		foreach (Collider item in colliders)
		{
			item.enabled = false;
		}
	}

	public virtual void OnReset()
	{
		HideHighLight();
		HideHighLightMesh();
		DisableCollider();
	}
}
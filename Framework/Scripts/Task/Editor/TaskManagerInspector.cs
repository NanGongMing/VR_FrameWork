using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityFramework.Task;

[CustomEditor(typeof(TaskManager))]
public class TaskManagerInspector : Editor
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();
		TaskManager CurTaskManager = (TaskManager)target;
		TaskConf CurTaskConf = CurTaskManager.CurrentTask;
		if (CurTaskConf == null)
		{
			return;
		}
		EditorGUILayout.LabelField(CurTaskConf.index.ToString() + " : " + CurTaskConf.taskName);
	}
}

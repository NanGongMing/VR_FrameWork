using UnityEngine;
using UnityFramework.Data;
using UnityFramework.Runtime;
using UnityFramework.Scene;
using UnityFramework.Timer;
using UnityFramework.UI;
using UnityFramework.WebRequest;

namespace UnityFramework
{
    public class Framework : MonoBehaviour
    {

        static bool IsInit;

        public static UIComponent UI
        {
            get;
            private set;
        }
        public static AudioComponent Audio
        {
            get;
            private set;
        }
        public static TimerComponent Timer
        {
            get;
            private set;
        }
        public static DataComponent Data
        {
            get;
            private set;
        }

        public static EventComponent Event
        {
            get;
            private set;
        }
        public static UnityWebRequestComponent UnityWebRequest
        {
            get;
            private set;
        }
        public static SceneComponent Scene
        {
            get;
            private set;
        }
        public static AnimComponent Anim
        {
            get;
            private set;
        }
        public bool IsDontDestroyOnLoad;

        private void Start()
        {
            if (!IsInit)
            {
                Init();
            }
            else
            {
                Destroy(gameObject);
            }

        }


        void Init()
        {
            IsInit = true;
            UI = Runtime.UnityFrameworkEntry.GetComponent<UIComponent>();
            Audio = Runtime.UnityFrameworkEntry.GetComponent<AudioComponent>();
            Timer = Runtime.UnityFrameworkEntry.GetComponent<TimerComponent>();
            Data = Runtime.UnityFrameworkEntry.GetComponent<DataComponent>();
            Event = Runtime.UnityFrameworkEntry.GetComponent<EventComponent>();
            UnityWebRequest = Runtime.UnityFrameworkEntry.GetComponent<UnityWebRequestComponent>();
            Scene = Runtime.UnityFrameworkEntry.GetComponent<SceneComponent>();
            Anim = Runtime.UnityFrameworkEntry.GetComponent<AnimComponent>();

            if (IsDontDestroyOnLoad)
            {
                DontDestroyOnLoad(this);
            }

            Data.InitEvent += Audio.Init;

            Data.InitData();
        }

    }
}
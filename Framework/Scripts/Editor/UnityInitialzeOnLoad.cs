/*
* FileName:          UnityInitialzeOnLoad
* CompanyName:       
* Author:            ShangChao
* CreateTime:        2020-05-28-14:54:05
* UnityVersion:      2018.4.2f1
* Version：          1.0
* Description:       Unity在启动时执行操作
* 
*/

using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
namespace UnityFramework.Editor
{
	[InitializeOnLoad]
	public class UnityInitialzeOnLoad
	{
		static UnityInitialzeOnLoad()
		{
			ScriptingDefineSymbolsSettingWindows.UnityInitialzeOnLoadAddMacros();
			AutoSave();
		}
		static void AutoSave()
		{
			EditorApplication.playModeStateChanged += (PlayModeStateChange state) =>
			{
				if (EditorApplication.isPlayingOrWillChangePlaymode && !EditorApplication.isPlaying)
				{
					Debug.Log("正在保存所有已打开的场景  " + state);
					EditorSceneManager.SaveOpenScenes();
					AssetDatabase.SaveAssets();
				}
			};
		}

	}
}


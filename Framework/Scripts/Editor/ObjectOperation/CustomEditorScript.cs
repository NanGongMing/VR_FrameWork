/*
* FileName:          CustomEditorScript
* CompanyName:  
* Author:            ShangChao
* CreateTime:        2020-06-17-17:44:35
* UnityVersion:      2018.4.2f1
* Version:           1.0
* Description:       自定义脚本Inspector面板
* 
*/

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace UnityFramework.Editor
{

	public class CustomEditorScript : UnityEditor.Editor
	{

		public override void OnInspectorGUI()
		{
			SerializedProperty property = serializedObject.GetIterator();
			while (property.NextVisible(true))
			{
				using (new EditorGUI.DisabledScope("m_Script" == property.propertyPath))
				{
					EditorGUILayout.PropertyField(property, true);
					break;
				}
			}
		}
	}


}
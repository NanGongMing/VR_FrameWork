/*
* FileName:          CustomEditorScript_CollideObject
* CompanyName:  
* Author:            ShangChao
* CreateTime:        2020-06-19-15:11:43
* UnityVersion:      2018.4.2f1
* Version:           1.0
* Description:       自定义CollideObject脚本的Inspector面板
* 
*/

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace UnityFramework.Editor
{
	[CustomEditor(typeof(CollideObject), true)]
	public class CustomEditorScript_CollideObject : CustomEditorScript
	{
		SerializedObject obj;
		SerializedProperty functionSwitch;
		SerializedProperty collideTarget;
		SerializedProperty isSetPos;
		SerializedProperty collideEndParent;
		SerializedProperty usePosV3;
		SerializedProperty collideEndPos;
		SerializedProperty collideEndLocalPosV3;
		SerializedProperty collideEndLocalRoationV3;
		SerializedProperty collideEndEvent;
		private void OnEnable()
		{
			obj = new SerializedObject(target);
			functionSwitch = obj.FindProperty("FunctionSwitch");
			collideTarget = obj.FindProperty("CollideTarget");
			isSetPos = obj.FindProperty("IsSetPos");
			collideEndParent = obj.FindProperty("CollideEndParent");
			usePosV3 = obj.FindProperty("UsePosV3");
			collideEndPos = obj.FindProperty("CollideEndPos");
			collideEndLocalPosV3 = obj.FindProperty("CollideEndLocalPosV3");
			collideEndLocalRoationV3 = obj.FindProperty("CollideEndLocalRoationV3");
			collideEndEvent = obj.FindProperty("CollideEndEvent");
		}
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			obj.Update();
			EditorGUILayout.PropertyField(functionSwitch);
			if (functionSwitch.boolValue)
			{
				EditorGUILayout.PropertyField(collideTarget);
				EditorGUILayout.PropertyField(isSetPos);
				if (isSetPos.boolValue)
				{
					EditorGUILayout.PropertyField(collideEndParent);
					EditorGUILayout.PropertyField(usePosV3);
					if (!usePosV3.boolValue)
					{
						EditorGUILayout.PropertyField(collideEndPos);
					}
					else
					{
						EditorGUILayout.PropertyField(collideEndLocalPosV3);
						EditorGUILayout.PropertyField(collideEndLocalRoationV3);
					}
				}
				EditorGUILayout.PropertyField(collideEndEvent);
			}
			obj.ApplyModifiedProperties();
		}
	}


}


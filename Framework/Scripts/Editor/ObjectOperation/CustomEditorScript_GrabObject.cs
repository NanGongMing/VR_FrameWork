/*
* FileName:          CustomEditorScript_GrabObject
* CompanyName:  
* Author:            ShangChao
* CreateTime:        2020-06-19-15:24:36
* UnityVersion:      2018.4.2f1
* Version:           1.0
* Description:       自定义GrabObject脚本的Inspector面板
* 
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityFramework.Editor
{
	using UnityEditor;
	[CustomEditor(typeof(GrabObject))]
	public class CustomEditorScript_GrabObject : CustomEditorScript
	{
		SerializedObject obj;
		SerializedProperty TouchSwitch;
		SerializedProperty TouchEnterUnityEvent;
		SerializedProperty TouchExitUnityEvent;
		SerializedProperty GrabSwitch;
		SerializedProperty WhichHand;
		SerializedProperty GrabButton;
		SerializedProperty GrabType;
		SerializedProperty isGrabState;
		SerializedProperty UseGravity;
		SerializedProperty IsKinematic;
		SerializedProperty GrabPoint;
		SerializedProperty UsePosV3;
		SerializedProperty GrabUpLocalPos;
		SerializedProperty GrabUpLocalPosV3;
		SerializedProperty GrabUpLocalRoationV3;
		SerializedProperty GrabDownParent;
		SerializedProperty PickUpUnityEvent;
		SerializedProperty PickDownUnityEvent;
		private void OnEnable()
		{
			obj = new SerializedObject(target);
			TouchSwitch = obj.FindProperty("TouchSwitch");
			TouchEnterUnityEvent = obj.FindProperty("TouchEnterUnityEvent");
			TouchExitUnityEvent = obj.FindProperty("TouchExitUnityEvent");
			GrabSwitch = obj.FindProperty("GrabSwitch");
			WhichHand = obj.FindProperty("WhichHand");
			GrabButton = obj.FindProperty("GrabButton");
			GrabType = obj.FindProperty("GrabType");
			isGrabState = obj.FindProperty("isGrabState");
			UseGravity = obj.FindProperty("UseGravity");
			IsKinematic = obj.FindProperty("IsKinematic");
			GrabPoint = obj.FindProperty("GrabPoint");
			UsePosV3 = obj.FindProperty("UsePosV3");
			GrabUpLocalPos = obj.FindProperty("GrabUpLocalPos");
			GrabUpLocalPosV3 = obj.FindProperty("GrabUpLocalPosV3");
			GrabUpLocalRoationV3 = obj.FindProperty("GrabUpLocalRoationV3");
			GrabDownParent = obj.FindProperty("GrabDownParent");
			PickUpUnityEvent = obj.FindProperty("PickUpUnityEvent");
			PickDownUnityEvent = obj.FindProperty("PickDownUnityEvent");
		}
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			obj.Update();
			EditorGUILayout.PropertyField(TouchSwitch);
			if (TouchSwitch.boolValue)
			{
				EditorGUILayout.PropertyField(TouchEnterUnityEvent);
				EditorGUILayout.PropertyField(TouchExitUnityEvent);
			}
			EditorGUILayout.PropertyField(GrabSwitch);
			if (GrabSwitch.boolValue)
			{
				EditorGUILayout.PropertyField(WhichHand);
				EditorGUILayout.PropertyField(GrabButton);
				EditorGUILayout.PropertyField(GrabType);
				EditorGUILayout.PropertyField(isGrabState);
				EditorGUILayout.PropertyField(UseGravity);
				EditorGUILayout.PropertyField(IsKinematic);
				EditorGUILayout.PropertyField(GrabPoint);
				if (GrabPoint.enumValueIndex == 2)
				{
					EditorGUILayout.PropertyField(UsePosV3);
					if (!UsePosV3.boolValue)
					{
						EditorGUILayout.PropertyField(GrabUpLocalPos);
					}
					else
					{
						EditorGUILayout.PropertyField(GrabUpLocalPosV3);
						EditorGUILayout.PropertyField(GrabUpLocalRoationV3);
					}
				}
				EditorGUILayout.PropertyField(GrabDownParent);
				EditorGUILayout.PropertyField(PickUpUnityEvent);
				EditorGUILayout.PropertyField(PickDownUnityEvent);
			}
			obj.ApplyModifiedProperties();
		}
	}
}


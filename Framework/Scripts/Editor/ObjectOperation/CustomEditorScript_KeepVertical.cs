/*
* FileName:          CustomEditorScript_KeepVertical
* CompanyName:  
* Author:            ShangChao
* CreateTime:        2020-06-19-15:31:27
* UnityVersion:      2018.4.2f1
* Version:           1.0
* Description:       自定义KeepVertical的Inspector面板
* 
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityFramework.Editor
{
	using UnityEditor;
	[CustomEditor(typeof(KeepVertical))]
	public class CustomEditorScript_KeepVertical : CustomEditorScript
	{
		SerializedObject obj;
		SerializedProperty functionSwitch;
		SerializedProperty ResetCollideTarget;
		SerializedProperty ResetEndEvent;

		private void OnEnable()
		{
			obj = new SerializedObject(target);
			functionSwitch = obj.FindProperty("FunctionSwitch");
			ResetCollideTarget = obj.FindProperty("ResetCollideTarget");
			ResetEndEvent = obj.FindProperty("ResetEndEvent");
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			obj.Update();
			EditorGUILayout.PropertyField(functionSwitch);
			if (functionSwitch.boolValue)
			{
				EditorGUILayout.PropertyField(ResetCollideTarget, true);

				EditorGUILayout.PropertyField(ResetEndEvent);
			}
			obj.ApplyModifiedProperties();
		}
	}
}


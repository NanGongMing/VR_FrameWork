/*
* FileName:          CustomDictionaryDrawer
* CompanyName:       
* Author:            Relly
* CreateTime:        2022-07-08-10:43:19
* UnityVersion:      2019.4.8f1
* Version:           1.0
* Description:       
* 
*/

using UnityEditor;
using static UnityFramework.CustomDictionary;

namespace UnityFramework.Editor
{
	[CustomPropertyDrawer(typeof(StringStringDictionary))]
	[CustomPropertyDrawer(typeof(IntStringDictionary))]
	[CustomPropertyDrawer(typeof(StringIntDictionary))]
	[CustomPropertyDrawer(typeof(IntIntDictionary))]
	[CustomPropertyDrawer(typeof(StringStringListDictionary))]
	public class CustomDictionaryDrawer : SerializableDictionaryPropertyDrawer { }


	[CustomPropertyDrawer(typeof(StringListStorage))]
	public class CustomDictionaryStorageDrawer : SerializableDictionaryStoragePropertyDrawer { }


}
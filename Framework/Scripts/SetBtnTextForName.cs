/*
* FileName:          SetTextForParentName
* CompanyName:       杭州中锐
* Author:            Relly
* CreateTime:        2021-11-23-16:08:49
* UnityVersion:      2019.4.8f1
* Version:           1.0
* Description:       
* 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class SetBtnTextForName : MonoBehaviour
{

}
#if UNITY_EDITOR
[CustomEditor(typeof(SetBtnTextForName))]
public class SetBtnTextForNameEditorInspector : Editor
{

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();
		//获取脚本对象
		SetBtnTextForName script = target as SetBtnTextForName;


		if (GUILayout.Button("设置Text内容"))
		{
			foreach (Transform item in script.transform)
			{
				Text text = item.GetComponentInChildren<Text>();
				if (text)
				{
					string content = item.gameObject.name;
					Char[] temp = content.ToCharArray();
					List<Char> list = temp.ToList();
					int num = temp.Length / 2;
					if (list.Count > 9)
					{
						list.Insert(num, '\n');
					}
					text.text = new string(list.ToArray());
					text.fontSize = 110;
				}
			}
		}

	}
}

#endif



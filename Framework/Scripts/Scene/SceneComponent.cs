/*
* FileName:          SceneComponent
* CompanyName:       
* Author:            Relly
* CreateTime:        2022-08-11-16:35:22
* UnityVersion:      2020.3.26f1c1
* Description:       
*/

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityFramework.Runtime;

namespace UnityFramework.Scene
{
    /// <summary>
    /// 场景组件
    /// </summary>
    [DisallowMultipleComponent]
    public class SceneComponent : UnityFrameworkComponent
    {
        /// <summary>
        /// 加载场景
        /// </summary>
        /// <param name="sceneName">场景名称</param>
        public void LoadScene(string sceneName) { SceneManager.LoadScene(sceneName); }

        /// <summary>
        /// 加载场景
        /// </summary>
        /// <param name="sceneId">场景id</param>
        public void LoadScene(int sceneId) { SceneManager.LoadScene(sceneId); }

        /// <summary>
        /// 添加场景
        /// </summary>
        /// <param name="sceneName">场景名称</param>
        public void AddtiveScene(string sceneName) { SceneManager.LoadScene(sceneName, LoadSceneMode.Additive); }

        /// <summary>
        /// 异步加载场景
        /// </summary>
        /// <param name="sceneName">场景名称</param>
        public void SyncLoadScene(string sceneName)
        {
            PlayerPrefs.SetString("NextSceneName", sceneName);
            PlayerPrefs.SetString("SceneSyncLoadType", "Single");
            SceneManager.LoadScene("AsyncLoadScene");
        }

        /// <summary>
        /// 异步添加场景
        /// </summary>
        /// <param name="sceneName">场景名称</param>
        public void SyncAddtiveScene(string sceneName)
        {
            PlayerPrefs.SetString("NextSceneName", sceneName);
            PlayerPrefs.SetString("SceneSyncLoadType", "Additive");
            SceneManager.LoadScene("AsyncLoadScene", LoadSceneMode.Additive);
        }

        /// <summary>
        /// 卸载场景
        /// </summary>
        /// <param name="sceneName">场景名称</param>
        public void UnloadSceneAsync(string sceneName) { SceneManager.UnloadSceneAsync(sceneName); }
    }
}

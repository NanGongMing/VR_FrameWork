using UnityEngine;

namespace UnityFramework
{
    public class AnimLogic : MonoBehaviour
    {
        private Animator animator;
        protected virtual void Awake()
        {
            animator = GetComponent<Animator>();
            Framework.Anim.AddAnimationEndEvent(animator, "AnimEnd");
            Framework.Anim.AddAnimationStartEvent(animator, "AnimStart");

        }
        void AnimEnd()
        {
            Framework.Anim.AnimPlayEndEvent?.Invoke();
        }
        void AnimStart()
        {
            Framework.Anim.AnimPlayStartEvent?.Invoke();
        }
    }
}
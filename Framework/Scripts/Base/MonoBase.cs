/*
* FileName:          MonoBase
* CompanyName:  
* Author:            
* CreateTime:        2021-08-19-17:53:51
* UnityVersion:      2019.4.8f1
* Version:           1.0
* Description:
* 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityFramework
{
	public abstract class MonoBase : MonoBehaviour
	{
		private void Awake()
		{
			OnInit();
		}
		private void Start()
		{
			OnStart();

		}
		private void OnEnable()
		{
			OnShow();

		}
		private void Update()
		{
			OnUpdate();

		}
		private void OnDisable()
		{
			OnHide();
		}
		protected virtual void OnInit()
		{

		}
		protected virtual void OnStart()
		{

		}
		protected virtual void OnShow()
		{

		}
		protected virtual void OnUpdate()
		{

		}
		protected virtual void OnHide()
		{

		}

		public virtual void Show()
		{
			gameObject.SetActive(true);
		}
		public virtual void Hide()
		{
			gameObject.SetActive(false);
		}
		public virtual void SetVisible(bool visible)
		{
			gameObject.SetActive(visible);
		}

	}
}
/*
* FileName:          PageTurn
* CompanyName:       杭州中锐
* Author:            Relly
* CreateTime:        2021-11-24-10:39:06
* UnityVersion:      2019.4.8f1
* Version:           1.0
* Description:       
* 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using UnityFramework;
public class PageTurn : MonoBehaviour
{
	public GameObject[] Pages;
	private int index = 0;
	public Button LeftBtn, RightBtn;
	private void Start()
	{
		index = 0;
		LeftBtn.onClick.AddListener(Left);
		RightBtn.onClick.AddListener(Right);
	}
	public void Left()
	{
		index--;
		UtilityTools.HideObj(Pages);
		UtilityTools.ShowObj(Pages[index = index >= 0 ? index : index = Pages.Length - 1]);
	}
	public void Right()
	{
		index++;
		UtilityTools.HideObj(Pages);
		UtilityTools.ShowObj(Pages[index = index >= Pages.Length ? index = 0 : index]);
	}
}




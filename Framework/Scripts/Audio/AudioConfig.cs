﻿/*
* FileName:          AudioInfo
* CompanyName:       
* Author:            Relly
* CreateTime:        2021-11-16-09:38:03
* UnityVersion:      2019.4.8f1
* Version:           1.0
* Description:       
* 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityFramework.Audio
{

	/// <summary>
	/// 音频配置
	/// </summary>
	public class AudioConfig
	{
		/// <summary>
		/// 编号
		/// </summary>
		public int Id;
		/// <summary>
		/// 名称
		/// </summary>
		public string AudioName;
		/// <summary>
		/// 路径
		/// </summary>
		public string Path;
		/// <summary>
		/// 文本内容
		/// </summary>
		public string TextContent;

	}

}
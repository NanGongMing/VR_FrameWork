/*
* FileName:          AudioSourceComponent
* CompanyName:  
* Author:            ShangChao
* CreateTime:        2020-10-12-16:06:29
* UnityVersion:      2018.4.2f1
* Version:           1.0
* Description:       AudioComponent:注册AudioSource
*                    
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityFramework.Audio
{
	[DisallowMultipleComponent]
	public class AudioSourceAgent : MonoBehaviour
	{
		[Header("播放器标识,为空不会加入缓存")]
		public string mName;
		AudioSource mAudioSource;
		protected virtual void Awake()
		{
			mAudioSource = GetComponent<AudioSource>();
			if (mAudioSource == null)
			{
				mAudioSource = gameObject.AddComponent<AudioSource>();
			}
			Framework.Audio.AddAudioSourse(mName, mAudioSource);
		}
	}
}

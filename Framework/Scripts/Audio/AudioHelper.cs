/*
* FileName:          AudioHelper
* CompanyName:       
* Author:            Relly
* CreateTime:        2021-11-15-16:06:55
* UnityVersion:      2019.4.8f1
* Version:           1.0
* Description:       加载AudioConfig,AudioData等辅助脚本
* 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityFramework.Runtime;


namespace UnityFramework.Runtime
{
    public class AudioHelper : MonoBehaviour
    {
        [Header("配置加载模式")]
        public ConfigLoadMode ConfigLoadMode;
        [Header("资源加载模式")]
        public ResourceLoadMode ResourceLoadMode;
        [Header("配置表名称")]
        public List<string> TableName = new List<string>();
        AudioComponent audioManager;
        public AudioClip[] ClipsAry = new AudioClip[] { };

        public void Init()
        {
            audioManager = Framework.Audio;
            TableName.TryAdd("AudioInfo");
            LoadAudio();

            Invoke("Test", 3);
        }

        public void Test()
        {
            Dictionary<float, Action> dic = new Dictionary<float, Action>()
            {
                { 5,()=>Log.Error(5) },
                { 10,delegate(){audioManager.StopAllAudio();Log.Error("停止播放");}}
            };
            //   audioManager.PlayAudio(10179, 0, dic);
            audioManager.PlayAudio(new List<string> { "Hello World", "TestAudio" }, 3, () => Log.Error(5));
        }
        void LoadAudio()
        {
            if (ConfigLoadMode == ConfigLoadMode.Path)
            {
                StartCoroutine(InitOperation_LoadAudioForPath());
            }
            else
            {
                InitOperation_LoadAudioForDataCache();
            }
        }
        void InitOperation_LoadAudioForDataCache()
        {
            for (int i = 0; i < ClipsAry.Length; i++)
            {
                AudioInfo audioInfo = new AudioInfo
                {
                    Id = i,
                    AudioName = ClipsAry[i].name,
                    Path = null,
                    TextContent = ClipsAry[i].name,
                    Clip = ClipsAry[i],
                    AudioActionDic = new Dictionary<float, Action>(),
                    TimerList = new List<Timer.Timer>()
                };
                audioManager.AddAudioInfo(audioInfo);
            }

        }
        //从路径加载
        IEnumerator InitOperation_LoadAudioForPath()
        {
            foreach (var tableName in TableName)
            {
                List<AudioInfo> tableDataList = global::UnityFramework.Framework.Data.GetData<List<AudioInfo>>(tableName);

                foreach (var item in tableDataList)
                {

                    int id = item.Id;
                    string name = item.AudioName;
                    string path = item.Path + name;
                    string textContent = item.TextContent;
                    AudioClip clip = null;
                    switch (ResourceLoadMode)
                    {
                        case ResourceLoadMode.Resource:
                            clip = Resources.Load<AudioClip>(path);
                            break;
                        case ResourceLoadMode.StreamingAssets:
                            path = Application.streamingAssetsPath + "/" + path + ".wav";
                            UnityWebRequest request = UnityWebRequestMultimedia.GetAudioClip(path, AudioType.WAV);
                            yield return request.SendWebRequest();
#if UNITY_2020_1_OR_NEWER
                            if (request.result == UnityWebRequest.Result.ProtocolError || request.result == UnityWebRequest.Result.ConnectionError)
#else
			                if (request.isHttpError || request.isNetworkError)
#endif
                            {
                                Log.Error($"Load {path} fail");
                            }
                            else
                            {
                                clip = DownloadHandlerAudioClip.GetContent(request);

                            }
                            request.Dispose();

                            break;
                    }
                    AudioInfo audioInfo = new AudioInfo
                    {
                        Id = id,
                        AudioName = name,
                        Path = path,
                        TextContent = textContent,
                        Clip = clip,
                        AudioActionDic = new Dictionary<float, Action>(),
                        TimerList = new List<Timer.Timer>()
                    };
                    audioManager.AddAudioInfo(audioInfo);
                }
            }


        }


    }
}
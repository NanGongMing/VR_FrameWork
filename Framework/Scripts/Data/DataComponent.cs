/*
* FileName:          DataManager
* CompanyName:       
* Author:            Relly
* CreateTime:        2021-11-24-17:36:08
* UnityVersion:      2019.4.8f1
* Version:           1.0
* Description:       
* 
*/

using LitJson;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityFramework.Runtime;
namespace UnityFramework.Data
{
    [DisallowMultipleComponent]
    public class DataComponent : UnityFrameworkComponent
    {
        string dataContent;
        Dictionary<string, JsonData> dataDic = new Dictionary<string, JsonData>();
        [Header("文件路径,带后缀")]
        public List<string> FilePaths = new List<string> { };

        public event Action InitEvent;


        public void InitData()
        {
            StartCoroutine(InitOperation());
        }
        IEnumerator InitOperation()
        {
            foreach (var item in FilePaths)
            {
                UnityWebRequest request = UnityWebRequest.Get(Application.streamingAssetsPath + "/" + item);
                yield return request.SendWebRequest();
#if UNITY_2020_1_OR_NEWER
                if (request.result == UnityWebRequest.Result.ProtocolError || request.result == UnityWebRequest.Result.ConnectionError)
#else
			if (request.isHttpError || request.isNetworkError)
#endif
                {
                    Log.Error(request.error);
                }
                else
                {
                    dataContent = request.downloadHandler.text;
                    if (!dataContent.IsNullOrEmpty())
                    {


                        Dictionary<string, JsonData> temp = JsonMapper.ToObject<Dictionary<string, JsonData>>(dataContent);

                        foreach (var item1 in temp)
                        {
                            dataDic.Add(item1.Key, item1.Value);
                        }
                    }
                }
            }
            InitEvent?.Invoke();

        }

        public T GetData<T>(string key)
        {
            if (!dataDic.ContainsKey(key))
            {
                Log.Error($"dataDic[{key}] Data does not exist");
                return default(T);
            }
            T tableDataList = default;
            try
            {
                //tableDataList = JsonConvert.DeserializeObject<Dictionary<string, T>>(content)[key];
                //  tableDataList = JsonMapper.ToObject<Dictionary<string, T>>(dataContent)[key];
                tableDataList = JsonMapper.ToObject<T>(dataDic[key].ToJson());
            }
            catch (Exception e)
            {
                Log.Error($"dataDic[{key}] Data does not exist");
                throw e;
            }
            if (tableDataList != null)
            {

                return tableDataList;
            }


            return default(T);
        }

        public void Clear()
        {
            dataDic.Clear();
        }
    }
}
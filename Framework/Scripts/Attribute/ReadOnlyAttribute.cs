/*
* FileName:          Attribute
* CompanyName:  
* Author:            
* CreateTime:        2021-08-03-13:20:03
* UnityVersion:      2019.4.8f1
* Version:           1.0
* Description:
* 
*/

using UnityEngine;

namespace UnityFramework
{
	public sealed class ReadOnlyAttribute : PropertyAttribute
	{

	}
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using System.IO;
using System.Text.RegularExpressions;
using System;

namespace UnityFramework
{
	public partial class UtilityTools : MonoSingleton<UtilityTools>
	{
		/// <summary>
		/// 改变btns按钮图片为normial,btn改为press
		/// </summary>
		/// <param name="btns">要变成正常状态UI的button数组</param>
		/// <param name="btn">变成按下状态UI的按钮</param>
		/// <param name="normial">正常状态UI</param>
		/// <param name="press">按下状态UI</param>
		public static void ChangeImg(Button[] btns, Button btn, Sprite normial, Sprite press)
		{
			foreach (var item in btns)
			{
				item.image.overrideSprite = normial;
			}
			btn.image.overrideSprite = press;

		}
		/// <summary>
		/// 改变btns按钮图片为img
		/// </summary>
		/// <param name="img">按钮状态UI</param>
		/// <param name="btns">要改变的按钮(可以是多个按钮对象以","分开)</param>
		public static void ChangeImg(Sprite img, params Button[] btns)
		{
			foreach (var item in btns)
			{
				item.image.overrideSprite = img;
			}

		}

		/// <summary>
		/// 给按钮添加事件
		/// </summary>
		/// <param name="btn"></param>
		/// <param name="call"></param>
		public static void BtnAddEvent(Button btn, UnityAction call)
		{
			btn.onClick.AddListener(call);
		}
		/// <summary>
		/// 给有button组件的GameObject添加事件
		/// </summary>
		/// <param name="btn"></param>
		/// <param name="call"></param>
		public static void BtnAddEvent(GameObject btn, UnityAction call)
		{
			(btn as GameObject).GetComponent<Button>().onClick.AddListener(call);
		}

		//-----------------------------------------------------

		/// <summary>
		/// 显示物体
		/// </summary>
		/// <param name="objs"></param>
		public static void ShowObj(GameObject obj)
		{
			if (!obj)
			{
				return;
			}
			obj.SetActive(true);
		}
		/// <summary>
		/// 显示物体
		/// </summary>
		/// <param name="objs"></param>
		public static void ShowObj(Transform obj)
		{
			if (!obj)
			{
				return;
			}
			obj.gameObject.SetActive(true);
		}
		/// <summary>
		/// 显示多个物体
		/// </summary>
		/// <param name="objs"></param>
		public static void ShowObj(params GameObject[] objs)
		{
			foreach (var item in objs)
			{
				ShowObj(item);
			}
		}
		/// <summary>
		/// 显示多个物体
		/// </summary>
		/// <param name="objs"></param>
		public static void ShowObj(params Transform[] objs)
		{
			foreach (var item in objs)
			{
				ShowObj(item);
			}
		}

		/// <summary>
		/// 显示多个物体
		/// </summary>
		/// <param name="objs"></param>
		public static void ShowObj(List<GameObject> objs)
		{
			foreach (var item in objs)
			{
				ShowObj(item);
			}
		}
		/// <summary>
		/// 显示多个物体
		/// </summary>
		/// <param name="objs"></param>
		public static void ShowObj(List<Transform> objs)
		{
			foreach (var item in objs)
			{
				ShowObj(item);
			}
		}
		/// <summary>
		/// 显示单个物体,隐藏其他物体
		/// </summary>
		/// <param name="showobj">要显示的物体</param>
		/// <param name="hideobjs">要隐藏的物体</param>
		public static void ShowObj(GameObject showobj, GameObject[] hideobjs)
		{
			foreach (var item in hideobjs)
			{
				HideObj(item);
			}
			ShowObj(showobj);
		}
		/// <summary>
		/// 显示单个物体,隐藏其他物体
		/// </summary>
		/// <param name="showobj">要显示的物体</param>
		/// <param name="hideobjs">要隐藏的物体</param>
		public static void ShowObj(GameObject showobj, Transform[] hideobjs)
		{
			foreach (var item in hideobjs)
			{
				HideObj(item);
			}
			ShowObj(showobj);
		}
		/// <summary>
		/// 显示单个物体,隐藏其他物体
		/// </summary>
		/// <param name="showobj">要显示的物体</param>
		/// <param name="hideobjs">要隐藏的物体</param>
		public static void ShowObj(Transform showobj, Transform[] hideobjs)
		{
			foreach (var item in hideobjs)
			{
				HideObj(item);
			}
			ShowObj(showobj);
		}
		/// <summary>
		/// 显示单个物体,隐藏其他物体
		/// </summary>
		/// <param name="showobj">要显示的物体</param>
		/// <param name="hideobjs">要隐藏的物体</param>
		public static void ShowObj(Transform showobj, GameObject[] hideobjs)
		{
			foreach (var item in hideobjs)
			{
				HideObj(item);
			}
			ShowObj(showobj);
		}

		/// <summary>
		/// 显示单个物体,隐藏其他物体
		/// </summary>
		/// <param name="showobj">要显示的物体</param>
		/// <param name="hideobjs">要隐藏的物体</param>
		public static void ShowObj(GameObject showobj, List<GameObject> hideobjs)
		{
			foreach (var item in hideobjs)
			{
				HideObj(item);
			}
			ShowObj(showobj);
		}
		/// <summary>
		/// 显示单个物体,隐藏其他物体
		/// </summary>
		/// <param name="showobj">要显示的物体</param>
		/// <param name="hideobjs">要隐藏的物体</param>
		public static void ShowObj(GameObject showobj, List<Transform> hideobjs)
		{
			foreach (var item in hideobjs)
			{
				HideObj(item);
			}
			ShowObj(showobj);
		}
		/// <summary>
		/// 显示单个物体,隐藏其他物体
		/// </summary>
		/// <param name="showobj">要显示的物体</param>
		/// <param name="hideobjs">要隐藏的物体</param>
		public static void ShowObj(Transform showobj, List<Transform> hideobjs)
		{
			foreach (var item in hideobjs)
			{
				HideObj(item);
			}
			ShowObj(showobj);
		}
		/// <summary>
		/// 显示单个物体,隐藏其他物体
		/// </summary>
		/// <param name="showobj">要显示的物体</param>
		/// <param name="hideobjs">要隐藏的物体</param>
		public static void ShowObj(Transform showobj, List<GameObject> hideobjs)
		{
			foreach (var item in hideobjs)
			{
				HideObj(item);
			}
			ShowObj(showobj);
		}


		/// <summary>
		/// 隐藏单个物体
		/// </summary>
		/// <param name="hideobj">要隐藏的物体</param>
		public static void HideObj(GameObject hideobj)
		{
			if (!hideobj)
			{
				return;
			}
			hideobj.SetActive(false);
		}
		/// <summary>
		/// 隐藏单个物体
		/// </summary>
		/// <param name="hideobj">要隐藏的物体</param>
		public static void HideObj(Transform hideobj)
		{
			if (!hideobj)
			{
				return;
			}
			hideobj.gameObject.SetActive(false);
		}
		/// <summary>
		/// 隐藏多个物体
		/// </summary>
		/// <param name="objs"></param>
		public static void HideObj(params GameObject[] objs)
		{
			foreach (var item in objs)
			{
				HideObj(item);
			}
		}
		/// <summary>
		/// 隐藏多个物体
		/// </summary>
		/// <param name="objs"></param>
		public static void HideObj(params Transform[] objs)
		{
			foreach (var item in objs)
			{
				HideObj(item);
			}
		}
		/// <summary>
		/// 隐藏物体
		/// </summary>
		/// <param name="objs"></param>
		public static void HideObj(List<GameObject> objs)
		{
			foreach (var item in objs)
			{
				HideObj(item);
			}
		}
		/// <summary>
		/// 隐藏物体
		/// </summary>
		/// <param name="objs"></param>
		public static void HideObj(List<Transform> objs)
		{
			foreach (var item in objs)
			{
				HideObj(item);
			}
		}
		/// <summary>
		/// 隐藏hideobj,显示showobjs
		/// </summary>
		/// <param name="hideobj">要隐藏的物体</param>
		/// <param name="showobjs">要显示的物体</param>
		public static void HideObj(GameObject hideobj, GameObject[] showobjs)
		{
			foreach (var item in showobjs)
			{
				ShowObj(item);
			}
			HideObj(hideobj);
		}
		/// <summary>
		/// 隐藏hideobj,显示showobjs
		/// </summary>
		/// <param name="hideobj">要隐藏的物体</param>
		/// <param name="showobjs">要显示的物体</param>
		public static void HideObj(GameObject hideobj, Transform[] showobjs)
		{
			foreach (var item in showobjs)
			{
				ShowObj(item);
			}
			HideObj(hideobj);
		}
		/// <summary>
		/// 隐藏hideobj,显示showobjs
		/// </summary>
		/// <param name="hideobj">要隐藏的物体</param>
		/// <param name="showobjs">要显示的物体</param>
		public static void HideObj(Transform hideobj, Transform[] showobjs)
		{
			foreach (var item in showobjs)
			{
				ShowObj(item);
			}
			HideObj(hideobj);
		}
		/// <summary>
		/// 隐藏hideobj,显示showobjs
		/// </summary>
		/// <param name="hideobj">要隐藏的物体</param>
		/// <param name="showobjs">要显示的物体</param>
		public static void HideObj(Transform hideobj, GameObject[] showobjs)
		{
			foreach (var item in showobjs)
			{
				ShowObj(item);
			}
			HideObj(hideobj);
		}

		/// <summary>
		/// 隐藏hideobj,显示showobjs
		/// </summary>
		/// <param name="hideobj">要隐藏的物体</param>
		/// <param name="showobjs">要显示的物体</param>
		public static void HideObj(GameObject hideobj, List<GameObject> showobjs)
		{
			foreach (var item in showobjs)
			{
				ShowObj(item);
			}
			HideObj(hideobj);
		}
		/// <summary>
		/// 隐藏hideobj,显示showobjs
		/// </summary>
		/// <param name="hideobj">要隐藏的物体</param>
		/// <param name="showobjs">要显示的物体</param>
		public static void HideObj(GameObject hideobj, List<Transform> showobjs)
		{
			foreach (var item in showobjs)
			{
				ShowObj(item);
			}
			HideObj(hideobj);
		}
		/// <summary>
		/// 隐藏hideobj,显示showobjs
		/// </summary>
		/// <param name="hideobj">要隐藏的物体</param>
		/// <param name="showobjs">要显示的物体</param>
		public static void HideObj(Transform hideobj, List<Transform> showobjs)
		{
			foreach (var item in showobjs)
			{
				ShowObj(item);
			}
			HideObj(hideobj);
		}
		/// <summary>
		/// 隐藏hideobj,显示showobjs
		/// </summary>
		/// <param name="hideobj">要隐藏的物体</param>
		/// <param name="showobjs">要显示的物体</param>
		public static void HideObj(Transform hideobj, List<GameObject> showobjs)
		{
			foreach (var item in showobjs)
			{
				ShowObj(item);
			}
			HideObj(hideobj);
		}
		/// <summary>
		/// 如果物体当前为显示就隐藏,如果隐藏就显示
		/// </summary>
		/// <param name="obj">物体</param>
		public static void ChangeActiveSelf(GameObject obj)
		{
			if (obj.activeSelf)
			{
				HideObj(obj);
			}
			else
			{
				ShowObj(obj);
			}
		}
		/// <summary>
		/// 如果物体当前为显示就隐藏,如果隐藏就显示
		/// </summary>
		/// <param name="obj">物体</param>
		public static void ChangeActiveSelf(Transform obj)
		{
			if (obj.gameObject.activeSelf)
			{
				HideObj(obj);
			}
			else
			{
				ShowObj(obj);
			}
		}

		//-----------------------------------------

		/// <summary>
		/// 继续播放当前动画
		/// </summary>
		/// <param name="anim"></param>
		public static void ContinuePlayAnim(Animation anim)
		{
			anim[anim.clip.name].speed = 1;
		}
		/// <summary>
		/// 暂停当前动画
		/// </summary>
		/// <param name="anim"></param>
		public static void PauseAnim(Animation anim)
		{
			anim[anim.clip.name].speed = 0;
		}
		/// <summary>
		/// 播放某动画
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="str"></param>
		public static void PlayAnim(AnimType animType, GameObject obj, string str)
		{
			switch (animType)
			{
				case AnimType.Animation:
					Animation animation = obj.GetComponent<Animation>();
					if (animation)
					{
						animation.Play(str);
					}
					break;
				case AnimType.Animator:
					Animator animator = obj.GetComponent<Animator>();
					if (animator)
					{
						animator.Play(str);
					}
					break;
				default:
					break;
			}
		}
		/// <summary>
		/// 播放动画
		/// </summary>
		/// <param name="obj"></param>
		public static void PlayAnim(GameObject obj)
		{
			obj.GetComponent<Animation>().Play();
		}
		/// <summary>
		/// 播放动画
		/// </summary>
		/// <param name="obj"></param>
		public static void PlayAnim(Animation obj)
		{
			obj.Play();
		}
		/// <summary>
		/// 播放某动画
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="str"></param>
		public static void PlayAnim(Animation obj, string str)
		{
			obj.Play(str);
		}
		/// <summary>
		/// 停止动画
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="str"></param>
		public static void StopAnim(GameObject obj, string str)
		{
			obj.GetComponent<Animation>().Stop(str);
		}
		/// <summary>
		/// 停止动画
		/// </summary>
		/// <param name="obj"></param>
		public static void StopAnim(GameObject obj)
		{
			obj.GetComponent<Animation>().Stop();
		}
		/// <summary>
		/// 停止动画
		/// </summary>
		/// <param name="obj"></param>
		public static void StopAnim(Animation obj)
		{
			obj.Stop();

		}
		/// <summary>
		/// 停止动画
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="str"></param>
		public static void StopAnim(Animation obj, string str)
		{
			obj.Stop(str);
		}
		/// <summary>
		/// 设置颜色
		/// </summary>
		/// <param name="_image">Image</param>
		/// <param name="_color">Color</param>
		public static void SetColor(Image _image, Color _color)
		{
			_image.color = _color;
		}
		/// <summary>
		/// 设置颜色
		/// </summary>
		/// <param name="_image">Image</param>
		/// <param name="_colorStr">Color 16进制</param>
		public static void SetColor(Image _image, string _colorStr)
		{
			if (ColorUtility.TryParseHtmlString(_colorStr, out Color _color))
			{
				_image.color = _color;
			}
		}
		static IEnumerator DelayCommand(float time, UnityAction unityAction)
		{
			yield return new WaitForSeconds(time);
			unityAction?.Invoke();
		}
		/// <summary>
		/// 设置Image射线检测状态
		/// </summary>
		/// <param name="_image">Image</param>
		/// <param name="_rayCast">是否具有射线交互属性</param>
		public static void SetRayCastTarget(Image _image, bool _rayCast)
		{
			_image.raycastTarget = _rayCast;
		}

		/// <summary>
		/// 设置内容
		/// </summary>
		/// <param name="_text">Text</param>
		/// <param name="_content">修改的内容</param>
		public static void SetContent(Text _text, string _content)
		{
			_text.text = _content;
		}

		/// <summary>
		/// 退出游戏
		/// </summary>
		public static void ExitGame()
		{
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
		}
		/// <summary>
		/// 延迟执行
		/// </summary>
		/// <param name="time"></param>
		/// <param name="unityAction"></param>
		public static void DelayCommandEvent(float time, UnityAction unityAction)
		{
			UtilityTools.Instance.StartCoroutine(DelayCommand(time, unityAction));
		}

		/// <summary>
		/// 加载场景
		/// </summary>
		/// <param name="name"></param>
		public static void LoadScene(string name)
		{
			SceneManager.LoadScene(name);
		}
		/// <summary>
		/// 加载场景
		/// </summary>
		/// <param name="num"></param>
		public static void LoadScene(int num)
		{
			SceneManager.LoadScene(num);
		}
		public static Transform FindChildByName(Transform _findParent, string _targetName)
		{
			Transform obj = _findParent.Find(_targetName);
			if (obj != null) return obj;
			for (int i = 0; i < _findParent.childCount; i++)
			{
				obj = FindChildByName(_findParent.GetChild(i), _targetName);
				if (obj != null)
				{
					return obj;
				}
			}
			return null;
		}

		/// <summary>
		/// 从本地读取文件 返回Sprite
		/// </summary>
		/// <param name="_filePath">图片路径</param>
		/// <param name="textureWidth">图片宽度</param>
		/// <param name="textureHeight">图片高度</param>
		/// <returns></returns>
		public static Sprite GetSpriteByPath(string _filePath, int textureWidth, int textureHeight)
		{
			Sprite sprite = null;
			//读取路径下文件
			StreamReader reader = new StreamReader(_filePath);
			//获取字节流
			MemoryStream ms = new MemoryStream();
			reader.BaseStream.CopyTo(ms);
			byte[] bytes = ms.ToArray();

			sprite = ConverSpriteByData(bytes, textureWidth, textureHeight);

			reader.Close();

			return sprite;
		}
		/// <summary>
		/// Json字符串转码
		/// </summary>
		/// <param name="_json"></param>
		/// <returns></returns>
		public static string EncodingJson(string _json)
		{
			string content = "";
			Regex reg = new Regex(@"(?i)\\[uU]([0-9a-f]{4})");
			content = reg.Replace(_json, delegate (Match m) { return ((char)Convert.ToInt32(m.Groups[1].Value, 16)).ToString(); });
			return content;
		}
		/// <summary>
		/// 64位字符串转化为精灵图片
		/// </summary>
		/// <param name="_spriteStr"></param>
		/// <returns></returns>
		public static Sprite GetSpriteBy64Str(string _spriteStr, int textureWidth, int textureHeight)
		{
			Sprite sprite = null;
			byte[] bytes = Convert.FromBase64String(_spriteStr);
			sprite = ConverSpriteByData(bytes, textureWidth, textureHeight);
			return sprite;
		}

		/// <summary>
		/// 字节转化为精灵图片
		/// </summary>
		/// <param name="datas"></param>
		/// <returns></returns>
		public static Sprite ConverSpriteByData(byte[] datas, int textureWidth, int textureHeight)
		{
			Sprite sprite = null;

			//定义Textrue2D并转化为精灵图片          
			Texture2D texture = new Texture2D(textureWidth, textureHeight);
			texture.LoadImage(datas);
			sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.one / 2);

			return sprite;
		}
		/// <summary>
		/// 从摄像机截图
		/// </summary>
		/// <param name="camera">摄像机</param>
		/// <param name="rect">大小</param>
		/// <returns></returns>
		public static Texture2D CaptureCamera(Camera camera, Rect rect)
		{
			// 建一个RenderTexture对象  
			RenderTexture rt = new RenderTexture((int)rect.width, (int)rect.height, 1);
			// 临时设置相关相机的targetTexture为rt, 并手动渲染相关相机  
			camera.targetTexture = rt;
			camera.Render();

			// 激活这个rt, 并从中中读取像素。  
			RenderTexture.active = rt;
			Texture2D screenShot = new Texture2D((int)rect.width, (int)rect.height, TextureFormat.RGB24, false);
			screenShot.ReadPixels(rect, 0, 0);// 注：这个时候，它是从RenderTexture.active中读取像素  
			screenShot.Apply();

			// 重置相关参数，以使用camera继续在屏幕上显示  
			camera.targetTexture = null;
			//ps: camera2.targetTexture = null;  
			RenderTexture.active = null; // JC: added to avoid errors  
			GameObject.Destroy(rt);
			// 最后将这些纹理数据，成一个png图片文件  
			byte[] bytes = screenShot.EncodeToPNG();

			// 获取系统时间
			System.DateTime now = new System.DateTime();
			now = System.DateTime.Now;
			string picName = string.Format("image{0}{1}{2}{3}{4}{5}.png", now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second);


			string path =

#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBGL
			Application.streamingAssetsPath + "/CaptureCamera";
#elif UNITY_ANDROID
		"/sdcard/DCIM/Camera";
#endif
			if (!Directory.Exists(path))
			{
				Directory.CreateDirectory(path);
			}
			string filename = path + "/" + picName;


			System.IO.File.WriteAllBytes(filename, bytes);
			Debug.Log(string.Format("截屏了一张照片: {0}", filename));

#if UNITY_EDITOR
			UnityEditor.AssetDatabase.Refresh();
#endif

			return screenShot;
		}
	}
}




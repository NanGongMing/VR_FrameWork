﻿/*
* FileName:          StringExtension
* CompanyName:       
* Author:            Relly
* CreateTime:        2022-02-07-14:30:35
* UnityVersion:      2019.4.8f1
* Version:           1.0
* Description:       
* 
*/

using System.Text.RegularExpressions;
using System.Diagnostics;
using System.IO;
using System.Globalization;

namespace UnityFramework
{
	public static class StringExtension
	{
		/// <summary>
		/// 字符串首字母大写,只转换第一个位置 例:my lsit =>My list
		/// </summary>
		/// <param name="content"></param>
		/// <returns></returns>
		public static string ToUpperFirst(this string content)
		{
			//Regex.Replace(content, "^[a-z]", m => m.Value.ToUpper())
			return Regex.Replace(content, @"^\w", t => t.Value.ToUpper());
		}

		/// <summary>
		/// 字符串首字母大写,包含空格后的 例:my lsit =>My List
		/// </summary>
		/// <param name="content"></param>
		/// <returns></returns>
		public static string ToUpperALLFirst(this string content)
		{
			return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(content.ToLower());
		}

		/// <summary>
		/// 执行dos命令   "del c:\\t1.txt".ExecuteDOS();
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="error">错误信息</param>
		/// <returns></returns>
		public static string ExecuteDOS(this string cmd, out string error)
		{
			Process process = new Process();
			process.StartInfo.FileName = "cmd.exe";
			process.StartInfo.UseShellExecute = false;
			process.StartInfo.RedirectStandardInput = true;
			process.StartInfo.RedirectStandardOutput = true;
			process.StartInfo.RedirectStandardError = true;
			process.StartInfo.CreateNoWindow = true;
			process.Start();
			process.StandardInput.WriteLine(cmd);
			process.StandardInput.WriteLine("exit");
			error = process.StandardError.ReadToEnd();
			return process.StandardOutput.ReadToEnd();
		}

		public static void DeleteFile(this string path)
		{
			if (File.Exists(path)) File.Delete(path);
		}

		public static void WriteAllText(this string path, string contents)
		{
			File.WriteAllText(path, contents);
		}

		public static void CreateDirectory(this string path)
		{
			Directory.CreateDirectory(path);
		}

		/// <summary> 
		/// 打开文件或网址 "c:\\t.txt".Open();  "http://www.baidu.com/".Open();
		/// </summary>
		/// <param name="s"></param>
		public static void Open(this string s)
		{
			Process.Start(s);
		}



		public static bool IsInt(this string s)
		{
			int i;
			return int.TryParse(s, out i);
		}

		public static int ToInt(this string s)
		{
			return int.Parse(s);
		}

		public static bool IsMatch(this string s, string pattern)
		{
			if (s == null) return false;
			else return Regex.IsMatch(s, pattern);
		}

		public static string Match(this string s, string pattern)
		{
			if (s == null) return "";
			return Regex.Match(s, pattern).Value;
		}

		public static string FormatWith(this string format, params object[] args)
		{
			return string.Format(format, args);
		}

		public static bool IsNullOrEmpty(this string s)
		{
			return string.IsNullOrEmpty(s);
		}

		/// <summary>
		/// 转全角(SBC case)
		/// </summary>
		/// <param name="content"></param>
		public static string ToSBC(this string content)
		{
			char[] c = content.ToCharArray();
			for (int i = 0; i < c.Length; i++)
			{
				if (c[i] == 32)
				{
					c[i] = (char)12288;
					continue;
				}
				if (c[i] < 127)
					c[i] = (char)(c[i] + 65248);
			}
			return new string(c);
		}

		/// <summary>
		/// 转半角(DBC case)
		/// </summary>
		/// <param name="content"></param>
		/// <returns></returns>
		public static string ToDBC(this string content)
		{
			char[] c = content.ToCharArray();
			for (int i = 0; i < c.Length; i++)
			{
				if (c[i] == 12288)
				{
					c[i] = (char)32;
					continue;
				}
				if (c[i] > 65280 && c[i] < 65375)
					c[i] = (char)(c[i] - 65248);
			}
			return new string(c);
		}
	}
}
/*
* FileName:          TransformExtension
* CompanyName:       
* Author:            Relly
* CreateTime:        2022-07-11-17:08:52
* UnityVersion:      2020.3.26f1c1
* Version:           1.0
* Description:       
* 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace UnityFramework
{
	public static class TransformExtension
	{
		/// <summary>
		/// 获取跟Inspector面板显示一致的旋转信息  Unity版本2018以上
		/// </summary>
		/// <param name="transform"></param>
		/// <returns></returns>
		public static Vector3 GetInspectorRotationValueMethod(this Transform transform)
		{
			System.Type transformType = transform.GetType();
			PropertyInfo m_propertyInfo_rotationOrder = transformType.GetProperty("rotationOrder", BindingFlags.Instance | BindingFlags.NonPublic);
			object m_OldRotationOrder = m_propertyInfo_rotationOrder.GetValue(transform, null);
			MethodInfo m_methodInfo_GetLocalEulerAngles = transformType.GetMethod("GetLocalEulerAngles", BindingFlags.Instance | BindingFlags.NonPublic);
			object value = m_methodInfo_GetLocalEulerAngles.Invoke(transform, new object[] { m_OldRotationOrder });


			string temp = value.ToString();
			//将字符串第一个和最后一个去掉
			temp = temp.Remove(0, 1);
			temp = temp.Remove(temp.Length - 1, 1);
			//用‘，’号分割
			string[] tempVector3;
			tempVector3 = temp.Split(',');
			//将分割好的数据传给Vector3
			Vector3 vector3 = new Vector3(float.Parse(tempVector3[0]), float.Parse(tempVector3[1]), float.Parse(tempVector3[2]));
			return vector3;
		}
		public static Vector3 GetInspectorRotationValueMethod(this Transform tan, Transform transform)
		{
			System.Type transformType = transform.GetType();
			PropertyInfo m_propertyInfo_rotationOrder = transformType.GetProperty("rotationOrder", BindingFlags.Instance | BindingFlags.NonPublic);
			object m_OldRotationOrder = m_propertyInfo_rotationOrder.GetValue(transform, null);
			MethodInfo m_methodInfo_GetLocalEulerAngles = transformType.GetMethod("GetLocalEulerAngles", BindingFlags.Instance | BindingFlags.NonPublic);
			object value = m_methodInfo_GetLocalEulerAngles.Invoke(transform, new object[] { m_OldRotationOrder });


			string temp = value.ToString();
			//将字符串第一个和最后一个去掉
			temp = temp.Remove(0, 1);
			temp = temp.Remove(temp.Length - 1, 1);
			//用‘，’号分割
			string[] tempVector3;
			tempVector3 = temp.Split(',');
			//将分割好的数据传给Vector3
			Vector3 vector3 = new Vector3(float.Parse(tempVector3[0]), float.Parse(tempVector3[1]), float.Parse(tempVector3[2]));
			return vector3;
		}

		/// <summary>
		/// 获取跟Inspector面板显示一致的旋转信息  Unity版本2018以下
		/// </summary>
		/// <param name="mTransform"></param>
		/// <returns></returns>
		public static Vector3 GetInpectorEulers(this Transform mTransform)
		{
			Vector3 angle = mTransform.eulerAngles;
			float x = angle.x;
			float y = angle.y;
			float z = angle.z;

			if (Vector3.Dot(mTransform.up, Vector3.up) >= 0f)
			{
				if (angle.x >= 0f && angle.x <= 90f)
				{
					x = angle.x;
				}
				if (angle.x >= 270f && angle.x <= 360f)
				{
					x = angle.x - 360f;
				}
			}
			if (Vector3.Dot(mTransform.up, Vector3.up) < 0f)
			{
				if (angle.x >= 0f && angle.x <= 90f)
				{
					x = 180 - angle.x;
				}
				if (angle.x >= 270f && angle.x <= 360f)
				{
					x = 180 - angle.x;
				}
			}

			if (angle.y > 180)
			{
				y = angle.y - 360f;
			}

			if (angle.z > 180)
			{
				z = angle.z - 360f;
			}
			Vector3 vector3 = new Vector3(Mathf.Round(x), Mathf.Round(y), Mathf.Round(z));
			//Debug.Log(" Inspector Euler:  " + Mathf.Round(x) + " , " + Mathf.Round(y) + " , " + Mathf.Round(z));
			return vector3;
		}

		public static Vector3 GetInpectorEulers(this Transform tan, Transform mTransform)
		{
			Vector3 angle = mTransform.eulerAngles;
			float x = angle.x;
			float y = angle.y;
			float z = angle.z;

			if (Vector3.Dot(mTransform.up, Vector3.up) >= 0f)
			{
				if (angle.x >= 0f && angle.x <= 90f)
				{
					x = angle.x;
				}
				if (angle.x >= 270f && angle.x <= 360f)
				{
					x = angle.x - 360f;
				}
			}
			if (Vector3.Dot(mTransform.up, Vector3.up) < 0f)
			{
				if (angle.x >= 0f && angle.x <= 90f)
				{
					x = 180 - angle.x;
				}
				if (angle.x >= 270f && angle.x <= 360f)
				{
					x = 180 - angle.x;
				}
			}

			if (angle.y > 180)
			{
				y = angle.y - 360f;
			}

			if (angle.z > 180)
			{
				z = angle.z - 360f;
			}
			Vector3 vector3 = new Vector3(Mathf.Round(x), Mathf.Round(y), Mathf.Round(z));
			//Debug.Log(" Inspector Euler:  " + Mathf.Round(x) + " , " + Mathf.Round(y) + " , " + Mathf.Round(z));
			return vector3;
		}
	}
}
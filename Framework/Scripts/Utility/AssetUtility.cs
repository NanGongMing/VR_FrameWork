/*
* FileName:          AssetUtility
* CompanyName:  
* Author:            ShangChao
* CreateTime:        2020-06-30-16:06:29
* UnityVersion:      2018.4.2f1
* Version:           1.0
* Description:       配置资源路径
* 
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityFramework
{
	public static class AssetUtility
	{
		public static string GetAudioAsset(string assetName)
		{
			return string.Format("Assets/Sounds/{0}.wav", assetName);
		}
		public static string GetVideoAsset(string assetName)
		{
			return string.Format("Assets/Videos/{0}.wav", assetName);
		}
	}
}




﻿/*
* FileName:          UIFollow
* CompanyName:       
* Author:            Relly
* CreateTime:        2022-04-02-15:05:50
* UnityVersion:      2020.3.26f1c1
* Version:           1.0
* Description:       
* 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if VIU_STEAMVR_2_0_0_OR_NEWER

using Valve.VR;
using Valve.VR.InteractionSystem;

namespace UnityFramework.UI
{
	public class UIFollow : MonoBehaviour
	{
		/// <summary>
		/// 跟随配置
		/// </summary>
		public UIFollowConf UIFollowConf;
		private VRHand target;
		private void Start()
		{
			target = null;
			if (VRHandHelper.Instance && UIFollowConf && UIFollowConf.followType == FollowType.FollowHand)
			{
				for (int i = 0; i < Player.instance.handCount; i++)
				{
					if (UIFollowConf.handType == SteamVR_Input_Sources.Any || Player.instance.hands[i].handType == UIFollowConf.handType)
					{
						target = Player.instance.hands[i].GetComponent<VRHand>();
						break;
					}
				}
			}
		}
		private void FixedUpdate()
		{
			if (VRHandHelper.Instance && UIFollowConf && UIFollowConf.followType == FollowType.FollowHead)
			{
				if (Vector3.Angle(VRHandHelper.Instance.Target.forward, transform.position - VRHandHelper.Instance.Target.position) > 38
					|| Vector3.Distance(VRHandHelper.Instance.Target.position, transform.position) > UIFollowConf.fllowDis * 1.2f ||
					Vector3.Distance(VRHandHelper.Instance.Target.position, transform.position) < UIFollowConf.fllowMinDis)
				{
					transform.position = Vector3.Lerp(transform.position,
						UnityUtil.GetFrontPos(VRHandHelper.Instance.Target, UIFollowConf.fllowDis, false),
						Time.deltaTime * UIFollowConf.flowSpeed);
				}
				UnityUtil.LookAtV(transform, VRHandHelper.Instance.Target, -1);
			}

			if (VRHandHelper.Instance && UIFollowConf && UIFollowConf.followType == FollowType.FollowHand)
			{
				if (target && gameObject.activeSelf)
				{
					transform.position = target.GetDlgLinePos();
					UnityUtil.LookAtV(transform, Player.instance.hmdTransform, UIFollowConf.forward);
				}
			}

		}
	}
}
#endif
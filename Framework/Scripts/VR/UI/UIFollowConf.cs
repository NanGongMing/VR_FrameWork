﻿/*
* FileName:          UIFollowConf
* CompanyName:       
* Author:            Relly
* CreateTime:        2022-04-02-14:40:54
* UnityVersion:      2020.3.26f1c1
* Version:           1.0
* Description:       
* 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if VIU_STEAMVR_2_0_0_OR_NEWER
using Valve.VR;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace UnityFramework.UI
{
	public enum FollowType
	{
		None,
		FollowHead,
		FollowHand
	}
	public class UIFollowConf : ScriptableObject
	{
		public FollowType followType;

		/// <summary>
		/// FollowHead 设置
		/// </summary>
		public float fllowDis = 0;
		public float fllowMinDis = 0;
		public float flowSpeed = 1;

		/// <summary>
		/// FollowHead 设置
		/// </summary>
		public SteamVR_Input_Sources handType;
		public int forward = 1;//方向

#if UNITY_EDITOR
		[MenuItem("Assets/Create/VR/UIFollowConf", false, 0)]
		static void CreateDynamicConf()
		{
			UnityEngine.Object obj = Selection.activeObject;
			if (obj)
			{
				string path = AssetDatabase.GetAssetPath(obj);
				ScriptableObject bullet = CreateInstance<UIFollowConf>();
				if (bullet)
				{
					string confName = UnityUtil.TryGetName<UIFollowConf>(path);
					AssetDatabase.CreateAsset(bullet, confName);
				}
				else
				{
					Debug.Log(typeof(UIFollowConf) + " is null");
				}
			}
		}

#endif
	}
}
#endif
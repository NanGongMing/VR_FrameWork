﻿/*
* FileName:          ToolConf
* CompanyName:       
* Author:            Relly
* CreateTime:        2022-03-31-10:06:01
* UnityVersion:      2020.3.26f1c1
* Version:           1.0
* Description:       
* 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if VIU_STEAMVR_2_0_0_OR_NEWER

using Valve.VR.InteractionSystem;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace UnityFramework
{
	public class ToolConf : ScriptableObject
	{
		public string ToolName;
		[HideInInspector]
		public ToolBasic ToolBasic;

		[EnumFlags]
		public Hand.AttachmentFlags AttachmentFlags = Hand.AttachmentFlags.SnapOnAttach | Hand.AttachmentFlags.DetachFromOtherHand | Hand.AttachmentFlags.VelocityMovement | Hand.AttachmentFlags.TurnOffGravity;
		//需勾选Interactable的useHandObjectAttachmentPoint,物体挂有skeletonPoser脚本时无效
		[Header("拿起时的位置旋转设置")]
		public bool IsSetCatchPose;
		public Vector3 ObjectAttachmentPointPosition;
		public Vector3 ObjectAttachmentPointAngle;

		[Header("碰到其他物体时的姿势设置")]
		public bool IsSetCollidePose;
		public ToolConf CollideObj;
		//碰撞后设置完位置信息是否可以再抓起来
		public bool IsCanCatchOnCollideEnd;
		public bool CollideEndIsKinematic;
		public Vector3 CollidePositon;
		public Vector3 CollideAngle;
		public ToolConf CollideParent;

#if UNITY_EDITOR
		[MenuItem("Assets/Create/VR/ToolConf", false, 0)]
		static void CreateDynamicConf()
		{
			UnityEngine.Object obj = Selection.activeObject;
			if (obj)
			{
				string path = AssetDatabase.GetAssetPath(obj);
				ScriptableObject bullet = CreateInstance<ToolConf>();
				if (bullet)
				{
					string confName = UnityUtil.TryGetName<ToolConf>(path);
					AssetDatabase.CreateAsset(bullet, confName);
				}
				else
				{
					Debug.Log(typeof(ToolConf) + " is null");
				}
			}
		}
#endif
	}
}
#endif
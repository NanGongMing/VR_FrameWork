﻿/*
* FileName:          ToolInteractable
* CompanyName:       
* Author:            Relly
* CreateTime:        2022-03-24-16:32:54
* UnityVersion:      2020.3.26f1c1
* Version:           1.0
* Description:       
* 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if VIU_STEAMVR_2_0_0_OR_NEWER
using Valve.VR;
using Valve.VR.InteractionSystem;

namespace UnityFramework
{

	public abstract class ToolInteractable : MonoBehaviour
	{
		protected bool isCanHover = false;
		protected bool isCanCatch = false;
		protected virtual void Awake()
		{
			isCanHover = true;
			isCanCatch = true;
		}


		#region  Hand.SendMessage Hand调用
		/// <summary>
		/// 当手柄触碰到可交互物体
		/// </summary>
		/// <param name="hand"></param>
		void OnHandHoverBegin(Hand hand)
		{
			if (!isCanHover)
				return;

			OnHoverBegin(hand);
		}

		/// <summary>
		/// 当手柄触碰到可交互物体时,并扣动了扳机，每帧UPdate调用
		/// </summary>
		/// <param name="hand"></param>
		void HandHoverUpdate(Hand hand)
		{
			if (!isCanHover)
				return;

			OnHoverUpdate(hand);
		}

		/// <summary>
		/// 手柄离开可交互物体
		/// </summary>
		/// <param name="hand"></param>
		void OnHandHoverEnd(Hand hand)
		{
			if (!isCanHover)
				return;

			OnHoverEnd(hand);
		}

		/// <summary>
		/// 持续抓取
		/// </summary>
		/// <param name="hand"></param>
		void HandAttachedUpdate(Hand hand)
		{
			if (!isCanCatch)
				return;

			OnCatching(hand);

			if (SteamVR_Actions._default.Use.GetStateDown(hand.handType))
			{
				OnUse(hand);
			}

			if (SteamVR_Actions._default.Use.GetState(hand.handType))
			{
				OnUse(hand);
			}

		}
		/// <summary>
		/// 抓取
		/// </summary>
		/// <param name="hand"></param>
		void OnAttachedToHand(Hand hand)
		{
			if (!isCanCatch)
				return;

			OnCatch(hand);
		}
		/// <summary>
		/// 放下
		/// </summary>
		/// <param name="hand"></param>
		void OnDetachedFromHand(Hand hand)
		{
			if (!isCanCatch)
				return;

			OnDetach(hand);
		}
		#endregion




		/// <summary>
		/// 手柄触碰物体每帧调用
		/// </summary>
		/// <param name="hand"></param>
		protected virtual void OnHoverUpdate(Hand hand)
		{
			//Debug.LogError($"OnHoverUpdate:{hand.handType}");

		}
		/// <summary>
		/// 手柄离开物体
		/// </summary>
		/// <param name="hand"></param>
		protected virtual void OnHoverEnd(Hand hand)
		{
			//	Debug.LogError($"OnHoverEnd:{hand.handType}");

		}
		/// <summary>
		/// 手柄触碰物体
		/// </summary>
		/// <param name="hand"></param>
		protected virtual void OnHoverBegin(Hand hand)
		{
			//	Debug.LogError($"OnHoverBegin:{hand.handType}");

		}
		/// <summary>
		/// 拿起工具
		/// </summary>
		/// <param name="hand"></param>
		protected virtual void OnCatch(Hand hand)
		{
			//	Debug.LogError($"OnCatch:{hand.handType}");

		}
		/// <summary>
		/// 握持工具
		/// </summary>
		/// <param name="hand"></param>
		protected virtual void OnCatching(Hand hand)
		{
			//	Debug.LogError($"OnCatching:{hand.handType}");

		}
		/// <summary>
		/// 放下工具
		/// </summary>
		/// <param name="hand"></param>
		protected virtual void OnDetach(Hand hand)
		{
			//	Debug.LogError($"OnDetach:{hand.handType}");

		}


		/// <summary>
		/// 使用功能, 比如开枪
		/// </summary>
		/// <param name="hand"></param>
		protected virtual void OnUse(Hand hand)
		{
			//Debug.LogError($"OnUse:{hand.handType}");
		}
	}
}
#endif
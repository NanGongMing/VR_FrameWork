﻿/*
* FileName:          ToolBasic
* CompanyName:       
* Author:            Relly
* CreateTime:        2022-03-31-09:51:35
* UnityVersion:      2020.3.26f1c1
* Version:           1.0
* Description:       
* 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if VIU_STEAMVR_2_0_0_OR_NEWER
using Valve.VR.InteractionSystem;

namespace UnityFramework
{
	public class ToolBasic : ToolInteractable
	{
		public ToolConf toolConf;
		[HideInInspector]
		public Hand CatchHand;
		protected Rigidbody mRigidbody;
		protected Throwable throwable;
		protected Interactable interactable;
		protected VelocityEstimator velocityEstimator;

		public event Action<Hand, ToolConf> OnHandCatch;
		public event Action<Hand, ToolConf> OnHandCatching;
		public event Action<Hand, ToolConf> OnHandDetach;
		public event Action<Hand, ToolConf> OnHandHoverBegin;
		public event Action<Hand, ToolConf> OnHandHoverUpdate;
		public event Action<Hand, ToolConf> OnHandHoverEnd;
		public event Action<Hand, ToolConf> OnHandUse;
		public event Action<ToolConf, ToolConf> onEnterTool;
		public event Action<ToolConf, ToolConf> onExitTool;
		public event Action<ToolConf, ToolConf> onCollideTool;

		// 此物体碰其他物体 ,设置此物体
		//其他物体碰次物体,设置其他物体
		Vector3 localPosition;
		Quaternion localRotation;
		Transform localParent;
		public Collider[] Colliders;
		public ToolBasic ColliderTool;
		internal bool attached = false;

		protected override void Awake()
		{
			base.Awake();
			Init();
		}
		private void Init()
		{
			mRigidbody = GetComponent<Rigidbody>();
			throwable = GetComponent<Throwable>();
			interactable = GetComponent<Interactable>();
			velocityEstimator = GetComponent<VelocityEstimator>();
			localPosition = transform.localPosition;
			localRotation = transform.localRotation;
			localParent = transform.parent;
			Colliders = transform.GetComponentsInChildren<Collider>();
			if (toolConf)
			{
				toolConf.ToolBasic = this;
				if (throwable && toolConf)
					throwable.attachmentFlags = toolConf.AttachmentFlags;
			}
		}
		public virtual void OnTaskInit(TaskConf taskConf)
		{

		}
		public virtual void OnTaskStart(TaskConf taskConf)
		{

		}

		public virtual void OnTaskDoing(TaskConf taskConf)
		{

		}

		public virtual void OnTaskEnd(TaskConf taskConf)
		{

		}

		private void OnTriggerEnter(Collider other)
		{
			ToolBasic otherTool = GetTypeParent<ToolBasic>(other.gameObject, true);

			if (otherTool)
			{
				ColliderTool = otherTool;
				OnEnterTool(otherTool.toolConf);
			}
		}
		private void OnTriggerExit(Collider other)
		{

			ToolBasic otherTool = GetTypeParent<ToolBasic>(other.gameObject, true);
			if (otherTool)
			{
				if (ColliderTool && ColliderTool == otherTool)
				{
					ColliderTool = null;
				}
				OnExitTool(otherTool.toolConf);
			}


		}

		private void OnCollisionEnter(Collision collision)
		{
			ToolBasic otherTool = GetTypeParent<ToolBasic>(collision.gameObject, true);
			if (otherTool)
			{
				ColliderTool = otherTool;
				OnEnterTool(otherTool.toolConf);
			}

		}

		private void OnCollisionExit(Collision collision)
		{
			ToolBasic otherTool = GetTypeParent<ToolBasic>(collision.gameObject, true);
			if (otherTool)
			{
				if (ColliderTool && ColliderTool == otherTool)
				{
					ColliderTool = null;
				}
				OnExitTool(otherTool.toolConf);
			}
		}
		protected virtual void OnEnterTool(ToolConf colliderTool)
		{
			if (colliderTool)
			{
				onEnterTool?.Invoke(toolConf, colliderTool);
			}
			if (colliderTool)
			{
				onCollideTool?.Invoke(toolConf, colliderTool);
			}

			//进行位置设置
			if (toolConf && toolConf.IsSetCollidePose)
			{
				mRigidbody.isKinematic = toolConf.CollideEndIsKinematic;
				transform.localPosition = toolConf.CollidePositon;
				transform.localEulerAngles = toolConf.CollideAngle;
				transform.transform.parent = toolConf.CollideParent.ToolBasic.transform;
				SetToolCatch(toolConf.IsCanCatchOnCollideEnd);
			}
		}
		protected virtual void OnExitTool(ToolConf colliderTool)
		{
			if (colliderTool)
			{
				onExitTool?.Invoke(toolConf, colliderTool);
			}
		}
		protected override void OnHoverBegin(Hand hand)
		{
			if (toolConf)
			{
				OnHandHoverBegin?.Invoke(hand, toolConf);
			}
		}

		protected override void OnHoverUpdate(Hand hand)
		{
			if (toolConf)
			{
				OnHandHoverUpdate?.Invoke(hand, toolConf);
			}
		}

		protected override void OnHoverEnd(Hand hand)
		{
			if (toolConf)
			{
				OnHandHoverEnd?.Invoke(hand, toolConf);
			}

		}
		protected override void OnCatch(Hand hand)
		{
			CatchHand = hand;
			if (toolConf)
			{
				OnHandCatch?.Invoke(hand, toolConf);
			}
			attached = true;
			SetCatchPose(hand);

		}
		protected override void OnCatching(Hand hand)
		{
			if (toolConf)
			{
				OnHandCatching?.Invoke(hand, toolConf);
			}
		}
		protected override void OnDetach(Hand hand)
		{
			CatchHand = null;
			attached = false;
			if (toolConf)
			{
				OnHandDetach?.Invoke(hand, toolConf);
			}
		}
		protected override void OnUse(Hand hand)
		{
			if (toolConf)
			{
				OnHandUse?.Invoke(hand, toolConf);
			}
		}
		protected void SetCatchPose(Hand hand)
		{
			Transform objectAttachmentPoint = UnityUtil.GetTypeChildByName<Transform>(hand.gameObject, "ObjectAttachmentPoint");
			//需要进行抓取姿态设置
			if (toolConf)
			{
				if (toolConf.AttachmentFlags.HasFlag(Hand.AttachmentFlags.SnapOnAttach) && toolConf.IsSetCatchPose)
				{
					objectAttachmentPoint.localPosition = toolConf.ObjectAttachmentPointPosition;
					objectAttachmentPoint.localEulerAngles = toolConf.ObjectAttachmentPointAngle;
				}
			}
		}
		public void SetToolHover(bool isCanHover)
		{
			if (interactable)
			{
				this.isCanHover = isCanHover;
			}
		}
		public void SetToolCatch(bool isCanCatch)
		{
			this.isCanCatch = isCanCatch;
			if (throwable)
			{
				if (isCanCatch)
					throwable.attachmentFlags = toolConf.AttachmentFlags;
				else
					throwable.attachmentFlags = Hand.AttachmentFlags.TurnOnKinematic;
			}
			if (!isCanCatch && velocityEstimator)
				velocityEstimator.FinishEstimatingVelocity();
		}

		public void SetToolKinematic(bool isKinematic)
		{
			if (mRigidbody)
				mRigidbody.isKinematic = isKinematic;
		}
		public void SetToolUseGravity(bool isUseGravity)
		{
			if (mRigidbody)
				mRigidbody.useGravity = isUseGravity;
		}

		public void HideTool()
		{
			gameObject.SetActive(false);
		}
		public void ShowTool()
		{
			gameObject.SetActive(true);
		}

		public void SetColliders(bool enabled, bool isTrigger = false)
		{
			Collider[] colliders = GetComponentsInChildren<Collider>();
			//设置碰撞体
			for (int i = 0; i < colliders.Length; i++)
			{
				colliders[i].enabled = enabled;
				colliders[i].isTrigger = isTrigger;
			}
		}
		public void ResetPos()
		{
			transform.parent = localParent;
			transform.localPosition = localPosition;
			transform.localRotation = localRotation;
		}
		protected T GetTypeParent<T>(GameObject goSlef, bool recursive = true)
		{
			var result = goSlef.GetComponent<T>();
			if (result != null)
				return result;

			if (recursive)
			{
				var trParent = goSlef.transform.parent;
				if (trParent)
					return GetTypeParent<T>(trParent.gameObject, true);
			}

			return default(T);
		}
	}
}
#endif

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if VRTK && VIU_STEAMVR_1_2_3_OR_OLDER
using VRTK;
namespace UnityFramework
{
	public class LeftHandMenu_VRTK : MonoBehaviour
	{
		SteamVR_TrackedObject trackedObject;
		SteamVR_Controller.Device device;
		GameObject menu;
		// Start is called before the first frame update
		void Start()
		{
			trackedObject = transform.GetComponent<SteamVR_TrackedObject>();
			device = SteamVR_Controller.Input((int)trackedObject.index);
			menu = GetComponentInChildren<VRTK_UICanvas>(true).gameObject;
		}

		// Update is called once per frame
		void Update()
		{
			if (device.GetPressDown(SteamVR_Controller.ButtonMask.ApplicationMenu))
			{
				if (menu.activeInHierarchy) { menu.SetActive(false); }
				else { menu.SetActive(true); }
			}

			if (device.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad))
			{
				menu.SetActive(false);
			}
		}
	}
}

#endif


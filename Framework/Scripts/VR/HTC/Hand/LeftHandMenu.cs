using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
#if VRTK_VERSION_3_3_0_OR_OLDER
using VRTK;
#endif
#if ViveInputUtility
using HTC.UnityPlugin.Vive;
using HTC.UnityPlugin.Pointer3D;
#endif
namespace UnityFramework
{
	public class LeftHandMenu : MonoBehaviour
	{
		[Header("当前VR插件:VRTK,ViveInputUtility,Oculus")]
		public PluginOption PluginOption;
		private void Awake()
		{
			Init();
		}
		void Init()
		{
			switch (PluginOption)
			{
				case PluginOption.VRTK:
#if VRTK_VERSION_3_3_0_OR_OLDER
					transform.parent.gameObject.AddComponent<LeftHandMenu_VRTK>();
					gameObject.AddComponent<VRTK_UICanvas>();
#endif
					break;
				case PluginOption.ViveInputUtility:
#if ViveInputUtility
					transform.parent.gameObject.AddComponent<LeftHandMenu_Vive>();
					gameObject.AddComponent<CanvasRaycastTarget>();
#endif
					break;
				case PluginOption.Oculus:
					break;
				default:
					break;
			}
		}
		// Start is called before the first frame update
		void Start()
		{
			gameObject.SetActive(false);
		}
		private void OnEnable()
		{
			transform.GetChild(0).gameObject.SetActive(true);
		}
		public void Back()
		{
			SceneManager.LoadScene("Menu");
		}

	}
}


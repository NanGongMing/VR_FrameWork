using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if ViveInputUtility
using HTC.UnityPlugin.Vive;
#endif
namespace UnityFramework
{

	public delegate void TouchTarget(GameObject target);
	public delegate void Down_Grip(GameObject target);
	public delegate void Up_Grip(GameObject target);
	public delegate void Down_Trigger(GameObject target);
	public delegate void Up_Trigger(GameObject target);
	public delegate void Down_Trigger_UseObj(GameObject target);
	public class VRHandControl : MonoBehaviour
	{
#if VRTK_VERSION_3_3_0_OR_OLDER && VIU_STEAMVR_1_2_3_OR_OLDER
		protected SteamVR_TrackedObject trackedObject
		{
			get
			{
				return transform.GetComponentInParent<SteamVR_TrackedObject>();
			}
		}
		protected SteamVR_Controller.Device device
		{
			get
			{
				return SteamVR_Controller.Input((int)trackedObject.index);
			}
		}
#endif


		/// <summary>
		/// 当前VR插件:VRTK,ViveInputUtility,Oculus
		/// </summary>
		[Header("当前VR插件:VRTK,ViveInputUtility,Oculus")]
		public PluginOption PluginOption;

		/// <summary>
		/// 左右手
		/// </summary>
		[Header("哪只手")]
		public WhichHand WhichHand;

		/// <summary>
		// 是否按下Grip键,true代表按下
		/// </summary>
		[HideInInspector]
		public bool isPressDownGrip;
		/// <summary>
		/// 是否按下Trigger键
		/// </summary>
		[HideInInspector]
		public bool isPressDownTrigger;
		/// <summary>
		/// 是否按下Pad键
		/// </summary>
		[HideInInspector]
		public bool isPressDownPad;
		/// <summary>
		/// 手当前碰到的带碰撞体的物体
		/// </summary>


		[ReadOnly]
		[Header("当前碰到带GrabObject组件的物体")]
		public GameObject CollideObj;


		[ReadOnly]
		[Header("当前抓取的物体")]
		public GameObject CurrentGrabObject;


		/// <summary>
		/// true代表当前为拾取状态
		/// </summary>
		public bool isGrabState
		{
			get { return CurrentGrabObject != null; }
		}
		/// <summary>
		/// Trigger按下
		/// </summary>
		public event Down_Trigger Down_TriggerHandler;
		/// <summary>
		/// Trigger抬起
		/// </summary>
		public event Up_Trigger Up_TriggerHandler;
		/// <summary>
		/// Grab按下
		/// </summary>
		public event Down_Grip Down_GripHandler;
		/// <summary>
		/// Grab抬起 
		/// </summary>
		public event Up_Grip Up_GripHandler;



		void Start()
		{
			Down_GripHandler += Down_GrabHandlerEvent;
			Up_GripHandler += Up_GrabHandlerEvent;
			Down_TriggerHandler += Down_TriggerHandlerEvent;
			Up_TriggerHandler += Up_TriggerHandlerEvent;
		}
		void Update()
		{
			InputButton(PluginOption);
		}
		public virtual void OnTriggerEnter(Collider other)
		{
			GrabObject grabObject = other.GetComponent<GrabObject>();
			if (grabObject != null)
			{
				CollideObj = grabObject.gameObject;
			}
		}
		public virtual void OnTriggerStay(Collider other)
		{
			GrabObject grabObject = other.GetComponent<GrabObject>();
			if (grabObject != null)
			{
				CollideObj = grabObject.gameObject;
			}
		}
		public virtual void OnTriggerExit(Collider other)
		{
			GrabObject grabObject = other.GetComponent<GrabObject>();
			if (grabObject != null)
			{
				CollideObj = null;
			}
		}

		/// <summary>
		/// 拾取
		/// </summary>
		/// <param name="obj"></param>
		void OnPickUp(GameObject obj, GrabButton grabButton)
		{
			if (!obj)
			{
				return;
			}
			GrabObject grabObj = obj.GetComponent<GrabObject>();
			if (!grabObj)
			{
				return;
			}
			if (grabObj.GrabSwitch && grabObj.GrabButton == grabButton)
			{
				if (grabObj.WhichHand == this.WhichHand || grabObj.WhichHand == WhichHand.Both)
				{
					if (grabObj.GrabType == GrabType.HoldDown)
					{
						grabObj.OnPickUp(this);
					}
					else if (grabObj.GrabType == GrabType.Toggle)
					{
						if (!isGrabState)
						{
							grabObj.OnPickUp(this);
						}
						else
						{
							CurrentGrabObject.GetComponent<GrabObject>().OnPickDown(this);
						}
					}

				}
			}
		}
		/// <summary>
		/// 放下松手
		/// </summary>
		void OnPickDown(GrabButton grabButton)
		{
			if (!CurrentGrabObject)
			{
				return;
			}

			GrabObject grabObj = CurrentGrabObject.GetComponent<GrabObject>();

			if (grabObj.GrabButton == grabButton)
			{
				if (grabObj.GrabType == GrabType.HoldDown)
				{
					grabObj.OnPickDown(this);
				}
			}
		}
		public virtual void Down_GrabHandlerEvent(GameObject obj)
		{

		}
		public virtual void Up_GrabHandlerEvent(GameObject obj)
		{

		}
		public virtual void Up_TriggerHandlerEvent(GameObject target)
		{

		}
		public virtual void Down_TriggerHandlerEvent(GameObject target)
		{

		}

		void GetPressDownGrip()
		{
			OnPickUp(CollideObj, GrabButton.Grip);
			isPressDownGrip = true;
			Down_GripHandler?.Invoke(CollideObj);
		}
		void GetPressUpGrip()
		{
			OnPickDown(GrabButton.Grip);
			isPressDownGrip = false;
			Up_GripHandler?.Invoke(CollideObj);
		}
		void GetPressDownTrigger()
		{
			OnPickUp(CollideObj, GrabButton.Trigger);
			isPressDownTrigger = true;
			Down_TriggerHandler?.Invoke(CollideObj);
		}
		void GetPressUpTrigger()
		{
			OnPickDown(GrabButton.Trigger);
			isPressDownTrigger = false;
			Up_TriggerHandler?.Invoke(CollideObj);
		}
#if ViveInputUtility
		HandRole ViveGetHand(WhichHand whichHand)
		{
			return whichHand == WhichHand.RightHand ? HandRole.RightHand : HandRole.LeftHand;
		}
		void ViveButton()
		{
			if (ViveInput.GetPressDown(ViveGetHand(WhichHand), ControllerButton.Grip))
			{
				GetPressDownGrip();
			}
			if (ViveInput.GetPressUp(ViveGetHand(WhichHand), ControllerButton.Grip))
			{
				GetPressUpGrip();
			}
			if (ViveInput.GetPressDown(ViveGetHand(WhichHand), ControllerButton.Trigger))
			{
				GetPressDownTrigger();
			}
			if (ViveInput.GetPressUp(ViveGetHand(WhichHand), ControllerButton.Trigger))
			{
				GetPressUpTrigger();
			}
		}
#endif
#if VRTK_VERSION_3_3_0_OR_OLDER && VIU_STEAMVR_1_2_3_OR_OLDER
		void VRTKButton()
		{
			//按下握柄键拾取物体 松开扳机键松开物体
			if (device.GetPressDown(SteamVR_Controller.ButtonMask.Grip))
			{
				GetPressDownGrip();
			}
			//松开握柄键松开物体
			if (device.GetPressUp(SteamVR_Controller.ButtonMask.Grip))
			{
				GetPressUpGrip();

			}
			//按下扳机键
			if (device.GetPressDown(SteamVR_Controller.ButtonMask.Trigger))
			{
				GetPressDownTrigger();

			}
			//抬起扳机
			if (device.GetPressUp(SteamVR_Controller.ButtonMask.Trigger))
			{
				GetPressUpTrigger();
			}
		}
#endif

		/// <summary>
		/// 手柄按键输入
		/// </summary>
		/// <param name="PluginOption"></param>
		void InputButton(PluginOption PluginOption)
		{
			switch (PluginOption)
			{
				case PluginOption.VRTK:
#if VRTK_VERSION_3_3_0_OR_OLDER && VIU_STEAMVR_1_2_3_OR_OLDER
					VRTKButton();
#endif
					break;
				case PluginOption.ViveInputUtility:
#if ViveInputUtility
					ViveButton();
#endif
					break;
				case PluginOption.Oculus:
					break;
				default:
					break;
			}
		}

	}
}


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if ViveInputUtility
using HTC.UnityPlugin.Vive;
using HTC.UnityPlugin.Pointer3D;
namespace UnityFramework
{
	public class LeftHandMenu_Vive : MonoBehaviour
	{
		GameObject menu;
		// Start is called before the first frame update
		void Start()
		{
			menu = GetComponentInChildren<CanvasRaycastTarget>(true).gameObject;
		}

		// Update is called once per frame
		void Update()
		{
			if (ViveInput.GetPressDown(HandRole.LeftHand, ControllerButton.Menu))
			{
				if (menu.activeInHierarchy) { menu.SetActive(false); }
				else { menu.SetActive(true); }
			}

			if (ViveInput.GetPressDown(HandRole.LeftHand, ControllerButton.Pad))
			{
				menu.SetActive(false);
			}
		}
	}
}

#endif


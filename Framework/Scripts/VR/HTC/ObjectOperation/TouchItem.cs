/*
* FileName:          TouchItem
* CompanyName:  
* Author:            ShangChao
* CreateTime:        2021-05-07-15:39:53
* UnityVersion:      2019.4.8f1
* Version:           1.0
* Description:
* 
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace UnityFramework
{
	public class TouchItem : CollideObject
	{

		Transform jiantou;
		Collider[] colliders;

		[Header("下一步")]
		public UnityEvent NextStep;
		private void Awake()
		{

			jiantou = this.transform.Find("jiantou");
			colliders = GetComponents<Collider>();
			HideTips();
			SetCollider(false, false);

		}

		public virtual void StartOperation()
		{

			ShowTips();

			SetCollider(true);
			FunctionSwitch = true;
			//AnimManager.GetInstance.AnimPlayStartEvent += AnimStartEvent;

			CollideEndEvent.AddListener(OnCollideEndEvent);
		}

		public void OnCollideEndEvent()
		{
			NextStep?.Invoke();


			HideTips();
		}
		public void ShowTips()
		{

			if (jiantou)
			{
				UtilityTools.ShowObj(jiantou);
			}
		}
		public void HideTips()
		{

			if (jiantou)
			{
				UtilityTools.HideObj(jiantou);
			}
		}
		public void SetCollider(bool enable, bool istrigger = true)
		{
			if (colliders != null && colliders.Length > 0)
			{
				foreach (var item in colliders)
				{
					item.enabled = enable;
					item.isTrigger = istrigger;
				}
			}
		}
	}
}
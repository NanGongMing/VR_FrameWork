using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using System;
namespace UnityFramework
{
	[Serializable]
	[DisallowMultipleComponent]
	[RequireComponent(typeof(Collider))]
	[RequireComponent(typeof(Rigidbody))]
	public class KeepVertical : MonoBehaviour
	{
		[Header("功能开关,是否使用此功能")]
		public bool FunctionSwitch;
		[Header("碰撞下列物体后本物体会重置旋转保持垂直")]
		public List<GameObject> ResetCollideTarget;
		[Header("复位后事件")]
		public UnityEvent ResetEndEvent;

		public virtual void OnCollisionEnter(Collision collision)
		{
			if (FunctionSwitch && ResetCollideTarget.Contains(collision.gameObject))
			{
				if (!GetComponent<GrabObject>() || !GetComponent<GrabObject>().isGrabState)
				{
					ObjKeepVertical(this.transform, collision);
				}
			}
		}
		public virtual void OnCollisionStay(Collision collision)
		{
			if (FunctionSwitch && ResetCollideTarget.Contains(collision.gameObject))
			{
				if (!GetComponent<GrabObject>() || !GetComponent<GrabObject>().isGrabState)
				{
					ObjKeepVertical(this.transform, collision);
				}
			}
		}

		/// <summary>
		/// 碰到ResetCollideTarget,物体会垂直ResetCollideTarget上方
		/// </summary>
		/// <param name="transform">本物体</param>
		/// <param name="collision">ResetCollideTarget的碰撞体</param>
		public void ObjKeepVertical(Transform transform, Collision collision)
		{
			Collider collider = transform.GetComponent<Collider>();
			collider.isTrigger = true;
			float size = collider.bounds.size.y / 2;
			Vector3 pos = transform.position;
			ContactPoint ContactPoint = collision.GetContact(0);
			transform.position = new Vector3(pos.x, ContactPoint.point.y + size, pos.z);

			transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, 0);

			transform.GetComponent<Rigidbody>().isKinematic = true;

			ResetEndEvent.Invoke();

		}
	}
}


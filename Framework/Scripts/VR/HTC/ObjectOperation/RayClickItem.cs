/*
* FileName:          RayClickItem
* CompanyName:  
* Author:            ShangChao
* CreateTime:        2021-03-22-16:49:14
* UnityVersion:      2019.4.8f1
* Version:           1.0
* Description:
* 
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace UnityFramework
{
	public class RayClickItem : MonoBehaviour
	{
		Collider[] colliders;
		Transform jiantou;

		[Header("下一步")]
		public UnityEvent NextStep;
		private void Awake()
		{
			jiantou = this.transform.Find("jiantou");
			colliders = GetComponents<Collider>();
			HideTips();
			SetCollider(false, false);
		}
		/// <summary>
		/// 操作前的准备  比如高亮 箭头 语音
		/// </summary>
		public virtual void StartOperation()
		{

			SetCollider(true);
			ShowTips();

			Delegation.Event_ClickItem += RayClickItemUnityEvent;

		}
		protected virtual void RayClickItemUnityEvent(Transform transform)
		{
			if (transform == this.transform)
			{
				HideTips();
				SetCollider(false);
			}
		}


		public void ShowTips()
		{

			if (jiantou)
			{
				UtilityTools.ShowObj(jiantou);
			}
		}
		public void HideTips()
		{

			if (jiantou)
			{
				UtilityTools.HideObj(jiantou);
			}
		}
		public void SetCollider(bool enable, bool istrigger = true)
		{
			if (colliders != null && colliders.Length > 0)
			{
				foreach (var item in colliders)
				{
					item.enabled = enable;
					item.isTrigger = istrigger;
				}
			}
		}
		private void OnDestroy()
		{
			Delegation.Event_ClickItem -= RayClickItemUnityEvent;

		}
	}
}
/*
* FileName:          
* CompanyName:  
* Author:            ShangChao
* CreateTime:        2021-03-22-16:27:01
* UnityVersion:      2019.4.8f1
* Version:           1.0
* Description:
* 
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
namespace UnityFramework
{
	public class GrabItem : GrabObject
	{
		Collider[] colliders;
		Transform jiantou;
		[Header("下一步")]
		public UnityEvent NextStep;
		protected override void Awake()
		{
			base.Awake();
			colliders = GetComponents<Collider>();
			jiantou = this.transform.Find("jiantou");
			HideTips();
			SetCollider(false);

		}
		/// <summary>
		/// 操作前的准备  比如高亮 箭头 语音
		/// </summary>
		public virtual void StartOperation()
		{

			ShowTips();

			SetCollider(true);
			GrabSwitch = true;
		}
		public void ShowTips()
		{

			if (jiantou)
			{
				UtilityTools.ShowObj(jiantou);
			}
		}
		public void HideTips()
		{

			if (jiantou)
			{
				UtilityTools.HideObj(jiantou);
			}
		}
		public void SetCollider(bool enable, bool istrigger = true)
		{
			if (colliders != null && colliders.Length > 0)
			{
				foreach (var item in colliders)
				{
					item.enabled = enable;
					item.isTrigger = istrigger;
				}
			}
		}
		protected override void OnPickUpEvent(VRHandControl hand)
		{
			base.OnPickUpEvent(hand);
			HideTips();
		}
		protected override void OnPickDownEvent(VRHandControl hand)
		{
			base.OnPickDownEvent(hand);

			GrabSwitch = false;
			NextStep?.Invoke();

			SetCollider(true, false);

		}

	}
}
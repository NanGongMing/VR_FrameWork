using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
namespace UnityFramework
{
	public enum GrabPoint
	{
		/// <summary>
		/// 手与物体的碰撞点
		/// </summary>
		CollidePoint,
		/// <summary>
		/// 物体中心点
		/// </summary>
		Zero,
		/// <summary>
		/// 自定义
		/// </summary>
		Custom
	}
	[Serializable]
	[DisallowMultipleComponent]
	[RequireComponent(typeof(Collider))]
	[RequireComponent(typeof(Rigidbody))]
	public class GrabObject : MonoBehaviour
	{
		[Header("功能开关,触碰")]
		public bool TouchSwitch;
		[Header("触碰事件事件")]
		public UnityEvent TouchEnterUnityEvent;
		[Header("退出触碰事件事件")]
		public UnityEvent TouchExitUnityEvent;
		public event Action<VRHandControl> TouchEnterEvent;
		public event Action<VRHandControl> TouchExitEvent;

		[Header("功能开关,拾取")]
		public bool GrabSwitch;
		[Header("哪只手触发")]
		public WhichHand WhichHand;
		[Header("拾取按钮")]
		public GrabButton GrabButton = GrabButton.Grip;
		[Header("HoldDown持续按键保持拾取状态.Toggle按下按键为拾取再次按下为松手")]
		public GrabType GrabType = GrabType.Toggle;
#if UNITY_EDITOR
		[ReadOnly]
#endif
		[Header("拾取状态,true为拾取状态,false为未拾取")]
		public bool isGrabState;
		[Header("松手后刚体设置")]
		public bool UseGravity;
		public bool IsKinematic;
		[Header("拾取位置,悬浮字段查看说明")]
		[Tooltip("Zero为物体中心,Custom自定义抓取位置,CollidePoint为手与物体的碰撞点")]
		public GrabPoint GrabPoint;
		[Header("自定义拾取位置赋值使用GrabUpLocalPos还是V3")]
		public bool UsePosV3;
		[Header("自定义拾取位置GrabUpLocalPos")]
		public Transform GrabUpLocalPos;
		[Header("自定义拾取位置V3")]
		public Vector3 GrabUpLocalPosV3;
		[Header("自定义拾取旋转")]
		public Vector3 GrabUpLocalRoationV3;
		[Header("松手后父物体,可为null")]
		public Transform GrabDownParent;
		//预留手部拾取动画针对不同物体的不同拾取动画  未写
		[Header("拾取事件")]
		public UnityEvent PickUpUnityEvent;
		[Header("松手后事件")]
		public UnityEvent PickDownUnityEvent;
		public event Action<VRHandControl> PickUpEvent;
		public event Action<VRHandControl> PickDownEvent;

		VRHandControl HandControl;
		protected virtual void Awake()
		{
			InitEvent();
		}
		protected virtual void Start()
		{

		}
		protected virtual void OnTriggerEnter(Collider other)
		{
			VRHandControl VRHandControl = other.GetComponent<HTC_HandControl>();
			if (TouchSwitch && VRHandControl != null)
			{
				OnTouchEnter(VRHandControl);
			}
		}
		protected virtual void OnTriggerStay(Collider other)
		{
			VRHandControl VRHandControl = other.GetComponent<HTC_HandControl>();
			if (TouchSwitch && VRHandControl != null)
			{
				OnTouchEnter(VRHandControl);
			}
		}
		protected virtual void OnTriggerExit(Collider other)
		{
			VRHandControl VRHandControl = other.GetComponent<HTC_HandControl>();
			if (TouchSwitch && VRHandControl != null)
			{
				OnTouchExit(VRHandControl);
			}

		}
		protected virtual void OnDestroy()
		{
			DestroyEvent();
		}
		void InitEvent()
		{
			TouchEnterEvent += OnTouchEnterEvent;
			TouchExitEvent += OnTouchExitEvent;
			PickUpEvent += OnPickUpEvent;
			PickDownEvent += OnPickDownEvent;
		}
		void DestroyEvent()
		{
			TouchEnterUnityEvent.RemoveAllListeners();
			TouchExitUnityEvent.RemoveAllListeners();
			PickUpUnityEvent.RemoveAllListeners();
			PickDownUnityEvent.RemoveAllListeners();
			TouchEnterEvent -= OnTouchEnterEvent;
			TouchExitEvent -= OnTouchExitEvent;
			PickUpEvent -= OnPickUpEvent;
			PickDownEvent -= OnPickDownEvent;
		}

		/// <summary>
		/// 退出触摸
		/// </summary>
		protected virtual void OnTouchExitEvent(VRHandControl hand)
		{

		}
		/// <summary>
		/// 进入触摸
		/// </summary>
		protected virtual void OnTouchEnterEvent(VRHandControl hand)
		{

		}
		/// <summary>
		/// 拾取抓取
		/// </summary>
		protected virtual void OnPickUpEvent(VRHandControl hand)
		{

		}
		/// <summary>
		/// 放下松手
		/// </summary>
		protected virtual void OnPickDownEvent(VRHandControl hand)
		{

		}
		/// <summary>
		/// 退出触摸
		/// </summary>
		void OnTouchExit(VRHandControl hand)
		{
			HandControl = null;
			TouchExitUnityEvent?.Invoke();
			TouchExitEvent?.Invoke(hand);
		}
		/// <summary>
		/// 进入触摸
		/// </summary>
		void OnTouchEnter(VRHandControl hand)
		{
			HandControl = hand;
			TouchEnterUnityEvent?.Invoke();
			TouchEnterEvent?.Invoke(hand);
		}
		/// <summary>
		/// 拾取抓取
		/// </summary>
		internal void OnPickUp(VRHandControl hand)
		{
			if (hand.CurrentGrabObject)
			{
				return;
			}
			isGrabState = true;
			HandControl = hand;
			HandControl.CurrentGrabObject = this.gameObject;
			Rigidbody rigi = GetComponent<Rigidbody>();
			rigi.useGravity = true;
			rigi.isKinematic = true;
			this.transform.parent = hand.transform;
			SetPos(this.gameObject);
			PickUpUnityEvent?.Invoke();
			PickUpEvent?.Invoke(hand);
		}
		/// <summary>
		/// 放下松手
		/// </summary>
		internal void OnPickDown(VRHandControl hand)
		{
			isGrabState = false;
			Rigidbody rigi = GetComponent<Rigidbody>();
			rigi.useGravity = UseGravity;
			rigi.isKinematic = IsKinematic;
			transform.parent = GrabDownParent;
			isGrabState = false;
			HandControl = null;
			hand.CurrentGrabObject = null;
			PickDownUnityEvent?.Invoke();
			PickDownEvent?.Invoke(hand);
		}

		/// <summary>
		/// 拾取后的位置
		/// </summary>
		/// <param name="UsePosV3"></param>
		/// <param name="obj">目标</param>
		public void SetPos(GameObject obj)
		{
			switch (GrabPoint)
			{
				case GrabPoint.Zero:
					obj.transform.localPosition = Vector3.zero;
					obj.transform.localEulerAngles = Vector3.zero;
					break;
				case GrabPoint.CollidePoint:
					//目前未计算真实碰撞点
					break;
				case GrabPoint.Custom:
					if (UsePosV3)
					{
						obj.transform.localPosition = GrabUpLocalPosV3;
						obj.transform.localEulerAngles = GrabUpLocalRoationV3;
					}
					else
					{
						if (GrabUpLocalPos)
						{
							obj.transform.localPosition = GrabUpLocalPos.localPosition;
							obj.transform.localEulerAngles = GrabUpLocalPos.localEulerAngles;
						}
						else
						{
							obj.transform.localPosition = GrabUpLocalPosV3;
							obj.transform.localEulerAngles = GrabUpLocalRoationV3;
						}
					}
					break;
				default:
					break;
			}

		}

	}
}




using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using System;
namespace UnityFramework
{
	[Serializable]
	[DisallowMultipleComponent]
	[RequireComponent(typeof(Collider))]
	[RequireComponent(typeof(Rigidbody))]
	public class ResetPosition : MonoBehaviour
	{
		[Header("功能开关,是否使用此功能")]
		public bool FunctionSwitch;
		[Header("碰撞下列物体后本物体会复位")]
		public List<GameObject> ResetCollideTarget;
		Rigidbody LocalRigi;
		bool UseGravity, IsKinematic;
		Transform LocalParent;
		Vector3 LocalScale;
		Vector3 LocalPosition;
		Vector3 LocalRotation;
		[Header("复位后事件")]
		public UnityEvent ResetEndEvent;

		public virtual void Start()
		{
			LocalRigi = GetComponent<Rigidbody>();
			LocalParent = transform.parent ? transform.parent : null;
			LocalScale = transform.localScale;
			LocalPosition = transform.localPosition;
			LocalRotation = transform.localEulerAngles;
			UseGravity = LocalRigi.useGravity;
			IsKinematic = LocalRigi.isKinematic;
		}
		public virtual void OnTriggerEnter(Collider other)
		{
			if (FunctionSwitch && ResetCollideTarget.Contains(other.gameObject))
			{
				if (!GetComponent<GrabObject>() || !GetComponent<GrabObject>().isGrabState)
				{
					ResetPos(this.transform);
				}
			}
		}
		public virtual void OnTriggerStay(Collider other)
		{
			if (FunctionSwitch && ResetCollideTarget.Contains(other.gameObject))
			{
				if (!GetComponent<GrabObject>() || !GetComponent<GrabObject>().isGrabState)
				{
					ResetPos(this.transform);
				}
			}
		}
		/// <summary>
		/// 物体复位原位置旋转
		/// </summary>
		/// <param name="transform">目标物体</param>
		void ResetPos(Transform transform)
		{
			transform.parent = LocalParent;
			transform.localScale = LocalScale;
			transform.localPosition = LocalPosition;
			transform.localEulerAngles = LocalRotation;
			transform.GetComponent<Rigidbody>().useGravity = UseGravity;
			transform.GetComponent<Rigidbody>().isKinematic = IsKinematic;
			ResetEndEvent.Invoke();

		}
	}

}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
namespace UnityFramework
{
	[Serializable]
	[DisallowMultipleComponent]
	[RequireComponent(typeof(Collider))]
	[RequireComponent(typeof(Rigidbody))]
	public class CollideObject : MonoBehaviour
	{
		[Header("功能开关,是否使用此功能")]
		public bool FunctionSwitch;
		[Header("要碰撞的物体")]
		public GameObject CollideTarget;
		[Header("是否使用位置赋值,true使用,false只执行碰撞事件")]
		public bool IsSetPos;
		[Header("碰撞后设置本物体的父物体")]
		public Transform CollideEndParent;
		[Header("位置赋值使用CollideEndPos还是V3")]
		public bool UsePosV3;
		[Header("碰撞后的位置CollideEndPos")]
		public Transform CollideEndPos;
		[Header("碰撞后的位置V3")]
		public Vector3 CollideEndLocalPosV3;
		[Header("碰撞后的旋转")]
		public Vector3 CollideEndLocalRoationV3;
		[Header("碰撞后事件")]
		public UnityEvent CollideEndEvent;
		public virtual void OnTriggerEnter(Collider other)
		{
			if (FunctionSwitch && CollideTarget == other.gameObject)
			{
				Collide_SetPos(this.transform);
			}
		}
		public virtual void OnTriggerStay(Collider other)
		{
			if (FunctionSwitch && CollideTarget == other.gameObject)
			{
				Collide_SetPos(this.transform);
			}
		}
		/// <summary>
		/// 设置新位置
		/// </summary>
		/// <param name="transform">要设置的物体</param>
		public virtual void Collide_SetPos(Transform transform)
		{
			if (IsSetPos)
			{
				if (UsePosV3)
				{
					transform.parent = CollideEndParent;
					transform.localPosition = CollideEndLocalPosV3;
					transform.localEulerAngles = CollideEndLocalRoationV3;
					CollideEndEvent.Invoke();
				}
				else
				{
					if (CollideEndPos)
					{
						transform.parent = CollideEndParent;
						transform.localPosition = CollideEndPos.position;
						transform.localEulerAngles = CollideEndPos.localEulerAngles;
						// transform.localScale = CollideEndPos.localScale;
						CollideEndEvent.Invoke();
					}
					else
					{
						transform.parent = CollideEndParent;
						transform.localPosition = CollideEndLocalPosV3;
						transform.localEulerAngles = CollideEndLocalRoationV3;
						CollideEndEvent.Invoke();
					}

				}
			}
			else
			{
				CollideEndEvent.Invoke();
			}


		}
		/// <summary>
		/// 设置新位置
		/// </summary>
		/// <param name="transform">要设置位置的物体</param>
		/// <param name="target">设置到target的位置</param>
		public virtual void Collide_SetPos(Transform transform, Transform target)
		{
			transform.parent = target.parent;
			transform.localScale = target.localScale;
			transform.localPosition = target.localPosition;
			transform.localEulerAngles = target.localEulerAngles;
		}
	}
}

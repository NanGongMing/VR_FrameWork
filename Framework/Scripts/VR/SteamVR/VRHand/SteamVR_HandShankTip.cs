#if STEAMVR

using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using System.Collections.Generic;

namespace K_UnityGF.VRFramework
{
    /// <summary>
    /// 手柄键位提示
    /// </summary>
    public class SteamVR_HandShankTip : MonoBehaviour
    {
        [Header("是否开启手柄提示"),SerializeField]
        private bool startHandTip;

        [Header("提示触发手"), SerializeField]
        private SteamVR_Input_Sources TipTriggerHand = SteamVR_Input_Sources.LeftHand;

        [Header("提示触发键"), SerializeField]
        private SteamVR_Action_Boolean TipTriggerKey = SteamVR_Actions.default_InteractUI;

        [Header("左手柄提示列表"), SerializeField]
        private List<TipInfo> LeftHandTipList = new List<TipInfo>()
        {
            new TipInfo(SteamVR_Actions.default_Teleport,"瞬移"),
            new TipInfo(SteamVR_Actions.default_GrabGrip,"拾取物体")
        };

        [Header("右手柄提示列表"), SerializeField]
        private List<TipInfo> RightHandTipList = new List<TipInfo>() 
        {
            new TipInfo(SteamVR_Actions.default_Teleport,"人物旋转"),
            new TipInfo(SteamVR_Actions.default_InteractUI,"UI与物体点击交互"),
            new TipInfo(SteamVR_Actions.default_GrabGrip,"拾取物体")
        };

        private void Start()
        {
            if (!startHandTip) return;
            Invoke("DelayTip", 1);

            SteamVR_Actions.default_InteractUI.AddOnStateDownListener(StartHandShankTip, SteamVR_Input_Sources.LeftHand);
            SteamVR_Actions.default_InteractUI.AddOnStateUpListener(StopHandShankTip, SteamVR_Input_Sources.LeftHand);
        }

        /// <summary>
        /// 延迟提示-Player完成初始化后
        /// </summary>
        private void DelayTip()
        {
            switch (TipTriggerHand)
            {
                case SteamVR_Input_Sources.RightHand:
                    ControllerButtonHints.ShowTextHint(Player.instance.rightHand, TipTriggerKey, "手柄按键提示");
                    break;
                case SteamVR_Input_Sources.LeftHand:
                    ControllerButtonHints.ShowTextHint(Player.instance.leftHand, TipTriggerKey, "手柄按键提示");
                    break;
            }
        }

        /// <summary>
        /// 开始按键提示
        /// </summary>
        /// <param name="fromAction"></param>
        /// <param name="fromSource"></param>
        private void StartHandShankTip(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
        {
            ControllerButtonHints.HideTextHint(Player.instance.leftHand, SteamVR_Actions.default_InteractUI);

            //开始所有按键提示
            LeftHandTipList.ForEach(handTip => ControllerButtonHints.ShowTextHint(Player.instance.leftHand,handTip.TipKey,handTip.TipValue));
            RightHandTipList.ForEach(handTip => ControllerButtonHints.ShowTextHint(Player.instance.rightHand, handTip.TipKey, handTip.TipValue));
        }

        /// <summary>
        /// 结束按键提示
        /// </summary>
        /// <param name="fromAction"></param>
        /// <param name="fromSource"></param>
        private void StopHandShankTip(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
        {
            ControllerButtonHints.HideAllTextHints(Player.instance.leftHand);
            ControllerButtonHints.HideAllTextHints(Player.instance.rightHand);
        }

        [System.Serializable]
        /// <summary>
        /// 提示类
        /// </summary>
        public class TipInfo
        {
            /// <summary>
            /// 提示键
            /// </summary>
            public SteamVR_Action_Boolean TipKey;

            /// <summary>
            /// 提示值
            /// </summary>
            public string TipValue;

            public TipInfo(SteamVR_Action_Boolean tipKey, string tipValue)
            {
                TipKey = tipKey;
                TipValue = tipValue;
            }
        }
    }
}

#endif

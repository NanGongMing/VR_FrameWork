﻿using UnityEngine;
#if VIU_STEAMVR_2_0_0_OR_NEWER

using Valve.VR;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace UnityFramework
{
	public class VRInputAxesConf : VRInputConf
	{
		public SteamVR_Action_Vector2 vr_Action_Vector2;
		public SteamVR_ActionSet actionSet;
		public Vector2 GetDelt(SteamVR_Input_Sources sources)
		{
			return vr_Action_Vector2.GetAxis(sources);
		}
#if UNITY_EDITOR
		[MenuItem("Assets/Create/VR/VRInputAxesConf", false, 0)]
		static void CreateDynamicConf()
		{
			UnityEngine.Object obj = Selection.activeObject;
			if (obj)
			{
				string path = AssetDatabase.GetAssetPath(obj);
				ScriptableObject bullet = ScriptableObject.CreateInstance<VRInputAxesConf>();
				if (bullet)
				{
					string confName = UnityUtil.TryGetName<VRInputAxesConf>(path);
					AssetDatabase.CreateAsset(bullet, confName);
				}
				else
				{
					Debug.Log(typeof(VRInputAxesConf) + " is null");
				}
			}
		}
#endif
	}
}
#endif
/*
* FileName:          UIText
* CompanyName:  
* Author:            
* CreateTime:        2021-08-20-14:33:47
* UnityVersion:      2019.4.8f1
* Version:           1.0
* Description:       
* 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityFramework;


namespace UnityFramework.UI
{
	public class UIText : UI<Text>
	{
		public Text Component
		{
			get
			{
				if (component == null)
				{
					component = GetComponent<Text>();
				}
				return component;
			}
		}
		protected override void OnStart()
		{
			base.OnStart();
			component.raycastTarget = false;
		}

		/// <summary>
		/// 更新文本
		/// </summary>
		/// <param name="contents">文本内容</param>
		public void UpdateContent(string _content)
		{
			component.text = _content;
		}
	}
}
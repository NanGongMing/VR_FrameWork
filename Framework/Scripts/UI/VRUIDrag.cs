/*
* FileName:          VRUIDrag
* CompanyName:  
* Author:            
* CreateTime:        2021-07-29-16:01:37
* UnityVersion:      2019.4.8f1
* Version:           1.0
* Description:       
* 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class VRUIDrag : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	public bool dragOnSurfaces = true;
	private Dictionary<int, GameObject> m_DraggingIcons = new Dictionary<int, GameObject>();
	private Dictionary<int, RectTransform> m_DraggingPlanes = new Dictionary<int, RectTransform>();
	Canvas canvas;
	Vector3 offset;
	Transform localParent;
	Transform TargetParent;
	Quaternion localQ;

	private void Start()
	{
		localParent = this.transform.parent;

	}
	public void OnBeginDrag(PointerEventData eventData)
	{
		canvas = GetComponent<Canvas>();
		if (!canvas)
		{
			return;
		}

		TargetParent = GameObject.FindGameObjectWithTag("RightHand").transform;
		this.transform.SetParent(TargetParent, true);
		#region MyRegion
		//// We have clicked something that can be dragged.
		//// What we want to do is create an icon for this.
		//var draggingIcon = canvas.gameObject;
		//if (!m_DraggingIcons.ContainsKey(eventData.pointerId))
		//{
		//	m_DraggingIcons.Add(eventData.pointerId, draggingIcon);

		//}
		////	m_DraggingIcons[eventData.pointerId] = draggingIcon;

		////draggingIcon.transform.SetParent(canvas.transform, false);
		//draggingIcon.transform.SetAsLastSibling();

		////	var draggingImage = draggingIcon.AddComponent<Image>();
		//// The icon will be under the cursor.
		//// We want it to be ignored by the event system.
		////	var draggingGroup = draggingIcon.AddComponent<CanvasGroup>();
		////	draggingGroup.blocksRaycasts = false;
		////draggingImage.sprite = GetComponent<Image>().sprite;

		////	var rectTransform = GetComponent<RectTransform>();
		////	draggingImage.SetNativeSize();
		////	draggingImage.rectTransform.sizeDelta = rectTransform.rect.size;
		//RectTransform plane = canvas.GetComponent<RectTransform>();

		//m_DraggingPlanes[eventData.pointerId] = plane;
		//Vector3 globalMousePos;
		//if (RectTransformUtility.ScreenPointToWorldPointInRectangle(plane, eventData.position, eventData.pressEventCamera, out globalMousePos))
		//{
		//	offset = globalMousePos - plane.position;

		//}
		//SetDraggedPosition(eventData);
		#endregion



	}

	public void OnDrag(PointerEventData eventData)
	{
		//if (m_DraggingIcons.ContainsKey(eventData.pointerId))
		//{
		//	SetDraggedPosition(eventData);
		//}
		localQ = transform.localRotation;
		//	transform.localRotation = Quaternion.Euler(0, localQ.y, 0);

	}

	public void OnEndDrag(PointerEventData eventData)
	{
		Vector3 pos = eventData.pointerCurrentRaycast.gameObject.transform.position;

		this.transform.SetParent(localParent, true);
		this.transform.position = pos;
		//transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);
	}
	private void SetDraggedPosition(PointerEventData eventData)
	{
		//GameObject draggingIcon;
		//if (!m_DraggingIcons.TryGetValue(eventData.pointerId, out draggingIcon)) { return; }

		//var rectTransform = draggingIcon.GetComponent<RectTransform>();
		//var raycastResult = eventData.pointerCurrentRaycast;
		//if (dragOnSurfaces && raycastResult.isValid && raycastResult.worldNormal.sqrMagnitude >= 0.0000001f)
		//{
		//	// When raycast hit something, place the dragged image at the hit position
		//	// Notice that if raycast performed by GraphicRaycaster module, worldNormal is not assigned (see GraphicRaycaster for more detail)
		//	// add a little distance to avoid z-fighting
		//	rectTransform.position = raycastResult.worldPosition + raycastResult.worldNormal * 0.01f;
		//	rectTransform.rotation = Quaternion.LookRotation(raycastResult.worldNormal, raycastResult.gameObject.transform.up);


		//}
		//else
		//{
		//	RectTransform plane;
		//	if (dragOnSurfaces && eventData.pointerEnter != null && eventData.pointerEnter.transform is RectTransform)
		//	{
		//		plane = eventData.pointerEnter.transform as RectTransform;
		//	}
		//	else
		//	{
		//		plane = m_DraggingPlanes[eventData.pointerId];
		//	}

		//	Vector3 globalMousePos;
		//	if (RectTransformUtility.ScreenPointToWorldPointInRectangle(plane, eventData.position, eventData.pressEventCamera, out globalMousePos))
		//	{

		//		rectTransform.position = globalMousePos - offset;
		//		rectTransform.rotation = plane.rotation;

		//		//rectTransform.rotation = Quaternion.LookRotation(eventData.pressEventCamera.transform.position - rectTransform.position, raycastResult.gameObject.transform.up);

		//	}
		//}
	}
}




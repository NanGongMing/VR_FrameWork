using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityFramework.UI
{
	public class UI<T> : MonoBase where T : UnityEngine.Component
	{
		protected T component;
		protected override void OnInit()
		{
			component = GetComponent<T>();
		}
	}
}
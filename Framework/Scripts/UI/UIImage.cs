/*
* FileName:          UIImage
* CompanyName:  
* Author:            
* CreateTime:        2021-08-20-14:33:47
* UnityVersion:      2019.4.8f1
* Version:           1.0
* Description:       
* 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityFramework;


namespace UnityFramework.UI
{
	public class UIImage : UI<Image>
	{
		public Image Component
		{
			get
			{
				if (component == null)
				{
					component = GetComponent<Image>();
				}
				return component;
			}
		}
		/// <summary>
		/// ���¾���ͼƬ
		/// </summary>
		/// <param name="_sprite"></param>
		public void UpdateSprite(Sprite _sprite) { component.sprite = _sprite; }
	}
}
/*
* FileName:          UIPanel
* CompanyName:  
* Author:            
* CreateTime:        2021-08-20-15:34:54
* UnityVersion:      2019.4.8f1
* Version:           1.0
* Description:       
* 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityFramework.UI
{
	public class UIPanel : UI<UIPanel>
	{
		public UIButton[] UIButtons;
		protected override void OnInit()
		{
			base.OnInit();
			UIButtons = GetComponentsInChildren<UIButton>();
		}

	}
}
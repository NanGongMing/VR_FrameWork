/*
* FileName:          UIToggle
* CompanyName:  
* Author:            
* CreateTime:        2021-08-20-14:33:47
* UnityVersion:      2019.4.8f1
* Version:           1.0
* Description:       
* 
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityFramework;


namespace UnityFramework.UI
{
	public class ToggleEvent : UnityEvent<bool, List<GameObject>> { };
	public class UIToggle : UI<Toggle>
	{
		public ToggleEvent ToggleEvent;
		public List<GameObject> Target;
		public Toggle Component
		{
			get
			{
				if (component == null)
				{
					component = GetComponent<Toggle>();
				}
				return component;
			}
		}
		protected override void OnStart()
		{
			base.OnStart();
			if (ToggleEvent == null)
			{
				ToggleEvent = new ToggleEvent();
			}
			component.onValueChanged.AddListener(ValueChangeEvent);
		}

		/// <summary>
		/// ֵ���¼�
		/// </summary>
		/// <param name="_bool"></param>
		protected virtual void ValueChangeEvent(bool _bool)
		{
			ToggleEvent?.Invoke(_bool, Target);
		}
	}
}
/*
* FileName:          UIViewData
* CompanyName:  
* Author:            ShangChao
* CreateTime:        2020-07-10-16:07:58
* UnityVersion:      2018.4.2f1
* Version:           1.0
* Description:       
* 
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityFramework.UI
{
	public class UIViewPicData
	{
		/// <summary>
		/// Ui名字
		/// </summary>
		public string UIName;
		/// <summary>
		/// 是不是按钮
		/// </summary>
		public bool IsButton;
		/// <summary>
		/// img
		/// </summary>
		public string UIImgSpriteName;
		/// <summary>
		/// 放上
		/// </summary>
		public string UIBtnHighSpriteName;
		/// <summary>
		/// 按下
		/// </summary>
		public string UIBtnPressSpriteName;
		/// <summary>
		/// disabled
		/// </summary>
		public string UIBtnDisSpriteName;
		/// <summary>
		/// 格式 后缀
		/// </summary>
		public string Format;
		/// <summary>
		/// 宽
		/// </summary>
		public string Width;
		/// <summary>
		/// 高
		/// </summary>
		public string Height;

	}

}
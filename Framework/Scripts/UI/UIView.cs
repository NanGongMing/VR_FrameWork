/*
* FileName:          UIView
* CompanyName:  
* Author:            ShangChao
* CreateTime:        2020-07-01-11:35:13
* UnityVersion:      2018.4.2f1
* Version:           1.0
* Description:       UI基类 控制UICanvas 
* 
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace UnityFramework.UI
{
	public abstract class UIView : MonoBase
	{
		/// <summary>
		/// Id
		/// </summary>
		public int Id;
		public string Name;

		/// <summary>
		/// 深度
		/// </summary>
		[Header("深度/SortOrder")]
		public int Depth = 0;
		/// <summary>
		/// 资源名称
		/// </summary>
		[Header("资源名称")]
		public string AssetName;
		/// <summary>
		/// 是否可见
		/// </summary>
		[Header("是否可见")]
		[SerializeField]
		private bool visible = true;
		Canvas canvas;

		public bool Visible
		{
			get => visible;
			set => SetVisible(value);
		}

		public int GetUIViewId()
		{
			int num = Framework.UI.GetUIViewId(this);
			if (num == -1)
			{
				return Id;
			}
			else
			{
				Id = num;
				return Id;
			}
		}
		/// <summary>
		/// 设置id
		/// </summary>
		void SetUIViewAutoId()
		{
			if (Framework.UI.IsAutoId)
			{
				Id = Framework.UI.SetUIViewAutoId();
			}
		}



		/// <summary>
		/// 初始化 获取UI物体 添加按钮事件等
		/// </summary>
		protected override void OnInit()
		{
			canvas = GetComponent<Canvas>();
			SetUIViewAutoId();
			OnDepthChanged(Depth);
			SetVisible(visible);
			if (string.IsNullOrEmpty(Name))
			{
				Name = gameObject.name;
			}
			OnRegisterUIView();

		}



		/// <summary>
		/// UIView注册 添加UIView缓存
		/// </summary>
		protected virtual void OnRegisterUIView()
		{
			Framework.UI.OnRegisterUIView(this);
		}

		/// <summary>
		/// 移除UIView 移除UIView缓存
		/// </summary>
		protected virtual void OnRemoveUIView()
		{
			Framework.UI.OnRemoveUIView(Id);
		}

		/// <summary>
		/// 显示
		/// </summary>
		protected override void OnShow()
		{
			SetVisible(true);
		}
		public override void SetVisible(bool boolean)
		{
			this.gameObject.SetActive(boolean);
			visible = boolean;
		}
		/// <summary>
		/// 隐藏 
		/// </summary>
		protected override void OnHide()
		{
			SetVisible(false);

		}


		/// <summary>
		/// 打开 
		/// </summary>
		public virtual void OnOpen()
		{
			OnShow();
		}

		/// <summary>
		/// 关闭
		/// </summary>
		/// <param name="isDestroy">是否销毁</param>
		/// <param name="userData"></param>
		public virtual void OnClose(bool isDestroy)
		{
			if (isDestroy)
			{
				Destroy(this.gameObject);
				OnRemoveUIView();
			}
			else
			{
				OnHide();
			}
		}

		/// <summary>
		/// 更新
		/// </summary>
		/// <param name="deltaTime">时间</param>
		public virtual void OnUpdate(float deltaTime)
		{

		}



		/// <summary>
		/// 深度改变
		/// </summary>
		/// <param name="depth">深度</param>
		protected virtual void OnDepthChanged(int depth)
		{
			if (canvas)
			{
				canvas.sortingOrder = depth;
			}
			else
			{
				Debug.LogError($"{this}设置depth Error:不存在Canvas组件");
			}
		}



	}
}


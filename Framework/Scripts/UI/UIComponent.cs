/*
* FileName:          UIManager
* CompanyName:  
* Author:            ShangChao
* CreateTime:        2020-07-06-15:59:25
* UnityVersion:      2018.4.2f1
* Version:           1.0
* Description:       
* 
*/

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityFramework.Runtime;

namespace UnityFramework.UI
{
	[DisallowMultipleComponent]
	public sealed partial class UIComponent : UnityFrameworkComponent
	{
		[SerializeField]
		[Header("是否自动设置Id")]
		private bool isAutoId;
		[SerializeField]

		private static int num = -1;
		private Dictionary<int, UIView> UIViewDicById = new Dictionary<int, UIView>();
		private Dictionary<string, UIView> UIViewDicByName = new Dictionary<string, UIView>();


		public bool IsAutoId { get => isAutoId; set => isAutoId = value; }

		public int SetUIViewAutoId()
		{
			num++;
			return num;
		}

		public int GetUIViewId(UIView uiView)
		{
			if (UIViewDicById.ContainsValue(uiView))
			{
				foreach (var item in UIViewDicById.Keys)
				{
					if (UIViewDicById[item] == uiView)
					{
						return item;
					}
				}
			}
			return uiView.Id;
		}

		/// <summary>
		/// 获取UIView
		/// </summary>
		/// <param name="id">id</param>
		/// <returns></returns>
		public UIView GetUIView(int id)
		{
			if (UIViewDicById.TryGetValue(id, out UIView uiView))
			{
				return uiView;
			}
			else
			{
				Debug.LogError($"获取 UIView Error : id {id}");
				return null;
			}
		}
		/// <summary>
		/// 获取UIView
		/// </summary>
		/// <param name="name">名字</param>
		/// <returns></returns>
		public UIView GetUIView(string name)
		{
			if (UIViewDicByName.TryGetValue(name, out UIView uiView))
			{
				return uiView;
			}
			else
			{
				Debug.LogError($"获取 UIView Error:id{name}");
				return null;
			}
		}

		/// <summary>
		/// 注册UIView
		/// </summary>
		/// <param name="uiView"></param>
		public void OnRegisterUIView(UIView uiView)
		{
			if (!UIViewDicById.ContainsKey(uiView.Id) && !UIViewDicByName.ContainsKey(uiView.Name))
			{
				UIViewDicById.Add(uiView.Id, uiView);
				UIViewDicByName.Add(uiView.Name, uiView);
			}
			else
			{
				Debug.LogError($"注册失败: Id|{uiView.Id}   Name|{uiView.Name}");
			}
		}

		/// <summary>
		/// 移除UIView
		/// </summary>
		/// <param name="id"></param>
		public void OnRemoveUIView(int id)
		{
			if (UIViewDicById.ContainsKey(id))
			{
				UIViewDicById.Remove(id);
				UIViewDicByName.Remove(GetUIView(id).name);
			}
		}
		/// <summary>
		/// 移除UIView
		/// </summary>
		/// <param name="name"></param>
		public void OnRemoveUIView(string name)
		{
			if (UIViewDicByName.ContainsKey(name))
			{
				UIViewDicByName.Remove(name);
				UIViewDicById.Remove(GetUIViewId(GetUIView(name)));

			}
		}

		/// <summary>
		/// 预加载UIView
		/// </summary>
		/// <param name="id">编号</param>
		/// <returns></returns>
		public UIView OnPreLoadUIView(int id)
		{
			if (UIViewDicById.TryGetValue(id, out UIView uiView))
			{
				uiView.OnOpen();
				return uiView;
			}
			else
			{
				Debug.LogError($"Open UIView Error:id{id}");
				return null;
			}
		}
		/// <summary>
		/// 打开UIView
		/// </summary>
		/// <param name="id">编号</param>
		/// <returns></returns>
		public UIView OnOpenUIView(int id)
		{
			if (UIViewDicById.TryGetValue(id, out UIView uiView))
			{
				uiView.OnOpen();
				return uiView;
			}
			else
			{
				Debug.LogError($"Open UIView Error:id{id}");
				return null;
			}
		}

		/// <summary>
		/// 关闭UIView
		/// </summary>
		/// <param name="id">编号</param>
		/// <param name="isDestroy">是否销毁</param>
		/// <returns></returns>
		public UIView OnCloseUIView(int id, bool isDestroy)
		{
			if (UIViewDicById.TryGetValue(id, out UIView uiView))
			{
				uiView.OnClose(isDestroy);
				return uiView;
			}
			else
			{
				Debug.LogError($"Close UIView Error:id{id}");
				return null;
			}
		}

		/// <summary>
		/// 更新
		/// </summary>
		void OnUpdate(float deltaTime)
		{
			foreach (var item in UIViewDicById.Values)
			{
				if (item.Visible)
				{
					item.OnUpdate(deltaTime);
				}
			}
		}
		private void Update()
		{
			OnUpdate(Time.deltaTime);
		}
		private void OnApplicationQuit()
		{
			num = -1;
			UIViewDicByName.Clear();
			UIViewDicById.Clear();

		}
	}
}